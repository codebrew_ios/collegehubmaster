

import UIKit
import Foundation
import CoreData
import FBSDKShareKit
import Google
import GoogleSignIn
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import IQKeyboardManagerSwift
import Stripe
import Firebase
import FirebaseMessaging
import UserNotifications
import CoreLocation
import GooglePlaces
import SwiftyJSON
import EZSwiftExtensions

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    var loaderLoadFirstTime = 0
    let defaults = UserDefaults.standard
    let gcmMessageIDKey = "gcm.message_id"
    let locationManager = CLLocationManager()
    var latitude : Double?
    var longitude : Double?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        
        // [START add_token_refresh_observer]
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        // [END add_token_refresh_observer]
        
        FIRApp.configure()
        
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        // [END register_for_notifications]
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
        application.registerForRemoteNotifications()
       

        // Initialize Google sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().delegate = self
        
        let kPlacesAPIKey = "AIzaSyC4g7UzsVfR8I7XrIBTV64NeWO-3lj4Ul4"
        let kMapsAPIKey = "AIzaSyAZCfa7_pNG2zepm-lbbgwIYJzmZZiFCO0"
        
        // Provide the Places API with your API key.
        GMSPlacesClient.provideAPIKey(kPlacesAPIKey)
        // Provide the Maps API with your API key. We need to provide this as well because the Place
        // Picker displays a Google Map.
        GMSServices.provideAPIKey(kMapsAPIKey)
        
        //IQ KeyBoard Manager Enabled
        IQKeyboardManager.sharedManager().enable = true
        
        
        //Setup Initial View Controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if ((UserDataSingleton.sharedInstance.loggedInUser?.access_token) != nil && UserDataSingleton.sharedInstance.loggedInUser?.is_verified == "1") {
            let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: "CustomTabbarController") as? CustomTabbarController
            self.window?.rootViewController = tabBarController
            self.window?.makeKeyAndVisible()
        }else{
            let navController = mainStoryboard.instantiateViewController(withIdentifier: "initialNavController") as? UINavigationController
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
        }
        STPPaymentConfiguration.shared().publishableKey = "pk_test_ikiKdCU8hnlWG065h2K82b2C"
        STPPaymentConfiguration.shared().appleMerchantIdentifier = "ca_AFvnodjTd6kCG0S2Jx9E4RlvuGLKwrja"
        
        //Register for location services
        isAuthorizedtoGetUserLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestLocation()
//            self.locationManager.requestAlwaysAuthorization()//edited
//            self.locationManager.startUpdatingLocation()//edited
        }
        
        guard let pushNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification]  else { return true }
        print(pushNotification)
        let data = JSON(pushNotification)
        
        UserDataSingleton.sharedInstance.notificationData = data
        UserDataSingleton.sharedInstance.userPush = 1
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        let data =  JSON(userInfo)
        handlePush(userInfo : data)
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)

        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        let data =  JSON(userInfo)
        handlePush(userInfo : data)
    }
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            UserDataSingleton.sharedInstance.device_token = refreshedToken
            updateFCMToken()
            
            
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
                let deviceTokenString = FIRInstanceID.instanceID().token()//deviceToken.base64EncodedString(options: .lineLength76Characters)
                
                print("APNs device token: \(deviceTokenString)")
                UserDefaults.standard.set(deviceTokenString, forKey: "device_token")
                print(UserDefaults.standard.value(forKey: "device_token") as! String? ?? "")
            }
        }
    }
    // [END connect_to_fcm]
    
    // [START connect_on_active]
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    // [END connect_on_active]
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        // Convert token to string
        let deviceTokenString = FIRInstanceID.instanceID().token()
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
        let deviceTokenForDevice = deviceToken.base64EncodedString(options: .lineLength76Characters)
        
        print("System device token: \(deviceTokenString)")
        print("APNs device token: \(deviceTokenString)")
        UserDefaults.standard.set(deviceTokenString, forKey: "device_token")
        print(UserDefaults.standard.value(forKey: "device_token") as! String? ?? "")
        //        UserDataSingleton.sharedInstance.loggedInUser?.device_token = deviceTokenString
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        GIDSignIn.sharedInstance().handle(url,
                                          sourceApplication: sourceApplication,
                                          annotation: annotation)
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // [START signin_handler]
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            // [START_EXCLUDE silent]
            //            NotificationCenter.default.post(
            //                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        } else {
            
            
            //Testing
            let token = FIRInstanceID.instanceID().token()
            print("InstanceID token: \(token!)")
            
            
            // [START_EXCLUDE]
            let googleUserData:[String: GIDGoogleUser] = ["data": user]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                object: nil,
                userInfo:googleUserData)
            
            // [END_EXCLUDE]
        }
    }
    // [END signin_handler]
    // [START disconnect_handler]
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        //        NotificationCenter.default.post(
        //            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
        //            object: nil,
        //            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
    // [END disconnect_handler]
    
    func tokenExpiredAction() {
        let alert = UIAlertController(title: "Session Expired", message: "Please Login again.", preferredStyle: .alert)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white
        
        guard let navController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
        self.window?.rootViewController = navController
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            //            guard let navController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
            //            self.window?.rootViewController = navController
            
        }))
        
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Method A hit")
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Method B hit")
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        
        let data =  JSON(response.notification.request.content.userInfo)
        handlePush(userInfo : data)
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        
        print("%@", remoteMessage.appData)
        
        let d : [String : Any] = remoteMessage.appData as! [String : Any]
        let name : String = d["name"] as! String
        let badge : String = d["badge"] as! String
        let post_id : String = d["p"] as! String
        let message_id : String = d["m"] as! String
        let user_id1 : String = d["u"] as! String
        let senderMessage : String = d["s_msg"] as! String
        let type : String = d["type"] as! String
    }
}
// [END ios_10_data_message_handling]

// PRAGMA:- Location
extension AppDelegate:CLLocationManagerDelegate{
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            locationManager.requestLocation()
            //do whatever init activities here.
        }
    }
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        UserDefaults.standard.set(locValue.latitude.toString, forKey: userPrefrences.lat.rawValue)
        UserDefaults.standard.set(locValue.longitude.toString, forKey: userPrefrences.long.rawValue)
        _ = getCurrentLocation(coordinate: locValue)
        
        print(UserDefaults.standard.value(forKey: userPrefrences.lat.rawValue) ?? "")
        print(UserDefaults.standard.value(forKey: userPrefrences.long.rawValue) ?? "")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    func getCurrentLocation(coordinate : CLLocationCoordinate2D) -> String{
        var placeValue = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { print($0) }
            
            if let city = addressDict["City"] as? String {
                print(city)
                UserDefaults.standard.set(city, forKey: userPrefrences.currentCity.rawValue)
                print(UserDefaults.standard.value(forKey: userPrefrences.currentCity.rawValue) ?? "")
                placeValue = city
                self.latitude = ((self.locationManager.location?.coordinate.latitude) ?? 76.00)
                
                self.longitude = ((self.locationManager.location?.coordinate.longitude) ?? 36.00)
                
                UserDataSingleton.sharedInstance.latitude = (self.latitude)?.toString
                UserDataSingleton.sharedInstance.longitude = (self.longitude)?.toString
            }
    
        })
        
        return placeValue
    }
    
}


//MARK: - HAndle Push Notification Methods

extension AppDelegate{
    
    func handlePush(userInfo : JSON){
        
        handlePushNavigation(userInfo: userInfo )
    }
    
    func handlePushNavigation(userInfo :  JSON){
        
        
        let storybard = UIStoryboard(name: "Main", bundle: nil)
        
        
        switch userInfo["type"].intValue {
        case 1:
            
            guard let VC = storybard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else{return}
            
            var model: MessageModel?
            model = MessageModel()
            
            model?.firstname = userInfo["name"].stringValue
            model?.users_id2 = userInfo["u"].stringValue
            VC.model = model
            VC.newMatch = true
            navigateToVc(vc: VC)
            
        case 2,3 :
            guard let VC = storybard.instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController else{return}
            VC.postIdNoti = userInfo["p"].stringValue
            VC.notiCheck = 1
            navigateToVc(vc: VC)
            
        default:break
            
        }
        
    }
    
    func navigateToVc(vc:UIViewController){
        
        ez.topMostVC?.presentVC(vc)
        
    }
}
//MARK:- UPDATE FCM TOKEN
extension  AppDelegate{
    func updateFCMToken()
    {
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.updateFCMToken(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token,device_token: UserDataSingleton.sharedInstance.device_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                _ = data as? ManagePaymentModal
                
            case .failureMessage( _):
                print("failure")
            default : break
            }
        }
        
    }

}
//extension AppDelegate {
//    let center = CLLocationCoordinate2DMake(self.latitude, self.longitude)
//    let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
//    let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
//    let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
//    let config = GMSPlacePickerConfig(viewport: viewport)
//    self.placePicker = GMSPlacePicker(config: config)
//    
//    // 2
//    placePicker.pickPlaceWithCallback { (place: GMSPlace?, error: NSError?) -> Void in
//    
//    if let error = error {
//    print("Error occurred: \(error.localizedDescription)")
//    return
//    }
//    // 3
//    if let place = place {
//    let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
//    let marker = GMSMarker(position: coordinates)
//    marker.title = place.name
//    marker.map = self.googleMapView
//    self.googleMapView.animateToLocation(coordinates)
//    } else {
//    print("No place was selected")
//    }
//    }
//}
//
