//
//  PopularNearbyTicketsTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyTicketsTableViewCell: UITableViewCell {

    
    //MARK::- CELL VARIABLES
    var tickets = [TicketPostViewModel]()
    var delegate: DelgatePopularVC?
    
    //MARK::- CELL OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
//MARK::- COLLECTION VIEW DELEGATES
extension PopularNearbyTicketsTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tickets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularNearbyTicketsCollectionViewCell", for: indexPath) as? PopularNearbyTicketsCollectionViewCell else {return UICollectionViewCell()}
        // checks to set the labels text only when they have the data from api else no text in labels-----------
        // for price
        let modal = tickets[indexPath.row]
        if let price = modal.price {
            cell.lblPrice.text = "$ " + price
        }
        else {
            cell.lblPrice.text = ""
        }
        // for time left
        if (modal.is_fixed == "1"){
            cell.lblTimeLeft.text = ""
            cell.btnBidNowOutlet.setTitle("Book Now", for: .normal)
        }
        else {
        let createdAt = (modal.createdAt).unwrap()
        let duration = (modal.duration).unwrap()
        let remaingDays = UtilityFunctions.calculateRemainingDays(createdAt,duration)
        cell.lblTimeLeft.text = remaingDays
        cell.btnBidNowOutlet.setTitle("Bid Now", for: .normal)
        }
        if(modal.is_favourite == "0"){
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart_blank"), for: .normal)
        }
        else{
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart"), for: .normal)
        }
        let price = (modal.price).unwrap().toInt()! * (modal.num_of_tickets).unwrap().toInt()!
        cell.lblPrice.text = "$ " + price.toString 
        cell.lblTicketsTitle.text = (modal.name).unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        let  dateIs = (modal.date)
        let dateString = dateIs?.toDateTime()
        cell.lblDateDay.text = dateString.unwrap()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("The selected index : \(indexPath.row)")
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        delegate?.passType(sectionType: 2, selectedIndexRow: indexPath.item)
    }
    
}
