//
//  PopularNearbyCarpoolTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyCarpoolTableViewCell: UITableViewCell {
    
    //MARK::- CELL VARIABLES
    var carpool =  [RidePostViewModel]()
    var delegate: DelgatePopularVC?
    
    //MARK::- CELL OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
//MARK::- COLLECTION VIEW DELEGATES
extension PopularNearbyCarpoolTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return carpool.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularNearbyCarpoolCollectionViewCell", for: indexPath) as? PopularNearbyCarpoolCollectionViewCell else {return UICollectionViewCell()}
        let modal = carpool[indexPath.row]
            cell.lblDestinationName.text = (modal.departure).unwrap() + " to " + (modal.destination).unwrap()
        let price = (modal.price).unwrap().toInt()! * (modal.num_of_seats).unwrap().toInt()!
            cell.lblPrice.text = "$ " + price.toString
            let  dateIs = (modal.leaving_on)
            let dateString = dateIs?.toDateTime()
            cell.lblDayAndDate.text = dateString.unwrap()
            cell.imgProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        if(modal.is_favourite == "0"){
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart_blank"), for: .normal)
        }
        else{
           cell.btnFavHeart.setImage(UIImage(named: "ic_heart"), for: .normal)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")
        delegate?.passType(sectionType: 0, selectedIndexRow: indexPath.item)
    }
    
    
}
