//
//  CardCollectionTableViewCell.swift
//  CollegeHub
//
//  Created by Sumanpreet on 11/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

protocol DelegateShowSlideShow {
    func showSlideShow(arrayimage : [String])
}

class CardCollectionTableViewCell: UITableViewCell {
    
    var arrayImages = [String]()
    var delegateSlideVC : DelegateShowSlideShow?
    var cardDetailVC : CardDetailViewController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension CardCollectionTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate{
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(arrayImages )
        return (arrayImages.count)
    }

    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardDetailImageCollectionViewCell", for: indexPath as IndexPath) as! CardDetailImageCollectionViewCell
        
        print(arrayImages)
        let imageURL = URL(string: arrayImages[indexPath.item])
        cell.imgView?.sd_setImage(with: imageURL)
        //cell.imgView?.sd_setImage(with: URL(string: (arrayImages[indexPath.row].image100).unwrap()))
        //imageView2.sd_setImage(with : URL(string:arrayOfFruits[indexPath.row]))
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegateSlideVC?.showSlideShow(arrayimage : arrayImages)
        print("You selected cell #\(indexPath.item)!")
    }
    
}
