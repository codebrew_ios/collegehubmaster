//
//  PopularNearbyBooksTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyBooksTableViewCell: UITableViewCell {

    
    //MARK::- CELL VARIABLES
    var books  =  [BookPostViewModel]()
    var delegate: DelgatePopularVC?
    //var delegateCardDetail: CardDetailViewController?
    
    //MARK::- CELL OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
//MARK::- COLLECTION VIEW DELEGATES
extension PopularNearbyBooksTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (books.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularNearbyBooksCollectionViewCell", for: indexPath) as? PopularNearbyBooksCollectionViewCell else {return UICollectionViewCell()}
        let modal = books[indexPath.row]
        cell.lblPrice.text = "$ " + (modal.price).unwrap()
        cell.lblBookName.text = (modal.name).unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        if (modal.is_fixed == "1"){
            cell.lblTimeLeft.text = ""
            cell.lblTimeLeftLabel.isHidden = true
            cell.lblTimeLeft.text = ""
            cell.lblTimeLeft.isHidden = true
            cell.btnBidNowOutlet.setTitle("Book Now", for: .normal)
            
        }
        else {
        let createdAt = (modal.createdAt).unwrap()
        let duration = (modal.duration).unwrap()
        let remaingDays = UtilityFunctions.calculateRemainingDays(createdAt,duration)
        cell.lblTimeLeft.text = remaingDays
        cell.btnBidNowOutlet.setTitle("Bid Now", for: .normal)
        }
        if(modal.is_favourite == "0"){
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart_blank"), for: .normal)
        }
        else{
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart"), for: .normal)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("The selected index : \(indexPath.row)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PopularCollectionCell"),
                                        object: nil,
                                        userInfo: ["index": 3])
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        delegate?.passType(sectionType: 3, selectedIndexRow: indexPath.item)
    }
    
}

