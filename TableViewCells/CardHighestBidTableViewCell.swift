//
//  CardHighestBidTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 16/05/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class CardHighestBidTableViewCell: UITableViewCell {

//MARK::- OUTLETS
    @IBOutlet weak var lblHighestBid: UILabel!
    @IBOutlet weak var lblHighestBidValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblHighestBidValue.layer.cornerRadius = 24
        lblHighestBidValue.layer.borderWidth = 2
        lblHighestBidValue.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
