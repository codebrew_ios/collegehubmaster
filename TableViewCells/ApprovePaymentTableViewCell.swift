//
//  ApprovePaymentTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 24/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

protocol DelegateApprovePayment: class {    
    func showAlert(message: String?,postsId: String?, section: Int?, row: Int?,users_id:String?,type:String?,firstname : String?,lastname: String?,image100: String?)
    func updateTableView(section: Int?,row:Int?)
    func presentReportVc(modal:ProfileTabSubMenus?,section:Int?,row:Int?)
}

class ApprovePaymentTableViewCell: UITableViewCell {
    
//MARK::- CELL OUTLETS
    @IBOutlet var btnApproveOutlet: UIButton!
    @IBOutlet var btnReportIssueOutlet: UIButton!
    @IBOutlet var imgProgileImage: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTitleTripDetails: UILabel!
    @IBOutlet var lblDestinationTo: UILabel!
    @IBOutlet var lblDestinationFrom: UILabel!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblAmmountToBePaid: UILabel!
    
//MARK::- Variables
    var section: Int?
    var row: Int?
    var userId = ""
    var posts_id = ""
    var type = ""
    var modal : ProfileTabSubMenus?
    var delegate: DelegateForTableCell?
    var myApprovePayment : ApprovePaymentsViewController?
    weak var alertDelegate:DelegateApprovePayment?
    var price : Int?
    
//MARK::- CELL LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
//MARK::- CELL BTN ACTIONS    
    @IBAction func btnApproveAction(_ sender: Any) {
        let username = (modal?.firstname).unwrap() + " " + (modal?.lastname).unwrap()
        guard let type = modal?.type else {return }
        switch type {
        case "6":
            price = (modal?.price).unwrap().toInt()! * (modal?.num_of_tickets).unwrap().toInt()!
        case "1":
            price = (modal?.price).unwrap().toInt()! * (modal?.num_of_seats).unwrap().toInt()!
        default:
            price = (modal?.price).unwrap().toInt() ?? 0
        }
        alertDelegate?.showAlert(message: "Aprove payment of $ \(((price)?.toString).unwrap()) for \(username)?",postsId: modal?.posts_id, section: self.section, row: self.row,users_id :modal?.users_id,type: modal?.type,firstname: modal?.firstname,lastname: modal?.lastname,image100: modal?.image100)
    }
    
    @IBAction func btnReportIssueAction(_ sender: Any) {
        alertDelegate?.presentReportVc(modal: modal, section: self.section, row: self.row)

    }
    

    
}
