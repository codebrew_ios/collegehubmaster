//
//  BiddingTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 27/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class BiddingTableViewCell: UITableViewCell {

//MARK::- CELL OUTLETS
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var imgUserProfileImgage: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet weak var lblTitleTopBid: UILabel!
    @IBOutlet weak var lblTopBidAmount: UILabel!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet weak var btnIncreadeBidOutlet: UIButton!
    @IBOutlet weak var constraintCollectionViewHeight: NSLayoutConstraint!
    
    //MARK::- VARIABLES
    var type : String?
    var myBiddingCell : BiddingViewController?
    var section = Int()
    var row = Int()
    var productImages = [String?]()
    

//MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }

//MARK::- CELL BTN ACTIONS
    @IBAction func btnIncreaseBidAction(_ sender: Any) {
        self.myBiddingCell?.viewChangeBidVC(section: section, row: row, view: "BiddingCell")
    }
}
//MARK::- COLLECTION VIEW DELEGATES
extension BiddingTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if productImages.count < 4{
                    return productImages.count
                }
                else {
                    return 4
                }
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellWidth =  UIScreen.main.bounds.width/4.6
    let height = collectionView.bounds.height
    return CGSize(width : cellWidth,height : height)
    
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BiddingProductimageCell", for: indexPath) as? BiddingProductImagesCollectionViewCell else {return UICollectionViewCell()}
    guard let image = productImages[indexPath.item] else { return cell }
    cell.imgProductImages?.sd_setImage(with: URL(string: image))
    if(indexPath.item == 3 && productImages.count > 4) {
        cell.lblImagesLeft.text = "\((productImages.count) - 4)+"
        cell.lblImagesLeft.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        cell.lblImagesLeft.isOpaque = true
    }
    else {
        cell.lblImagesLeft.isOpaque = false
        cell.lblImagesLeft.backgroundColor = UIColor.clear
        cell.lblImagesLeft.text = ""
    }
    
    return cell
}

}
