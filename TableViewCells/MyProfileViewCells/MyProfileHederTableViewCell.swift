//
//  MyProfileHederTableViewCell.swift
//  CollegeHub
//
//  Created by Sumanpreet on 15/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

//protocol DelegatePostAd : class {
//    func postAd()
//}

class MyProfileHederTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    //MARK::- PROPETIES
    //weak var delegate: DelegateMoveSegmentTo?
    
    //MARK::- VIEW CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK::- ACTIONS
    @IBAction func btnPostAd(_ sender: Any) {
        print("post ad in profile tab")
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "SlideToPopAdController"),
            object: nil,
            userInfo:nil)
    }
}
