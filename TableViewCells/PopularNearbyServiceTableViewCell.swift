//
//  PopularNearbyServiceTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SDWebImage

class PopularNearbyServiceTableViewCell: UITableViewCell {
    
    //MARK::- CELL VARIABLES
    var services = [ServicePostViewModel]()
    var delegate: DelgatePopularVC?

    //MARK::- CELL OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
//MARK::- COLLECTION VIEW DELEGATES
extension PopularNearbyServiceTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (services.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularNearbyServiceCollectionViewCell", for: indexPath) as? PopularNearbyServiceCollectionViewCell else {return UICollectionViewCell()}
        let modal = services[indexPath.row]
        cell.lblPrice.text = "$ " +  (modal.price).unwrap()
        cell.lblDurationOfService.text = (modal.duration).unwrap() + " hrs"
        cell.lblServiceName.text = (modal.service_name).unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        if(modal.is_favourite == "0"){
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart_blank"), for: .normal)
        }
        else{
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart"), for: .normal)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("The selected index : \(indexPath.row)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PopularCollectionCell"),
                                        object: nil,
                                        userInfo: ["index": 1])
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        delegate?.passType(sectionType: 1, selectedIndexRow: indexPath.item)
    }
    
}
