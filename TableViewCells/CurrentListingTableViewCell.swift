//
//  CurrentListingTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 24/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

protocol DelegateUpdateCurrentListing: class {
    func showAlert(message: String?,postsId: String?, section: Int?, row: Int?)
    func showSingleAlert(message: String?)
}

class CurrentListingTableViewCell: UITableViewCell {

//MARK::- CELL VARIABLES
    var myTripTableViewController: CurrentListingViewController?
    weak var alertDelegate:DelegateUpdateCurrentListing?
    var posts_id = ""
    var isEditable = ""
    var section = Int()
    var row = Int()
    var loaderLoadFirstTime = 0
    
//MARK::- CELL OUTLETS
    
    @IBOutlet var lblDestination: UILabel!
    @IBOutlet var lblDeparture: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnRemoveOutlet: UIButton!
    @IBOutlet var btnEditOutlet: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnEditOutlet.setTitle("View", for: .normal)
    }

//MARK::- CELL BTN ACTIONS
    @IBAction func btnRemoveAction(_ sender: Any) {
        
        switch isEditable
        {
        case "0" :
            alertDelegate?.showSingleAlert(message :"This post has already been booked or has got new bids.So you can not remove this post now")
            

        case "1" :
            alertDelegate?.showAlert(message: "Are you sure you want to delete this post?",postsId: self.posts_id, section: self.section, row: self.row)
        default : break
        }
    }
   
    //this is an edit button in current listing
    @IBAction func btnApproveAction(_ sender: Any) {
        self.myTripTableViewController?.viewCurrentListing(section: section, row: row, view: "Current Listing")
    }
    
}
