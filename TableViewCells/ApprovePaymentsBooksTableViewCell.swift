//
//  ApprovePaymentsBooksTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 29/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class ApprovePaymentsBooksTableViewCell: UITableViewCell {

//MARK::- CELL OUTLETS
    @IBOutlet var btnApproveOutlet: UIButton!
    @IBOutlet var btnReportIssueOutlet: UIButton!
    @IBOutlet var imgProgileImage: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblAmmountToBePaid: UILabel!
    @IBOutlet var lblProductTitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
    
//MARK::- Variables
    var postsId: String?
    var usersId:String?
    var section: Int?
    var row: Int?
    var type = ""
    var delegate: DelegateForTableCell?
    var modal : ProfileTabSubMenus?
    weak var alertDelegate:DelegateApprovePayment?
    var price : Int?
    
//MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }    
//MARK::- CELL BTN ACTIONS    
    @IBAction func btnApproveAction(_ sender: Any) {
        let username = (modal?.firstname).unwrap() + " " + (modal?.lastname).unwrap()
        guard let type = modal?.type else {return }
        switch type {
        case "6":
            price = (modal?.price).unwrap().toInt()! * (modal?.num_of_tickets).unwrap().toInt()!
        case "1":
            price = (modal?.price).unwrap().toInt()! * (modal?.num_of_seats).unwrap().toInt()!
        default:
            price = (modal?.price).unwrap().toInt() ?? 0
        }
        
        
        alertDelegate?.showAlert(message: "Aprove payment of $ \(((price)?.toString).unwrap()) for \(username)?",postsId: modal?.posts_id, section: self.section, row: self.row,users_id :modal?.users_id,type: modal?.type,firstname: modal?.firstname,lastname: modal?.lastname,image100: modal?.image100)
        
        
    }
    
    @IBAction func btnReportIssueAction(_ sender: Any) {
         alertDelegate?.presentReportVc(modal: modal, section: self.section, row: self.row)

    }
    
    

}
