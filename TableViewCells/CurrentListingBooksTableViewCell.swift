//
//  CurrentListingBooksTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 29/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class CurrentListingBooksTableViewCell: UITableViewCell {

//MARK::- CELL VARIABLES
    var myBookTableViewController: CurrentListingViewController?
    var delegate: DelegateForTableCell?
    weak var alertDelegate:DelegateUpdateCurrentListing?
    var posts_id = ""
    var isEditable = ""
    var section = Int()
    var row = Int()
    var loaderLoadFirstTime = 0
    
//MARK::- CELL OUTLETS
    
    @IBOutlet var lblDetailTitle: UILabel!
    @IBOutlet var lblBooktTitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnRemoveOutlet: UIButton!
    @IBOutlet var btnEditOutlet: UIButton!

//MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
    super.awakeFromNib()
        
    }

//MARK::- CELL BTN ACTIONS
    @IBAction func btnRemoveAction(_ sender: Any) {
        //let editable = (self.myBookTableViewController?.arrayCurrentListing[section].is_editable).unwrap()
        
        switch isEditable
        {
        case "0" :
              alertDelegate?.showSingleAlert(message :"This post has already been booked or has got new bids.So you can not remove this post now")
            
        case "1" :
            alertDelegate?.showAlert(message: "Are you sure you want to delete this post?",postsId: self.posts_id, section: self.section, row: self.row)

            
        default :
           break
        }
    }
    
    
    @IBAction func btnApproveAction(_ sender: Any) {
        self.myBookTableViewController?.viewCurrentListing(section: section, row: row, view: "Current Listing")

    }

}
