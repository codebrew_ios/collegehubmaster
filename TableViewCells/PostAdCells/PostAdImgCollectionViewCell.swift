//
//  PostAdImgCollectionViewCell.swift
//  CollegeHub
//
//  Created by Sumanpreet on 22/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PostAdImgCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}
