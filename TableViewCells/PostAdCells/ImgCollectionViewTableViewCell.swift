//
//  ImgCollectionViewTableViewCell.swift
//  CollegeHub
//
//  Created by Sumanpreet on 22/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class ImgCollectionViewTableViewCell: UITableViewCell {

    var arrayImgs = [UIImage]()
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ImgCollectionViewTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if #available(iOS 10.0, *) {
            return UICollectionViewFlowLayoutAutomaticSize
        }
        else {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
    }
}
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostAdImgCollectionViewCell", for: indexPath as IndexPath) as! PostAdImgCollectionViewCell
        cell.imgView.image = arrayImgs[indexPath.row]
        return cell
    }
    
    
}
