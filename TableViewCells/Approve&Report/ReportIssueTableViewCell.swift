//
//  ReportIssueTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 10/05/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class ReportIssueTableViewCell: UITableViewCell {

//MARK::- OUTLETS
    
    @IBOutlet weak var imgViewProfilePic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lblTripDetailsFixed: UILabel!
    @IBOutlet weak var lblToFixed: UILabel!
    @IBOutlet weak var lblDepratureCityName: UILabel!
    @IBOutlet weak var lblDestinationCityName: UILabel!
    
//MARK::- VARIABLES
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
