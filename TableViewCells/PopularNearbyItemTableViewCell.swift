//
//  PopularNearbyItemTableViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyItemTableViewCell: UITableViewCell {
    
    //MARK::- CELL VARIABLES
    var items = [OtherPostViewModel]()
    var delegate: DelgatePopularVC?
    
    //MARK::- CELL OUTLETS
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK::- CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
}
//MARK::- COLLECTION VIEW DELEGATES
extension PopularNearbyItemTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (items.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth =  UIScreen.main.bounds.width/1.7
        let height = collectionView.bounds.height
        return CGSize(width : cellWidth,height : height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularNearbyItemsCollectionViewCell", for: indexPath) as? PopularNearbyItemsCollectionViewCell else {return UICollectionViewCell()}
        let modal = items[indexPath.row]
        cell.lblProductName.text = (modal.name).unwrap()
        cell.lblPrice.text = "$" + (modal.price).unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        if (modal.is_fixed == "1"){
            cell.lblTimeLeft.text = ""
            cell.lblTimeLeftLabel.isHidden = true
            cell.btnBidNowOutlet.setTitle("Book Now", for: .normal)

        }
        else {
        let createdAt = (modal.createdAt).unwrap()
        let duration = (modal.duration).unwrap()
        let remaingDays = UtilityFunctions.calculateRemainingDays(createdAt,duration)
        cell.lblTimeLeft.text = remaingDays
        cell.btnBidNowOutlet.setTitle("Bid Now", for: .normal)
        }
        if(modal.is_favourite == "0"){
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart_blank"), for: .normal)
        }
        else{
            cell.btnFavHeart.setImage(UIImage(named: "ic_heart"), for: .normal)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("The selected index : \(indexPath.row)")
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PopularCollectionCell"),
                                        object: nil,
                                        userInfo: ["index": 4])
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        delegate?.passType(sectionType: 4, selectedIndexRow: indexPath.item)
    }
}
