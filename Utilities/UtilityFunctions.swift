//
//  UtilityFunctions.swift
//  Clikat Supplier
//
//  Created by Night Reaper on 08/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import UIKit
//import TYAlertController
//import RMDateSelectionViewController
import EZSwiftExtensions
import SystemConfiguration
import NVActivityIndicatorView
import SwiftyJSON
import ISMessages

class UtilityFunctions {
    
    class func alertForLocationAccess(vc :UIViewController) {
        
        let alert = UIAlertController(title: "Location Access Not Granted", message: "This application requires location services to work", preferredStyle: .alert)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white
        
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
            let settingsUrl = URL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(url)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        vc.present(alert, animated: true, completion: nil)
        
    }
    
    
    class func showISMessages(alert title : String? , message : String? , type : ISAlertType){
        
        ISMessages.hideAlert(animated: true)
        
        ISMessages.showCardAlert(withTitle: title , message: message , duration: 0.1 , hideOnSwipe: true , hideOnTap: true , alertType: type , alertPosition: .bottom , didHide: nil)
        ez.runThisAfterDelay(seconds: 1.0) {
            ISMessages.hideAlert(animated: true)
        }
        
    }
    
    class func startColoredLoader(){
//        GiFHUD.setGif("loader.gif")
//        GiFHUD.dismissOnTap()
//        GiFHUD.showWithOverlay()
        let activityData = ActivityData(size: CGSize(width:50 , height: 50), message: "", messageFont:nil , type: .ballClipRotate, color: UIColor(r: 13, g: 213, b: 178), padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.clear)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    class func startWhiteLoader(){
        
        let activityData = ActivityData(size: CGSize(width:50 , height: 50), message: "", messageFont:nil , type: .ballClipRotate, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.clear)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    class func stopLoader(){
//        GiFHUD.dismiss()
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    class func shareContentOnSocialMedia (withViewController controller : UIViewController?,message : String?){
        let  activityController = UIActivityViewController(activityItems: [message ?? ""], applicationActivities: [])
        controller?.present(activityController, animated: true, completion: nil)
    }
    
    class func sharedAppDelegateInstance() -> AppDelegate  {
        
        let delegate = UIApplication.shared.delegate
        return delegate as! AppDelegate
    }
    class func show(alert title:String , message:String  , buttonOk: @escaping () -> () , viewController: UIViewController){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = appColor
        alertController.view.backgroundColor = UIColor.white
        alertController.addAction(UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.default, handler: nil))
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {  (action) in
         alertController.dismiss(animated: true, completion: nil)
            buttonOk()
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    class func showSingle(alert title:String , message:String  , buttonOk: @escaping () -> () , viewController: UIViewController){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = appColor
        alertController.view.backgroundColor = UIColor.white
        //alertController.addAction(UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.default, handler: nil))
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {  (action) in
            buttonOk()
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }

    //making an alert with ok button click action
    class func showAlertAndDismiss(message: String?, controller: UIViewController, dismissController: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            dismissController(true)
//            _ = controller.dismiss(animated: true, completion: nil)
            
        }))
        
        controller.present(alert, animated: true, completion: {
            print("completion block")
            
        })

    }
    class func popularAlertForSearch(controller: UIViewController, showtab: @escaping (Int) -> Void) {
        let alert = UIAlertController(title: "Looking for...", message: nil, preferredStyle: .alert)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white
        
//        let popular = ["Rides", "Services", "Tickets", "Books", "Items"]
        alert.addAction(UIAlertAction(title: "Rides", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            showtab(0)
            
        }))
        alert.addAction(UIAlertAction(title: "Services", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            showtab(1)
            
        }))
        alert.addAction(UIAlertAction(title: "Tickets", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            showtab(2)
            
        }))
        alert.addAction(UIAlertAction(title: "Books", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            showtab(3)
            
        }))
        alert.addAction(UIAlertAction(title: "Items", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            showtab(4)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler:{ (UIAlertAction)in
            print("User click cancel button")
            alert.dismiss(animated: true, completion: nil)
            
        }))
        controller.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    class func showAlert(message : String?, controller: UIViewController){

        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
            
        }))
        
        controller.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    class func calculateRemainingDays(_ createdOn: String?, _ duration: String?) -> String {
        print(createdOn.unwrap())
        print(duration.unwrap())
        var stringToReturn = ""
        if let createdOn = createdOn  {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
            let dateCreatedOn = dateFormatter.date(from: createdOn) //according to date format your date string
            print(dateCreatedOn ?? "")
            
            let durationToInt = duration?.toInt() ?? 100
            
            let futureDate = Calendar.current.date(byAdding: .day, value: durationToInt, to: dateCreatedOn!)
            print(futureDate ?? "no date for future")
            
            
            //finding the difference of 2 dates
            let currentDate = Date()
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.day, .hour , .minute]
            formatter.unitsStyle = .abbreviated
            let differenceOfDates = formatter.string(from: currentDate, to: futureDate!)!
            print(differenceOfDates)
            
            if differenceOfDates[0] == "-" {
                print("Negative date")
                stringToReturn = "closed"
            }
            else {
                print("Positive date")
                stringToReturn = differenceOfDates
                
            }
            
        }
        return stringToReturn
    }
    

    
    class func showAlert(title : String?,message : String?,success: () -> (),cancel: () -> ()){
//        let alert = TYAlertView(title: title,message: message)
//        alert.buttonDefaultBgColor = Colors.MainColor.color()
//        alert.addAction(TYAlertAction(title: "Cancel", style: TYAlertActionStyle.Default, handler: { (action) in
//            alert.hideView()
//            cancel()
//        }))
//        alert.addAction(TYAlertAction(title: "OK", style: TYAlertActionStyle.Default, handler: { (action) in
//            alert.hideView()
//            success()
//        }))
//
//        alert.showInWindow()
    }
    
    /*
    static func addParallaxToView(view : UIView){
        let leftRightMin = -20.0
        let leftRightMax = 20.0
        
        let upDownMin = -20.0
        let upDownMax = 20.0
        
        
        let leftRight = UIInterpolatingMotionEffect(keyPath: "center.x",type: .TiltAlongHorizontalAxis)
        leftRight.minimumRelativeValue = leftRightMin
        leftRight.maximumRelativeValue = leftRightMax
        let upDown = UIInterpolatingMotionEffect(keyPath: "center.y",type: .TiltAlongVerticalAxis)
        upDown.minimumRelativeValue = upDownMin
        upDown.maximumRelativeValue = upDownMax
        
        //Create a motion effect group
        let meGroup = UIMotionEffectGroup()
        meGroup.motionEffects = [leftRight, upDown]
        
        view.addMotionEffect(meGroup)
    }
    */
    
    
}

//MARK: - Reachability
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {$0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)} } )
            else { return false }

        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
}
