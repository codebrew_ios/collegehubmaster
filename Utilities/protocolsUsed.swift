//
//  protocolsUsed.swift
//  CollegeHub
//
//  Created by Sierra 4 on 12/05/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import Foundation


protocol DelegateForTableCell: class {
    func passIndexPath(section: Int, row: Int, action: String)
}
protocol DelgateSegmentedController: class {
    func keepTrackOfSelectedIndex(selectedIndex: Int)
}
protocol DelgatePopularVC: class {
    func passType(sectionType: Int, selectedIndexRow: Int)
}
//protocol for moving the sjsegmented controller selected index to the action sheet item selection
protocol DelegateMoveSegmentTo: class {
    func moveSegement(index: Int)
}

protocol DelegateSearchText: class {
    func passSearchText(text: String)
}
extension Date{
    
    func getFormattedDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM , yyyy"
        let formattedDate = formatter.string(from: self )
        return formattedDate
    }
    
    func toString(dateStr:String, from:String, to:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = from
        guard let date =  formatter.date(from: dateStr) else { return "" }
        formatter.dateFormat = to
        return formatter.string(from: date)
    }
    
}
