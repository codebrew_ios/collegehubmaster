//
//  SlideShowCollectionViewCell.swift
//  CollegeHub
//
//  Created by Sierra 2 on 12/06/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class SlideShowCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    
}
