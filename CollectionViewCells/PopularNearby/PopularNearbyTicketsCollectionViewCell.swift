//
//  PopularNearbyTicketsCollectionViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyTicketsCollectionViewCell: UICollectionViewCell {
    
    //MARK::- CELL OUTLETS
    @IBOutlet var lblTicketsTitle: UILabel!
    @IBOutlet var imgProfileImage: UIImageView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblTimeLeft: UILabel!
    @IBOutlet var lblDateDay: UILabel!
    @IBOutlet weak var btnFavHeart: UIButton!
    @IBOutlet weak var btnBidNowOutlet: UIButton!
    
}
