//
//  PopularNearbyBooksCollectionViewCell.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PopularNearbyBooksCollectionViewCell: UICollectionViewCell {
 
    //MARK::- CELL OUTLETS
    
    @IBOutlet weak var btnBidNowOutlet: UIButton!
    @IBOutlet var imgProfileImage: UIImageView!
    @IBOutlet var lblTimeLeft: UILabel!
    @IBOutlet var lblBookName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet weak var lblTimeLeftLabel: UILabel!
    @IBOutlet weak var btnFavHeart: UIButton!
    
}
