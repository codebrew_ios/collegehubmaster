//
//  PendingRating.swift
//  CollegeHub
//
//  Created by Sierra 2 on 12/06/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import Foundation
import SwiftyJSON
import EZSwiftExtensions

class PendingRating : NSObject,JSONDecodable {
    
    var posts_id : String?
    var users_id2 : String?
    var firstname : String?
    var lastname : String?
    var image100 : String?
    
    
    required init(withAttributes attributes: OptionalSwiftJSONParameters) throws {
        super.init()
        
        posts_id = PostViewKeys.posts_id.rawValue => attributes
        users_id2 = PostViewKeys.users_id2.rawValue => attributes
        firstname = PostViewKeys.firstname.rawValue => attributes
        lastname = PostViewKeys.lastname.rawValue => attributes
        image100 = PostViewKeys.image100.rawValue => attributes
        
    }
    override init() {
        super.init()
    }
    
    class func parseArrayinToModal(withAttributes attributes : [JSON]?) -> AnyObject? {
        
        var arrayVendor : [PendingRating] = []
        guard let arrayData = attributes else {return arrayVendor as AnyObject?}
        
        
        for dict in arrayData {
            do {
                let modal =  try PendingRating(withAttributes: dict.dictionaryValue )
                arrayVendor.append(modal)
            } catch _ {
            }
            
        }
        return arrayVendor as AnyObject?
    }
    
}

