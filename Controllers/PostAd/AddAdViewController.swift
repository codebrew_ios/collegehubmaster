//
//  PostAdViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 21/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces
import CoreLocation
import NVActivityIndicatorView
import ActionSheetPicker_3_0
import BSImagePicker
import Photos
import EZSwiftExtensions


class AddAdViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblAdTypeName: UILabel!
    @IBOutlet weak var viewTextView: UIView!
    
    @IBOutlet weak var txtView: RSKPlaceholderTextView!{
        didSet{
            
            txtView.delegate = self
        }
    }
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var keyboardConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
    var typeOfConversation = Int()
    
    fileprivate lazy var httpClient : HTTPClient = HTTPClient()
    var currentArrayCount:Int?
    var subquestions1Exist: Bool?
    var subquestion1Index = 0
    var subquestions2Exist: Bool?
    var subquestion2Index = 0
    
    var type14Enabled: Bool?
    var type14Count = 0
    var type6Enabled: Bool?
    var type6Count = 0
    var subquestion1IndexForType6 = 0
    var temp = ""
    var loaderLoadFirstTime = 0
    
    //edited
    var resultsViewController: GMSAutocompleteResultsViewController?
    
    
    //type of add title
    var adType:String?
    
    //QuestionModel Types
    var currentModel = QuestionModel()
    var currentIndexpath = IndexPath(item: 0, section: 0)
    
    //Selected Values
    var dateSelected = ""
    var googlePlaceSelected = ""
    
    //Location
    var placesClient: GMSPlacesClient!
    let locationManager = CLLocationManager()
    
    //Arrays
    var cellValues = [cellValue]()
    var arrayTexts = [questionValueAndType]()
    var arrayOfCollections = [[questionValueAndType]]()
    var totalData = [[QuestionModel]]()
    var arrayRideValues = [String]()
    var arrayRideQuestions = [QuestionModel]()
    var arrayRideSubQuestions = [QuestionModel]()
    var arrayImages = [UIImage]()
    var arrayStops = [String]()
    var firstTypeDialogue: String?
    
    //Cell Enums
    enum cellPosition{
        case left
        case right
    }
    
    enum cellType{
        case text
        case collection
        case collectionImgs
    }
    
    //TypeAliasis
    typealias questionValueAndType = (model: QuestionModel,value: String, type: String)
    typealias cellValue = (quesValueAndType: [questionValueAndType], position: cellPosition, type: cellType)
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.placeholder = "Type here..."
        placesClient = GMSPlacesClient.shared()
        resultsViewController = GMSAutocompleteResultsViewController()
       // resultsViewController?.delegate = self as! GMSAutocompleteResultsViewControllerDelegate//edited
        isAuthorizedtoGetUserLocation()//edited
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            
            locationManager.requestLocation()
        }
        
        
        lblAdTypeName.text = "Post a \(adType ?? "") Offer"
        
        if typeOfConversation == 0 {
            firstTypeDialogue = "ride share"
        }
        else {
            firstTypeDialogue = adType
        }
        //Set parameters initially
        //         self.navBar?.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        subquestions1Exist = false
        subquestions2Exist = false
        type14Enabled = false
        type6Enabled = false
        viewTextView.isHidden = true
        guard let userName = UserDataSingleton.sharedInstance.loggedInUser?.firstname, let firstDialogue = firstTypeDialogue else { return }
        
        cellValues.append(([(currentModel ,"Hi \(userName), I’m your virtual assistant Mason. I’ll help you post your \(firstDialogue.lowercased()) offer.","0")],.left,.text))
        
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.separatorStyle = .none
        
        fetchQuestions()
        
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "hideMenuButton"),
            object: nil,
            userInfo:nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func checkStripeAccount(){
       // self.fetchProfileData()
        let stripeAccount = UserDataSingleton.sharedInstance.loggedInUser?.stripe_account_id
        if(stripeAccount == ""){
            guard let stripeAccountCreateVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ManagingPaymentsViewController") as? ManagingPaymentsViewController else {
                return }
            //            let cardCheck = UserDataSingleton.sharedInstance.loggedInUser?.card_added
            
            //            stripeAccountCreateVC?.amounts = amounts
            //            stripeAccountCreateVC?.postId = postId
            //            stripeAccountCreateVC?.typeToPass = typeToPass
            stripeAccountCreateVC.accessToken = UserDataSingleton.sharedInstance.loggedInUser?.access_token
            stripeAccountCreateVC.user_id = UserDataSingleton.sharedInstance.loggedInUser?.user_id
            
            // if cardCheck == "0" {
            ez.runThisInMainThread {
                stripeAccountCreateVC.addAdVC = self
                self.presentVC(stripeAccountCreateVC)
            }
        }
        else {
            publish()
        }
    }
    
    func publish(){
//    let stripeAccount = UserDataSingleton.sharedInstance.loggedInUser?.stripe_account_id
//        if(stripeAccount == ""){
//            let stripeAccountCreateVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ManagingPaymentsViewController") as? ManagingPaymentsViewController
////            let cardCheck = UserDataSingleton.sharedInstance.loggedInUser?.card_added
//            
////            stripeAccountCreateVC?.amounts = amounts
////            stripeAccountCreateVC?.postId = postId
////            stripeAccountCreateVC?.typeToPass = typeToPass
//            stripeAccountCreateVC?.accessToken = UserDataSingleton.sharedInstance.loggedInUser?.access_token
//            stripeAccountCreateVC?.user_id = UserDataSingleton.sharedInstance.loggedInUser?.user_id
//            
//           // if cardCheck == "0" {
//                ez.runThisInMainThread {
//                    self.presentVC(stripeAccountCreateVC!)
//                }
////            }else {
////                buyPost()
////                
////            }
//  
//        }
//        else {
        for question in self.arrayRideQuestions{
            
            if (question.sub_questions?.count)! > 0{
              
                for subquestion in question.sub_questions!{
                
                    if (subquestion.sub1_questions?.count)! > 0{
                     
                        for sub1question in subquestion.sub1_questions!{
                        
                            if (sub1question.sub2_questions?.count)! > 0{
                            
                                for sub2question in sub1question.sub2_questions!{
                                
                                    print(sub2question.sub2_questions_id ?? "")
                                    
                                    if((sub2question.answer?.count)! > 0){
                                    
                                        sub1question.answer = [sub1question.question!]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        var j = JSONSerializer.toJson(self.arrayRideQuestions)
        j = j.chopPrefix()
        j = j.chopSuffix()
        print(j)
        let user = UserDataSingleton.sharedInstance.loggedInUser
        
        if (typeOfConversation == 1 || typeOfConversation == 3 || typeOfConversation == 6){
       UtilityFunctions.startColoredLoader()
            
            
            APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.createPosts(access_token: user?.access_token, data: j, type: typeOfConversation.toString, lat: "", lng: "")),loaderNeeded:loaderLoadFirstTime) { (response) in
                self.loaderLoadFirstTime = 0
                
             UtilityFunctions.stopLoader()
                switch response{
                    
                case .success(let data) :
                    print(data ?? "")
                    print("published successfully.")
                    UtilityFunctions.showAlertAndDismiss(message: "Ad posted successfully!", controller: self, dismissController: {(flag) -> Void in
                        if flag == true {
                            _ = self.dismiss(animated: true, completion: nil)
                        }
                    })
                    
                    let x = data as? String
                    print(x ?? "")
                    
                case .failureMessage(let message):
                    print("failure")
                    UtilityFunctions.showAlert(message: message, controller: self)
                default : break
                    
                }
            }
        }else{
            UtilityFunctions.startColoredLoader()
            APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.createPosts(access_token: user?.access_token, data: j, type: typeOfConversation.toString,lat :"",lng : "")), image: arrayImages, completion: { (response) in
               
                 UtilityFunctions.stopLoader()
                switch response{
                    
                case .success(let data) :
                    print(data ?? "")
                    print("published successfully.")
                    UtilityFunctions.showAlertAndDismiss(message: "Ad posted successfully!", controller: self, dismissController: {(flag) -> Void in
                        if flag == true {
                            _ = self.dismiss(animated: true, completion: nil)
                        }
                    })
                    
                    let x = data as? String
                    print(x ?? "")
                    
                case .failureMessage(let message):
                    print("failure")
                    UtilityFunctions.showAlert(message: message, controller: self)
                default : break
                    
                }
            })
        }
    }
    //}
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true);
        self.navBar?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    //MARK::- Button Actions
    
    @IBAction func btnCancelButtonClick(_ sender: Any) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPopAdController(_ sender: Any) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    func checkOnTextView(type: Int) -> Bool {
        var valueToReturn = Bool()
        switch type {
        //maybe case 1 for tickets number
        case 1:
            //            "Ride"
            //            "Service"
            //            "Ticket"
            //            "Book"
            //            "Ticket"
            if adType == "Ticket"  {
                if txtView.text.toInt()! > 10 || txtView.text.toInt()! == 0{
                    valueToReturn = false
                }
                else {
                    valueToReturn = true
                }
                
            }
        case 11:
            if txtView.text.toInt()! > 999999 || txtView.text.toInt()! == 0 {
                valueToReturn = false
                //txtView.placeholder = "Duration(MAX 5 days)..."
            }
            else {
                valueToReturn = true
            }
        case 13://duration
            if adType == "Service"  {
                if  txtView.text.toInt()! > 12 || txtView.text.toInt()! == 0{
                    valueToReturn = false
                }
                else {
                    valueToReturn = true
                }
                
            }else if txtView.text.toInt()! > 5 {
                valueToReturn = false
            }
            else {
                valueToReturn = true
            }
            
        //case for tickets and duration value for service which is not more than 10
        default:
            valueToReturn = true
        }
        return valueToReturn
    }
    //btn to send
    @IBAction func btnSendClick(_ sender: Any) {
        
        if self.cellValues.count > 0{
            if txtView.text.length != 0{
                self.viewTextView.isHidden = true
                let type:Int =  Int(arrayTexts[currentIndexpath.item].type)!
                //0 select, 1 numeric, 2 file, 3 date, 4 radiobtn, 5 time, 6 dropdown, 7 images, 8 use_location, 9 text, 10 current_location, 11 price, 12 edition, 13 duration, 14 multiple_inputs
                
                let textValidity = checkOnTextView(type: type)
                
                if textValidity == false {
                    self.view.endEditing(true)
                    UtilityFunctions.showAlert(message: "Invalid Value entered", controller: self)
                    //txtView.resignFirstResponder()
                    
                }
                else if textValidity == true {
                    var textDisplayed = ""
                    if type == 11{
                        textDisplayed = "$" + txtView.text
                    }else if (type == 13){ //duration
                        textDisplayed = txtView.text + " hours"
                    }else{
                        textDisplayed = txtView.text
                    }
                    
                    self.cellValues[self.cellValues.count - 1] = ([(currentModel ,textDisplayed,"0")],.right,.text)
                    self.tblView.reloadData()
                    fillAnswers(indexPath: currentIndexpath, value: txtView.text)
                    
                    self.addNewRows(indexPath:IndexPath(row: 0, section: 0))
                    txtView.text = ""
                    txtView.resignFirstResponder()
                }
                
            }
            
            
        }
        //        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Keyboard Notification
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardConstraint?.constant = 0.0
            } else {
                self.keyboardConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    //MARK:- APIs
    
    func fetchQuestions(){
        
        let activityData = ActivityData(size: CGSize(width:50 , height: 50), message: "", messageFont:nil , type: .ballClipRotate, color: appColor, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.clear)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        httpClient.getRequest(withApi: APIConstants.basePath+APIConstants.getQuestions, success: { (data,res) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            let statusCode = res.response?.statusCode.toString
            guard let response = data else {
                apiResponse.failure(.none)
                return
            }
            
            let json = JSON(response)
            
            //Previously Used
            print(json["success"].stringValue)
            let responseType = apiValidation(rawValue: statusCode!) ?? .failure
            
            if responseType == apiValidation.success {
                
                if(json["success"]).boolValue{
                    
                    let myData = QuestionModel.parseArrayinToModal(withAttributes: json[APIConstants.data].arrayValue)
                    
                    
                    //Original data parsing result
                    self.totalData =  (myData as? [[QuestionModel]])!
                    
                    /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
                    //                    switch self.typeOfConversation {
                    //                    case 1:
                    //                        indexTypeOfConvo = 0
                    //                        self.arrayRideQuestions = ((self.totalData[indexTypeOfConvo]) as [QuestionModel]?)!
                    //                    case 3:
                    //                        indexTypeOfConvo = 2
                    //                        self.arrayRideQuestions = ((self.totalData[indexTypeOfConvo ]) as [QuestionModel]?)!
                    //                    case 5:
                    //                        indexTypeOfConvo = 4
                    //                        self.arrayRideQuestions = ((self.totalData[indexTypeOfConvo]) as [QuestionModel]?)!
                    //                    case 6:
                    //                        indexTypeOfConvo = 5
                    //                        self.arrayRideQuestions = ((self.totalData[indexTypeOfConvo]) as [QuestionModel]?)!
                    //                    case 7:
                    //                        indexTypeOfConvo = 6
                    //                        self.arrayRideQuestions = ((self.totalData[indexTypeOfConvo]) as [QuestionModel]?)!
                    //                    default: break
                    //                    }
                    self.arrayRideQuestions = ((self.totalData[self.typeOfConversation - 1]) as [QuestionModel]?)!
                    
                    print(self.arrayRideQuestions.count)
                    
                    if self.arrayRideQuestions.count > 0{
                        // Call get Array value here
                        self.getCellCountAndOrder(model: self.arrayRideQuestions[0])
                        self.currentArrayCount = 1
                        self.tblView.reloadData()
                    }
                    
                    return
                }else{
                    print(json["message"])
                    //                    self.isTokenExpire = false
                    //                    return
                }
            }
            else if responseType == apiValidation.failure || responseType == apiValidation.serverError{
                return
            }
                
            else if  responseType == apiValidation.invalidAccessToken {
                
                //invaild token
                //                completion(apiResponse.failure(responseType))
                return
            }
            
        }, failure: { (message) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            apiResponse.failureMessage(message)
        })
    }
    
    //MARK:- Cell Count functions
    func getCellCountAndOrder(model: QuestionModel){
        
        //0 select, 1 numeric, 2 file, 3 date, 4 radiobtn, 5 time, 6 dropdown, 7 images, 8 use_location, 9 text, 10 current_location, 11 price, 12 edition, 13 duration, 14 multiple_inputs
        
        if type6Enabled!{
            type6Count = type6Count+1
        }
        
        if(type14Enabled == true){
            
            type14Count = type14Count+1
            
            if type14Count == 1{
                cellValues.append(([(model,"+ Add stop","8")],.right,.collection))
            }else if type14Count <= 3{
                cellValues.append(([(model,"Skip","15"),(model,"+ Add another stop","8")],.right,.collection))
            }else{
                type14Enabled = false
            }
            
            return
        }
        
        if(subquestions1Exist == false){
            
            cellValues.append(([(model,model.question!,model.type!)],.left,.text))
            
            if model.have_options == "1"{
                
                if let countSub = model.sub_questions?.count, countSub > 0{
                    
                    var tempSubQuestions = [questionValueAndType]()
                    for quesModel in model.sub_questions! {
                        
                        tempSubQuestions.append((quesModel,quesModel.question!,quesModel.type!))
                        
                        if quesModel.have_options == "1"{
                            
                            if quesModel.type == "14"{
                                // Allow inputs upto 3
                                type14Enabled = true
                                tempSubQuestions.append((quesModel,"No","0")) // Manual override of No value in no. of Stops
                                break
                            }
                            
                            if let countSub1 = quesModel.sub1_questions?.count, countSub1 > 0{
                                
                                //Set bool for entering sub questions as 1
                                subquestions1Exist = true
                                continue
                                
                            }else{
                                subquestions1Exist = false
                            }
                            
                        }
                        
                    }
                    cellValues.append((tempSubQuestions,.right,.collection))
                }
                
                if (model.type! == "7"){
                    initiatePhotoSeclector(model: model)
                }
            }
        }else{
            if (subquestions2Exist == false){
                if let countSub = model.sub_questions?.count, countSub > 0{
                    
                    var index = 0
                    
                    for quesModel in model.sub_questions! {
                        
                        if type6Enabled!{
                            if quesModel.type != "6"{
                                continue
                            }
                        }else{
                            if index != subquestion1Index{
                                index = index + 1
                                continue
                            }
                        }
                        
                        
                        if quesModel.have_options == "1"{
                            
                            // current the model is not entering here
                            if let countSub1 = quesModel.sub1_questions?.count, countSub1 > 0{
                                
                                var index = 0
                                var tempSub1Questions = [questionValueAndType]()
                                
                                for qModel in quesModel.sub1_questions! {
                                    
                                    if qModel.have_options == "1"{
                                        
                                        if let countSub2 = qModel.sub2_questions?.count, countSub2 > 0{
                                            
                                            for quesM in qModel.sub2_questions! {
                                                
                                                subquestions2Exist = true
                                                break
                                            }
                                        }
                                        
                                        if type6Enabled!{
                                            if type6Count < 3{
                                                tempSub1Questions.append((qModel,qModel.question!,qModel.type!))
                                                break
                                            }else{
                                                type6Enabled = false
                                                if index == 1{
                                                    tempSub1Questions.append((qModel,qModel.question!,qModel.type!))
                                                    subquestions1Exist = false
                                                    subquestions2Exist = false
                                                    self.currentArrayCount = self.currentArrayCount! + 1
                                                    break
                                                }
                                            }
                                        }else{
                                            tempSub1Questions.append((qModel,qModel.question!,qModel.type!))
                                        }
                                        
                                    }else{
                                        subquestions2Exist = false
                                    }
                                    
                                    index = index + 1
                                }
                                cellValues.append((tempSub1Questions,.left,.text))
                                getCellCountAndOrder(model: model)
                                //Set bool for entering sub questions as 0
                            }
                        }
                        
                        index = index + 1
                    }
                }
            }else{
                
                var index = 0
                
                for quesModel in model.sub_questions! {
                    
                    print(type6Count)
                    if(type6Count == 3){
                        
                        subquestions1Exist = false
                        if index == subquestion1Index{
                            index = index + 1
                            continue
                        }
                    }else{
                        if type6Enabled!{
                            
                        }else{
                            subquestions1Exist = false
                        }
                        if index != subquestion1Index{
                            index = index + 1
                            continue
                        }
                    }
                    
                    
                    if quesModel.have_options == "1"{
                        
                        // current the model is not entering here
                        if let countSub1 = quesModel.sub1_questions?.count, countSub1 > 0{
                            
                            var index = 0
                            var tempSub2Questions = [questionValueAndType]()
                            for qModel in quesModel.sub1_questions! {
                                if qModel.have_options == "1"{
                                    
                                    if let countSub2 = qModel.sub2_questions?.count, countSub2 > 0{
                                        
                                        for quesM in qModel.sub2_questions! {
                                            
                                            if(type6Count == 3){
                                                if index != subquestion2Index{
                                                    tempSub2Questions.append((quesM,quesM.question!,quesM.type!))
                                                    break
                                                }
                                            }else{
                                                if index == subquestion2Index{
                                                    tempSub2Questions.append((quesM,quesM.question!,quesM.type!))
                                                    break
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                                index = index + 1
                                
                            }
                            cellValues.append((tempSub2Questions,.right,.collection))
                            
                            //Set bool for entering sub questions as 0
                            subquestions2Exist = false
                        }
                    }
                    
                    index = index + 1
                }
            }
            
        }
        
    }
    
    func returnNewCount(currentCount: Int, type: String) -> Int{
        
        return 0
    }
    
    
    
    
}

//MARK:- TableView

extension AddAdViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellValue = self.cellValues[indexPath.row]
        
        if cellValue.position == .left {
          
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostAdLeftTextTableViewCell" , for: indexPath) as? PostAdLeftTextTableViewCell  else {return UITableViewCell() }
            cell.lblLeftText.text = cellValue.quesValueAndType[0].value
          
            return cell
        }
        else{
           
            if cellValue.type == .text{
              
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostAdRightTextTableViewCell" , for: indexPath) as? PostAdRightTextTableViewCell  else {return UITableViewCell() }
                cell.lblRightText.text = cellValue.quesValueAndType[0].value
             
                return cell
           
            }
            else if cellValue.type == .collection{
               
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewTableViewCell" , for: indexPath) as? CollectionViewTableViewCell  else {return UITableViewCell() }
                
                arrayTexts = cellValue.quesValueAndType
                cell.collectionView.reloadData()
                
                //To stop collection views from being selected again
               
                if indexPath.row == cellValues.count - 1{
                   
                    cell.isUserInteractionEnabled = true
                }
                else{
                  
                    cell.isUserInteractionEnabled = false
                }
                return cell
            }
            else{
               
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImgCollectionViewTableViewCell" , for: indexPath) as? ImgCollectionViewTableViewCell  else {return UITableViewCell() }
                
                arrayTexts = cellValue.quesValueAndType
                cell.arrayImgs = arrayImages
                cell.collectionView.reloadData()
            
                return cell
          
            }
        }
        
        //            cell.lblLeftText.text = "Hi \(UserDataSingleton.sharedInstance.loggedInUser?.firstname), I’m your virtual assistant Mason. I’ll help you post your advertisement."
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cellValue = self.cellValues[indexPath.row]
        
        if cellValue.position == .right{
            if cellValue.type == .collection{
                return 80 // 16+48+16
            }else if cellValue.type == .collectionImgs{
                return 160
            }
        }else{
            return UITableViewAutomaticDimension
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}


//MARK:- CollectionView

extension AddAdViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let stringSize = arrayTexts[indexPath.row].value.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)])
        print(stringSize.width)
        //60 intent value [System to Gotham conversion] //White space issue
        return CGSize(width: stringSize.width + 60, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -10 // Set after checking for multiple devices
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayTexts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostAdCollectionViewCell", for: indexPath as IndexPath) as! PostAdCollectionViewCell
        cell.lbl.text = arrayTexts[indexPath.row].value
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        
        //Value to be entered as answer
        let value:String =  arrayTexts[indexPath.item].value
        
        if (value == "No"){
            self.arrayRideQuestions[4].sub_questions?[1].answer = ["No"]
        }
        
        if (value == "Discard"){
            //Pop Controller here
            self.dismiss(animated: true, completion: nil)
            arrayTexts[indexPath.item].type = "15"
        }else if(value == "Publish"){
            
           // self.publish()
            self.fetchProfileData()
            //_ = self.dismiss(animated: true, completion: nil)
            
        }else{
            if subquestions1Exist!{
                subquestion1Index = indexPath.item
            }
            
            if subquestions2Exist!{
                subquestion2Index = indexPath.item
            }
            
            updateTableView(indexPath: indexPath)
        }
        
        
    }
    
}

//MARK:- TableView Updates and additions
extension AddAdViewController{
    
    func fillAnswers(indexPath: IndexPath, value: String){
        let tempModel = arrayTexts[indexPath.item].model
        let type = arrayTexts[indexPath.item].type
        var mainID = ""
        let sub_questions_id = tempModel.sub_questions_id
        let sub2_questions_id = tempModel.sub2_questions_id
        
        if (sub_questions_id != nil){
            mainID = sub_questions_id!
            
            for question in self.arrayRideQuestions{
                if (question.sub_questions?.count)! > 0{
                    for subquestion in question.sub_questions!{
                        print(subquestion.sub_questions_id ?? "")
                        
                        if (subquestion.sub_questions_id == mainID){
                            subquestion.answer = [value]
                        }
                        
                        if(subquestion.question == "Yes (Input up to 3 stops)"){
                            subquestion.answer = arrayStops
                        }else if(subquestion.question == "No"){
                            if (subquestion.sub_questions_id == mainID){
                                subquestion.answer = ["No"]
                            }
                        }
                    }
                }
            }
            
        }else if (sub2_questions_id != nil){
            mainID = sub2_questions_id!
            
            for question in self.arrayRideQuestions{
                if (question.sub_questions?.count)! > 0{
                    for subquestion in question.sub_questions!{
                        if (subquestion.sub1_questions?.count)! > 0{
                            for sub1question in subquestion.sub1_questions!{
                                if (sub1question.sub2_questions?.count)! > 0{
                                    for sub2question in sub1question.sub2_questions!{
                                        print(sub2question.sub2_questions_id ?? "")
                                        if(sub2question.sub2_questions_id == mainID){
                                            sub2question.answer = [value]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func updateTableView(indexPath: IndexPath){
        //0 select, 1 numeric, 2 file, 3 date, 4 radiobtn, 5 time, 6 dropdown, 7 images, 8 use_location, 9 text, 10 current_location, 11 price, 12 edition, 13 duration, 14 multiple_inputs
        
        var value = ""
        let type:Int =  Int(arrayTexts[indexPath.item].type)!
        let model = arrayTexts[indexPath.item].model
        let indexTappedInModel = indexPath.item
        
        currentModel = model
        currentIndexpath = indexPath
        viewTextView.isHidden = true
        
        
        //self.arrayRideQuestions[self.currentArrayCount!].sub_questions[indexTappedInModel]
        //MARK::_ keyboard type set
        switch type {
        case 0:
            if type14Enabled!{
                type14Enabled = false
                self.currentArrayCount = self.currentArrayCount! + 1
            }
            
            value = arrayTexts[indexPath.item].value
            
            if cellValues.count > 0{
                cellValues[cellValues.count - 1] = ([(model,value,"0")],.right,.text)
                tblView.reloadData()
                fillAnswers(indexPath: indexPath, value: value)
            }
            addNewRows(indexPath: indexPath)
        case 1:
            //Open Keyboard with numeric
            viewTextView.isHidden = false
            if adType == "Ticket" {
                txtView.placeholder = "No.of tickets(MAX 10)..."
            }
            txtView.keyboardType = .numberPad
            txtView.becomeFirstResponder()
            return
        case 2:
            //Does not exist
            return
        case 3:
            //Open Date Picker
            openDatePicker(self,model: model)
        case 4:
            //Radio do nothing
//            txtView.placeholder = "Input row/section..."
            if subquestions1Exist! || subquestions2Exist!{
                value = arrayTexts[indexPath.item].value
                if cellValues.count > 0{
                    cellValues[cellValues.count - 1] = ([(model,value,"0")],.right,.text)
                    tblView.reloadData()
                    fillAnswers(indexPath: indexPath, value: value)
                }
                addNewRows(indexPath: indexPath)
            }
            
            return
        case 5:
            //Open Keyboard to enter duration
            viewTextView.isHidden = false
            
            txtView.keyboardType = .default
            txtView.becomeFirstResponder()
            return
        case 6:
            //Drop Down figure out later
            type6Enabled = true
            if subquestions1Exist! || subquestions2Exist!{
                value = arrayTexts[indexPath.item].value
                if cellValues.count > 0{
                    cellValues[cellValues.count - 1] = ([(model,value,"0")],.right,.text)
                    tblView.reloadData()
                    fillAnswers(indexPath: indexPath, value: value)
                }
                addNewRows(indexPath: indexPath)
            }
            return
        case 7:
            //Open multiselect image picker figure out later
            initiatePhotoSeclector(model: model)
            return
        case 8:
            //Open google places picker
            openPlacePicker()
            value = googlePlaceSelected
        case 9:
            //Open Keyboard for normal text
            value = arrayTexts[indexPath.item].value
            viewTextView.isHidden = false
            if (value == "Input row/section number"){
                txtView.placeholder = "input row/section"
            }else{
                txtView.placeholder = "\(firstTypeDialogue ?? "optional") name..." as NSString
            }
            txtView.keyboardType = .default
            txtView.becomeFirstResponder()
            return
        case 10:
            //Get current location place
            value = getCurrentLocation(model: model)
            
        case 11:
            //Open Keyboard with numeric price
            viewTextView.isHidden = false
            txtView.placeholder = "Price($)..."
            txtView.keyboardType = .decimalPad
            txtView.becomeFirstResponder()
            return
        case 12:
            //Open Keyboard with numeric edition
            viewTextView.isHidden = false
            txtView.placeholder = "Edition..."
            txtView.keyboardType = .default
            
            //txtView.keyboardType = .numberPad
            txtView.becomeFirstResponder()
            return
        case 13:
            //Open Keyboard with numeric duration in mins
            if adType == "Service" {
                txtView.placeholder = "Duration(MAX 12 hrs)..."
            }
            else {
                txtView.placeholder = "Duration(MAX 5 days)..."
            }
            viewTextView.isHidden = false
            
            txtView.keyboardType = .numberPad
            txtView.becomeFirstResponder()
            return
        case 14:
            //Type 14 unique case of multiple choices possible
            
            value = arrayTexts[indexPath.item].value
            if cellValues.count > 0{
                cellValues[cellValues.count - 1] = ([(model,value,"0")],.right,.text) //-----------------------
                tblView.reloadData()
                fillAnswers(indexPath: indexPath, value: value)
            }
            addNewRows(indexPath: indexPath)
            
            return
        case 15:
            //Skip
            if adType == "Ticket" {
                txtView.placeholder = "input row/section"
            }
            
            if type14Enabled!{
                type14Enabled = false
                self.currentArrayCount = self.currentArrayCount! + 1
            }
            if cellValues.count > 0{
                cellValues.remove(at: cellValues.count - 1)
                tblView.reloadData()
            }
            addNewRows(indexPath: indexPath)
        default:
            return
        }
        
        
        //        if cellValues.count > 0{
        //            cellValues[cellValues.count - 1] = ([(value,"0")],.right,.text)
        //            tblView.reloadData()
        //        }
    }
    
    func addNewRows(indexPath: IndexPath){
//                self.tblView.beginUpdates()
        
        //        print(currentArrayCount)
        getCellCountAndOrder(model: self.arrayRideQuestions[currentArrayCount!])
        
        var arrayOfItemsAdded = [IndexPath(row: cellValues.count-2, section: 0),IndexPath(row: cellValues.count-1, section: 0)]
        
        if type14Enabled == true{
            // Let it automatically pick values from static parameters
            if(type14Count == 0){
                arrayOfItemsAdded = [IndexPath(row: cellValues.count-2, section: 0),IndexPath(row: cellValues.count-1, section: 0)]
            }else{
                arrayOfItemsAdded = [IndexPath(row: cellValues.count-1, section: 0)]
            }
            
            if(type14Count > 1){
                if(indexPath.item == 0){
                    type14Enabled = false // In case of Skip Tapped
                    self.currentArrayCount = self.currentArrayCount! + 1
                }else{
                    //Another Stop picked
                }
            }
        }else if type6Enabled!{
            arrayOfItemsAdded = [IndexPath(row: cellValues.count-2, section: 0),IndexPath(row: cellValues.count-1, section: 0)]
        }else{
            if(subquestions1Exist == true){
                //Let it be the same question and donot update currentArrayCount here
            }else{
                if (type14Count == 4){
                    self.currentArrayCount = self.currentArrayCount! + 1
                    getCellCountAndOrder(model: self.arrayRideQuestions[currentArrayCount!])
                    type14Count = 0
                    self.currentArrayCount = self.currentArrayCount! + 1
                    
                }else{
                    self.currentArrayCount = self.currentArrayCount! + 1
                }
            }
        }
        
//                self.tblView.insertRows(at: arrayOfItemsAdded, with: .none)
//                self.tblView.endUpdates()
        
        tblView.reloadData()
        
        let indexPath = IndexPath(row: cellValues.count-1, section: 0)
        self.tblView.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
}

//MARK:- Location Delegates

extension AddAdViewController: GMSAutocompleteViewControllerDelegate {
    
    func getCurrentLocation(model: QuestionModel) -> String{
        let placeValue = UserDefaults.standard.value(forKey: userPrefrences.currentCity.rawValue) as? String! ?? ""
        DispatchQueue.main.async {
            if self.cellValues.count > 0{
                self.cellValues[self.cellValues.count - 1] = ([(self.currentModel,placeValue ?? "","0")],.right,.text)
                self.tblView.reloadData()
                self.fillAnswers(indexPath: self.currentIndexpath, value: placeValue ?? "")
                self.addNewRows(indexPath:IndexPath(row: 0, section: 0))
            }
        }
        
        return placeValue ?? ""
    }

    
    func openPlacePicker(){
        
        let acController = GMSAutocompleteViewController()
        
        let neBoundsCorner = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!,
                                                    longitude: (locationManager.location?.coordinate.longitude)!)
        
        let swBoundsCorner = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude.abs)! + 1,
                                                    longitude: (locationManager.location?.coordinate.longitude.abs)! + 1)
        
        let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner,
                                         coordinate: swBoundsCorner)
        
        acController.autocompleteBounds = bounds
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)

        
      
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        
        arrayStops.append(place.name)
        
        googlePlaceSelected = place.name
        if cellValues.count > 0{
            cellValues[cellValues.count - 1] = ([(currentModel ,googlePlaceSelected,"0")],.right,.text)
            tblView.reloadData()
            fillAnswers(indexPath: currentIndexpath, value: googlePlaceSelected)
            if !type14Enabled!{
                arrayStops = []
            }
        }
        
        dismiss(animated: true, completion: nil)
        
        addNewRows(indexPath:IndexPath(item: 1, section: 0))
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension AddAdViewController:CLLocationManagerDelegate{
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
}

//MARK:- Select Functions

extension AddAdViewController{
    func openDatePicker(_ sender : Any, model: QuestionModel){
        
        let datePicker = ActionSheetDatePicker(title: "Pick a Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date!, doneBlock: {
            picker, value, index in
            
            
            //            picker?.minimumDate = Date()
            //dd-MMM-YYYY
            //yyyy-mm-dd
            
            //print("value = \(value)")
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: value as! Date)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
            let myDateString = dateFormatter.string(from: value! as! Date)
            print(myDateString)
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd/mm/yy"
            let showDate = inputFormatter.date(from: myDateString)
            inputFormatter.dateFormat = "yyyy-MM-dd"
    
            
     //       let resultString = inputFormatter.string(from: showDate!)
            
            let resultString = inputFormatter.string(from: value! as! Date )

            
            
            print(resultString)
            
            //            self.txtFieldDOB.text = resultString
            self.dateSelected = resultString
            if self.cellValues.count > 0{
                self.cellValues[self.cellValues.count - 1] = ([(model,resultString,"0")],.right,.text)
                self.tblView.reloadData()
                self.fillAnswers(indexPath: self.currentIndexpath, value: resultString)
                
            }
            self.addNewRows(indexPath:IndexPath(row: 0, section: 0))
            
        }, cancel: { ActionStringCancelBlock in return },origin: (self.view))
        
        //        ,origin: (sender as AnyObject).superview!?.superview)
        
        //datePicker?.maximumDate = NSDate() as Date!
        datePicker?.minimumDate = Date()
        datePicker?.setTextColor(appColor)
        datePicker?.pickerBackgroundColor = UIColor.white
        datePicker?.toolbarBackgroundColor = UIColor.white
        datePicker?.toolbarButtonsColor = appColor
        
        datePicker?.show()
    }
    
    func openTimePicker(_ sender : Any){
        
        let datePicker = ActionSheetDatePicker(title: "Pick a Date", datePickerMode: UIDatePickerMode.time, selectedDate: NSDate() as Date!, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            self.dateSelected = (value as! String?)!
            
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        
        //        datePicker?.maximumDate = NSDate() as Date!
        
        datePicker?.minuteInterval = 20
        
        datePicker?.setTextColor(appColor)
        datePicker?.pickerBackgroundColor = UIColor.white
        datePicker?.toolbarBackgroundColor = UIColor.white
        datePicker?.toolbarButtonsColor = appColor
        
        datePicker?.show()
    }
}
//MARK::- TextView
extension AddAdViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
     //   textView.becomeFirstResponder()
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
         
            self.viewTextView.isHidden = true
            textView.text = ""
            textView.resignFirstResponder()
        
            return false
    
        }
        
//        let stringLength = textView.text.characters.count
//        
//        let type:Int =  Int(arrayTexts[currentIndexpath.item].type)!
//        if type == 11{//Price
//            //textView.text = "$ " + textView.text
//            textView.keyboardType = .numberPad
//            if stringLength > 5{
//                return false
//            }
//        }else if (type == 12){//Edition
//            if stringLength > 5{
//                return false
//            }
//        }else if (type == 13){//Duration
//            
//            let duration = textView.text.toInt() ?? 13
//            if duration >= 5 {
//                return false
//            }
//            //            if stringLength > 5{
//            //                return false
//            //            }
//        }else{//Text
//            
//        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
//        if self.cellValues.count > 0{
//            self.viewTextView.isHidden = true
//            
//            //0 select, 1 numeric, 2 file, 3 date, 4 radiobtn, 5 time, 6 dropdown, 7 images, 8 use_location, 9 text, 10 current_location, 11 price, 12 edition, 13 duration, 14 multiple_inputs
//            let type:Int =  Int(arrayTexts[currentIndexpath.item].type)!
//            var textDisplayed = ""
//            if type == 11{
//                textDisplayed = "$ " + textView.text
//            }else if (type == 13){
//                //adType to check the duration variation item also ticket also
//                // no.of tickets = 10 and duration of journey in ride not more than 12
//                if adType == "Ticket" {
//                    
//                }
//                textDisplayed = textView.text + " days"
//            }else{
//                textDisplayed = textView.text
//            }
//            
//            self.cellValues[self.cellValues.count - 1] = ([(currentModel ,textDisplayed,"0")],.right,.text)
//            self.tblView.reloadData()
//            fillAnswers(indexPath: currentIndexpath, value: textView.text)
//            
//        }
//        self.addNewRows(indexPath:IndexPath(row: 0, section: 0))
    }
    
    
}

extension AddAdViewController{
    
    func initiatePhotoSeclector(model: QuestionModel){
        PHPhotoLibrary.requestAuthorization { (status) in
            
        }
        
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 6
        vc.takePhotos = true
        
        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            print("Selected: \(asset)")
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            
            if assets.count > 0{
                for asset in assets{
                    self.arrayImages.append(self.getAssetThumbnail(asset: asset))
                }
                
                DispatchQueue.main.async {
                    
                    //                    self.cellValues.insert(([(self.currentModel ,"0","0")],.right,.collectionImgs), at: self.cellValues.count)
                    self.cellValues[self.cellValues.count - 1] = ([(self.currentModel ,"0","0")],.right,.collectionImgs)
                    self.tblView.reloadData()
                    self.fillAnswers(indexPath: self.currentIndexpath, value: "")
                    
                    let indexPath = IndexPath(row: self.cellValues.count - 1, section: 0)
                    self.tblView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    self.addNewRows(indexPath: indexPath)
                }
            }
            
        }, completion: nil)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 400, height: 400), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
            //            self.imgView.image = thumbnail
        })
        return thumbnail
    }
}

extension String {
    func chopPrefix(_ count: Int = 1) -> String {
        return substring(from: index(startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return substring(to: index(endIndex, offsetBy: -count))
    }
}
//MARK:- Deny PERMISSION
extension AddAdViewController {
    func fetchProfileData()
    {
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfile(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            switch response {
            case .success(let data):
                
                weak var weakSelf  = self
                print(data ?? "")
                let x = data as? User
                print(x?.access_token ?? "")
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                self.checkStripeAccount()
            case .failureMessage(let message):
                print(message ?? "")
                UtilityFunctions.showAlert(message: message, controller: self)
                
            default : break
            }
        }
        
    }
}
