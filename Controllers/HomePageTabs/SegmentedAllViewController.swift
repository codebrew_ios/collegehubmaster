//
//  SegmentedAllViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 25/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class SegmentedAllViewController: UIViewController {

    //MARK:- Variables
    let segmentControllerHome = SJSegmentedViewController()
    var selectedSegment: SJSegmentTab?
    var textSearched: String?
    
    //MARK:- Outlets
    @IBOutlet weak var containerViewForSegmentedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("new container")
        UserDefaults.standard.set(false, forKey: "headerViewConstraintToBeAdjusted")
        segmentView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        UserDefaults.standard.set(false, forKey: "headerViewConstraintToBeAdjusted")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK::- Functions
    func segmentView(){
        guard let headerVC = self.storyboard?.instantiateViewController(withIdentifier: "HeaderViewController") as? HeaderViewController else { return }
        let homeStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        guard let child_1 = self.storyboard?.instantiateViewController(withIdentifier: "PopularNearbyViewController") as? PopularNearbyViewController else{ return  }
        child_1.title = " Popular Nearby "
        
        //below delegate is fired to make the popular nearby view all btn functionality
        child_1.delegate = self
        
        guard let child_2 = homeStoryboard.instantiateViewController(withIdentifier: "HomePageCarpoolViewController") as? HomePageCarpoolViewController else{ return  }
        child_2.title = "Carpool"
        guard let child_3 = homeStoryboard.instantiateViewController(withIdentifier: "HomePageServiceViewController") as? HomePageServiceViewController else{ return }
        child_3.title = "Services"
        guard let child_4 = homeStoryboard.instantiateViewController(withIdentifier: "HomePageTicketViewController") as? HomePageTicketViewController else{ return }
        child_4.title = "Tickets"
        guard let child_5 = homeStoryboard.instantiateViewController(withIdentifier: "HomePageBookViewController") as? HomePageBookViewController else{ return }
        child_5.title = "Books"
        guard let child_6 = homeStoryboard.instantiateViewController(withIdentifier: "HomePageItemViewController") as? HomePageItemViewController else{ return }
        child_6.title = "Items"
        
        segmentControllerHome.headerViewController = headerVC
        segmentControllerHome.segmentControllers = [child_1,child_2,child_3,child_4,child_5,child_6]
        segmentControllerHome.headerViewHeight = 80
        segmentControllerHome.segmentViewHeight = 48
        segmentControllerHome.segmentTitleColor = UIColor(red: 179/255.0, green: 179/255.0, blue: 179/255.0, alpha: 1.0)
        segmentControllerHome.selectedSegmentViewColor = appColor
        segmentControllerHome.segmentShadow = SJShadow.light()
        segmentControllerHome.delegate = self
        containerViewForSegmentedView.addSubview(segmentControllerHome.view)
        segmentControllerHome.view.frame = self.containerViewForSegmentedView.bounds
    }    
}

//MARK::- Delegate SegmentController
extension SegmentedAllViewController: SJSegmentedViewControllerDelegate {
    
    func didSelectSegmentAtIndex(_ index: Int) {
        print(index)
        
    }
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        print(index)
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "TabChanged"),
            object: index,
            userInfo: nil)
        
        let searchKeyNull = UserDefaults.standard.value(forKey: "SearchKeyNull") as! Bool
        if searchKeyNull == true{
            switch index {
            case 0:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "PopularData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            case 1:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "CarpoolData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            case 2:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "ServicesData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            case 4:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "BooksData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            case 3:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "TicketsData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            case 5:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "ItemsData"),
                    object: nil,
                    userInfo:["searchingText": "", "selectedIndex" : index])
            default:
                return
            }
        }else{
            UserDefaults.standard.set(true, forKey: "SearchKeyNull")
        }
        
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(UIColor(red: 179/255.0, green: 179/255.0, blue: 179/255.0, alpha: 1.0))
            selectedSegment?.titleFont(UIFont(name: "Gotham-Book", size: 13.0)!)
        }
        if segmentControllerHome.segments.count > 0 {
            selectedSegment = segmentControllerHome.segments[index]
            selectedSegment?.titleColor(appColor)
            selectedSegment?.titleFont(UIFont(name: "Gotham-Bold", size: 13.0)!)
        }
    }

}

//MARK::- CustomDelegate MoveSegmentController
extension SegmentedAllViewController: DelegateMoveSegmentTo {
    func moveSegement(index: Int) {
        switch index {
        case 0:
            segmentControllerHome.setSelectedSegmentAt(1, animated: true)
        case 1:
            segmentControllerHome.setSelectedSegmentAt(2, animated: true)
        case 2:
            segmentControllerHome.setSelectedSegmentAt(3, animated: true)
        case 3:
            segmentControllerHome.setSelectedSegmentAt(4, animated: true)
        case 4:
            segmentControllerHome.setSelectedSegmentAt(5, animated: true)
        case 5:
            segmentControllerHome.setSelectedSegmentAt(6, animated: true)
        default:
            segmentControllerHome.setSelectedSegmentAt(0, animated: true)
        }

    }
}
