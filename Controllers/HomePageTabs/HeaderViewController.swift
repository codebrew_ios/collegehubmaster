//
//  HeaderViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

protocol DelegateShowSheet: class {
    func showSheet()
}

class HeaderViewController: UIViewController {
    
    //MARK: Variables
    var temp = ""
    var vcSelected = "PopularData"
    var delegate: DelegateShowSheet?
    var selectedIndexHeader = 0
    var selectedType = ""
    var bookVC : HomePageBookViewController?

    @IBOutlet weak var txtSearchBar: UITextField!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchBar.delegate = self
        txtSearchBar.returnKeyType = UIReturnKeyType.done

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HeaderViewController.refreshSearchBar),
                                               name: NSNotification.Name(rawValue: "TabChanged"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HeaderViewController.initialiseWithSearchText),
                                               name: NSNotification.Name(rawValue: "SearchTextInPopularNearby"),
                                               object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "headerViewConstraintToBeAdjusted") as! Bool{
            topConstraint.constant = 25
        }else{
            topConstraint.constant = 16
        }
    }
    
//MARK::- Functions
    func initialiseWithSearchText(not: Notification) {
        
        if let text = not.object as? String {
            
            switch selectedIndexHeader {
            case 0:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "PopularData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            case 1:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "CarpoolData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            case 2:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "ServicesData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            case 4:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "BooksData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            case 3:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "TicketsData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            case 5:
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "ItemsData"),
                    object: nil,
                    userInfo:["searchingText": text, "selectedIndex" : selectedIndexHeader])
            default:
                return
            }
        }
    }
    
    func refreshSearchBar(not: Notification) {
        if let selectedIndex = not.object as? Int {
            selectedIndexHeader = selectedIndex
        }
        else {
            print("No index passed")
        }
        
        txtSearchBar.text = ""
    }
}

//MARK::- TextFieldDelegates
extension HeaderViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        txtSearchBar.resignFirstResponder()
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(string != ""){
            temp = (textField.text).unwrap() + string
        }else {
            temp = String(textField.text.unwrap().characters.dropLast())
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textToPass = textField.text else { return }

        if selectedIndexHeader == 0 && textToPass.length != 0 {
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "EditingEnded"),
                object: textToPass
                )
        }else{
            print("Not in popular nearby view controller.")
        }
        
        switch selectedIndexHeader {
        case 0:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "PopularData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        case 1:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "CarpoolData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        case 2:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ServicesData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        case 4:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "BooksData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        case 3:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "TicketsData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        case 5:
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ItemsData"),
                object: nil,
                userInfo:["searchingText": textToPass, "selectedIndex" : selectedIndexHeader])
        default:
            return
        }
    }
}


