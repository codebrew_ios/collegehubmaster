//
//  HomePageBookViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 06/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SDWebImage
import PullToRefreshKit
import EZSwiftExtensions
import NVActivityIndicatorView

class HomePageBookViewController: UIViewController {
    
    //MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
    
    //MARK::- VC VARIABLS
    var posts_id = ""
    var page = 1
    var arrayBooks = [BookPostViewModel]()
    var searchKey = ""
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    var headerSearch : HeaderViewController?
    
    //takeOver--------------------------------------
    var temp = ""
    var viewCalled = ""
    
    let cardDetailVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    
    //MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoResultFound.isHidden = true
        tblView.isHidden = false
        tblView.delegate = self
        tblView.dataSource = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomePageBookViewController.initialise),
                                               name: NSNotification.Name(rawValue: "BooksData"),
                                               object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "BooksData"),
                                                  object: nil)
    }
    
    
    //MARK::- FUNCTIONS
    func initialise(not: Notification){
        
        pullToRefresh()
        pullToRefreshUpward()
        page = 1
        temp = ""
        
        lblNoResultFound.isHidden = true
        tblView.isHidden = false
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        tblView?.rowHeight = UITableViewAutomaticDimension;
        tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        
        if let myDict = not.userInfo as? [String: Any] {
            if let typeIndex = myDict["selectedIndex"] as? Int {
                if typeIndex == 4 {
                    if let textValue = myDict["searchingText"] as? String {
                        temp = textValue
                        if textValue.length > 0 {
                            fetchViewPostBooks(searchKey: textValue)
                        }else{
                            fetchViewPostBooks(searchKey: "")
                        }
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchViewPostBooks(searchKey: temp)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        temp = ""
    }
    
    //MARK::- BTN ACTIONS
    @IBAction func btnSellABookAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let addAdVc = storyboard.instantiateViewController(withIdentifier: "AddAdViewController") as? AddAdViewController else {return}
        addAdVc.typeOfConversation = 5
        addAdVc.adType = "Book"
        self.presentVC(addAdVc)
        
        /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
    }
    
    
    
}

//MARK::- TABLE VIEW DELEGATES
extension HomePageBookViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBooks.count
    }
    func tableView(_ tableView: UITableView, commiteditingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        posts_id = (UserDataSingleton.sharedInstance.booksData?[indexPath.row].posts_id).unwrap()
        let favouriteAction = UITableViewRowAction(style: .normal, title: "                ") { (action:UITableViewRowAction! , indexPath:IndexPath! ) -> Void in
            print(self.posts_id)
            self.addingPostToFavourite(posts_id: self.arrayBooks[indexPath.row].posts_id, row: indexPath.row)
            
            
        }
        
        favouriteAction.backgroundColor = UIColor.clear
        
        return [favouriteAction]
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.sellABookCell.rawValue , for: indexPath) as? HomeScreenBookTableViewCell  else {return UITableViewCell() }
        var model = arrayBooks[indexPath.row]
        //        if temp != ""  {
        //            model = filteredMessages[indexPath.row]
        //        } else {
        //            model = arrayBooks[indexPath.row]
        //        }
        
        cell.lblUserName.text = model.firstname.unwrap() + " " + model.lastname.unwrap()
        cell.selectionStyle = .none
        if(model.is_fixed == "2"){
        cell.lblPriceOrBid.text = "Bid"
        }
        else {
            cell.lblPriceOrBid.text = "Price"
        }
        cell.lblPrice.text = ("$ \(model.price?.toDouble() ?? 0.0)")
        cell.lblTitleOfBook.text = model.name.unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: model.image100.unwrap()))
        cell.lblRating.text = String(format:"%@  ",model.avg_rating.unwrap())
        cell.productImages = (model.image) ?? []
        
        if model.is_favourite == "0" {
            
            cell.lblAddTo.text = "Add to"
            cell.lblFavorites.text = "favourites"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart_white")
            
            
            //favouriteAction.title = "\nAdd to \nfavourites"
        }
        else {
            
            cell.lblAddTo.text = "Remove"
            cell.lblFavorites.text = "favourite"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("The selected row has index: \(indexPath.row)")
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")
        
        guard let cardDetailVc = cardDetailVc else { return }
        cardDetailVc.cardIndex = indexPath.row
        cardDetailVc.currentPage = "Books"
        cardDetailVc.currentDataFetched = arrayBooks
        cardDetailVc.amounts = arrayBooks[indexPath.row].price ?? "no price"
        cardDetailVc.postId = arrayBooks[indexPath.row].posts_id ?? "no post id"
        cardDetailVc.usersId = arrayBooks[indexPath.row].users_id ?? "no user id"
        cardDetailVc.isFixed = arrayBooks[indexPath.row].is_fixed?.toInt() ?? 100
        cardDetailVc.createdOn = arrayBooks[indexPath.row].createdAt.unwrap()
        cardDetailVc.duration = arrayBooks[indexPath.row].duration
        cardDetailVc.minBid = arrayBooks[indexPath.row].minBid.unwrap()
        cardDetailVc.maxBid = arrayBooks[indexPath.row].maxBid.unwrap()
        
        /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
        if let type = arrayBooks[indexPath.row].type {
            cardDetailVc.typeToPass = type.toInt() ?? 100
        }
        
        ez.runThisInMainThread {
            self.presentVC(cardDetailVc)
        }
        
    }
    func pullToRefresh(){
        print(temp)
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchViewPostBooks(searchKey: (self?.temp)!)
        }
    }
    func pullToRefreshUpward(){
        print(temp)
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchViewPostBooks(searchKey: (self?.temp)!)
        }
       
    }

}

//MARK:- API CALLS
extension HomePageBookViewController {
    
    func addingPostToFavourite(posts_id:String?, row: Int)
    {
        UtilityFunctions.startColoredLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.addToFavourite(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message:(message?.message ?? ""), preferredStyle: .alert)
                    print(message?.message ?? "")
                    alert.view.tintColor = appColor
                    alert.view.backgroundColor = UIColor.white
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction) in
                        if self.arrayBooks[row].is_favourite == "0" {
                            self.arrayBooks[row].is_favourite = "1"
                            
                        }
                        else {
                            self.arrayBooks[row].is_favourite = "0"
                            
                        }
                        self.tblView.reloadData()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
    }
    
    func fetchViewPostBooks(searchKey: String){
        
//        if(tempRefresh != "Header" && tempRefresh != "Footer"){
//            UtilityFunctions.startColoredLoader()
//        }
        let type = "5"
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewPosts(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page: page.toString, type: type, search_key: searchKey)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                print(data ?? "")
                guard let map = data as? [BookPostViewModel] else { return }
                if (self.temp == "") {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                    
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayBooks.append(contentsOf: map)
                    case "Header":
                        self.arrayBooks = []
                        self.arrayBooks = map
                    default :
                        //self.arrayBooks = []
                        self.arrayBooks = map
                    }
                }else {
                    
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayBooks.append(contentsOf: map)
                    case "Header":
                        self.arrayBooks = []
                        self.arrayBooks = map
                    default :
                        //self.arrayBooks = []
                        self.arrayBooks = map
                    }
                }
                
                if self.arrayBooks.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                
                self.tempRefresh = ""
                self.tblView.delegate = self
                self.tblView.dataSource = self
                self.tblView.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
                
            }
        }
    }
    
}
