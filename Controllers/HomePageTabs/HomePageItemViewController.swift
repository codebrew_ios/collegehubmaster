//
//  HomePageItemViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 06/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import SJSegmentedScrollView
import EZSwiftExtensions
import NVActivityIndicatorView
var viewName = ""

class HomePageItemViewController: UIViewController {
    //MARK::- OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
    
    //MARK::- VARIABLES
    var posts_id = ""
    var page = 1
    var arrayItems = [OtherPostViewModel]()
    var searchKey = ""
    //takeOver--------------------------------------
    var temp = ""//edited
    var filteredMessages = [OtherPostViewModel]()//edited
    var textSearchFetch = ""
    var tempRefresh = ""
    var viewCalled = ""
    var loaderLoadFirstTime = 0
    
    
    let cardDetailVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    
    //MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomePageItemViewController.initialise),
                                               name: NSNotification.Name(rawValue: "ItemsData"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ItemsData"),
                                                  object: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        temp = ""
    }
    
    //MARK::- FUNCTIONS
    func initialise(not: Notification){
        
        pullToRefresh()
        pullToRefreshUpward()
        page = 1
        temp = ""
        
        lblNoResultFound.isHidden = true
        tblView.isHidden = false
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        tblView?.rowHeight = UITableViewAutomaticDimension;
        tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        
        if let myDict = not.userInfo as? [String: Any] {
            if let typeIndex = myDict["selectedIndex"] as? Int {
                if typeIndex == 5 {
                    if let textValue = myDict["searchingText"] as? String {
                        temp = textValue
                        if textValue.length > 0 {
                            fetchViewPostItem(searchText: textValue)
                        }else{
                            fetchViewPostItem(searchText: "")
                        }
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchViewPostItem(searchText: temp)
    }
    
    //MARK::- BTN ACTIONS
    //MARK::- sell functionality
    @IBAction func btnSellAItemAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addAdVc = storyboard.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 7
        addAdVc.adType = "Item"
        self.presentVC(addAdVc)
    }
    
}

extension HomePageItemViewController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        print("sadsa")
    }
}

//MARK::- TABLE VIEW DELEGATES
extension HomePageItemViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, commiteditingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //used to delete row of table view
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        posts_id = (UserDataSingleton.sharedInstance.othersData?[indexPath.row].posts_id).unwrap()
        let favouriteAction = UITableViewRowAction(style: .normal, title: "                ") { (action:UITableViewRowAction! , indexPath:IndexPath! ) -> Void in
            self.addingPostToFavourite(posts_id: self.arrayItems[indexPath.row].posts_id, row: indexPath.row)
            print("adding")
            print(self.posts_id)
        }
        
        favouriteAction.backgroundColor = UIColor.clear
        return [favouriteAction]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.sellAItemCell.rawValue , for: indexPath) as? HomeScreenItemTableViewCell  else {return UITableViewCell() }
        let model = arrayItems[indexPath.row]
        //        if temp != ""  {
        //            model = filteredMessages[indexPath.row]
        //        } else {
        //            model = arrayItems[indexPath.row]
        //        }
        
        cell.lblItemTitle.text = model.name.unwrap()
        cell.lblUserName.text = model.firstname.unwrap() + " " + model.lastname.unwrap()
        cell.selectionStyle = .none
        cell.lblPrice.text = ("$ \(model.price?.toDouble() ?? 0.0)")
        if(model.is_fixed == "2"){
            cell.lblPriceOrBid.text = "Bid"
        }
        else {
            cell.lblPriceOrBid.text = "Price"
        }
        cell.imgProfileImage?.sd_setImage(with: URL(string: model.image100.unwrap()))
        cell.lblRating.text = String(format:"%@  ",model.avg_rating.unwrap())
        cell.productImages = model.image ?? []
        
        if model.is_favourite == "0" {
            
            cell.lblAddTo.text = "Add to"
            cell.lblFavorites.text = "favourites"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart_white")
            
            
            //favouriteAction.title = "\nAdd to \nfavourites"
        }else {
            
            cell.lblAddTo.text = "Remove"
            cell.lblFavorites.text = "favourite"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart")
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("The selected row has index: \(indexPath.row)")
        //MARK:_ DElegate fires
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        
        guard let cardDetailVc = cardDetailVc else { return }
        cardDetailVc.cardIndex = indexPath.row
        cardDetailVc.currentPage = "Items"
        cardDetailVc.currentDataFetched = arrayItems
        cardDetailVc.amounts = arrayItems[indexPath.row].price ?? "no price"
        cardDetailVc.postId = arrayItems[indexPath.row].posts_id ?? "no post id"
        cardDetailVc.usersId = arrayItems[indexPath.row].users_id ?? "no users id"
        /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
        cardDetailVc.typeToPass = 7
        cardDetailVc.isFixed = arrayItems[indexPath.row].is_fixed?.toInt() ?? 100
        cardDetailVc.createdOn = arrayItems[indexPath.row].createdAt.unwrap()
        cardDetailVc.duration = arrayItems[indexPath.row].duration.unwrap()
        cardDetailVc.minBid = arrayItems[indexPath.row].minBid.unwrap()
        cardDetailVc.maxBid = arrayItems[indexPath.row].maxBid.unwrap()
        
        ez.runThisInMainThread {
            self.presentVC(cardDetailVc)
        }
        
    }
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchViewPostItem(searchText: (self?.temp)!)
        }
        
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchViewPostItem(searchText: (self?.temp)!)
        }
        
    }
    
}
//MARK:- API CALLS
extension HomePageItemViewController {
    func addingPostToFavourite(posts_id:String?, row: Int)
    {
        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.addToFavourite(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1

            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                print("added to favorites")
                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message:(message?.message ?? ""), preferredStyle: .alert)
                    print(message?.message ?? "")
                    alert.view.tintColor = appColor
                    alert.view.backgroundColor = UIColor.white
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
                        if self.arrayItems[row].is_favourite == "0" {
                            self.arrayItems[row].is_favourite = "1"
                        }
                        else {
                            self.arrayItems[row].is_favourite = "0"
                        }
                        self.tblView.reloadData()
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
        
    }
    func fetchViewPostItem(searchText: String){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewPosts(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page: page.toString, type: "7", search_key: searchText)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1

            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                print(data ?? "")
                guard let map = data as? [OtherPostViewModel] else { return }
                if self.temp == "" {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayItems.append(contentsOf: map)
                    case "Header":
                        self.arrayItems = []
                        self.arrayItems = map
                    default :
                       // self.arrayItems = []
                        self.arrayItems = map

                    }
                }
                else {
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayItems.append(contentsOf: map)
                    case "Header":
                        self.arrayItems = []
                        self.arrayItems = map
                    default :
                       // self.arrayItems = []
                        self.arrayItems = map
                    }
                    
                }
                
                if self.arrayItems.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                
                self.tempRefresh = ""
                self.tblView.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
                
            }
        }
    }
}

