//
//  HomePageServiceViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 06/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SDWebImage
import EZSwiftExtensions
import NVActivityIndicatorView

class HomePageServiceViewController: UIViewController {

    //MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    
    @IBOutlet weak var lblNoResultFound: UILabel!
    
    //MARK::- VC VARIABLS
    var posts_id = ""
    var page = 1
    var arrayService = [ServicePostViewModel]()
    var searchKey = ""
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    
    //takeOver--------------------------------------
    var temp = ""//edited
    var filteredMessages = [ServicePostViewModel]()//edited
    var viewCalled = ""
    

    let addAdVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    //MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomePageServiceViewController.initialise),
                                               name: NSNotification.Name(rawValue: "ServicesData"),
                                               object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        temp = ""
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ServicesData"),
                                                  object: nil)
    }
    
    //MARK::- FUNCTIONS
    func initialise(not: Notification){
        pullToRefresh()
        pullToRefreshUpward()
        page = 1
        temp = ""
        
        lblNoResultFound.isHidden = true
        tblView.isHidden = false
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        tblView?.rowHeight = UITableViewAutomaticDimension;
        tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        
        if let myDict = not.userInfo as? [String: Any] {
            if let typeIndex = myDict["selectedIndex"] as? Int {
                if typeIndex == 2 {
                    if let textValue = myDict["searchingText"] as? String {
                        temp = textValue
                        if textValue.length > 0 {
                            fetchViewPostService(searchText: textValue)
                        }else{
                            fetchViewPostService(searchText: "")
                        }
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchViewPostService(searchText: temp)
    }

    
    //MARK::- BTN ACTIONS
    //MARK::- offer service func
    @IBAction func btnOfferAServiceAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addAdVc = storyboard.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 3
        addAdVc.adType = "Service"
        
        /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
        self.presentVC(addAdVc)
        //self.navigationController?.pushViewController(addAdVc, animated: true)
    }
}

//MARK::- TABLE VIEW DELEGATES
extension HomePageServiceViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayService.count
    }
    func tableView(_ tableView: UITableView, commiteditingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        posts_id = (UserDataSingleton.sharedInstance.serviceDataOfferAService?[indexPath.row].posts_id).unwrap()
        let favouriteAction = UITableViewRowAction(style: .normal, title: "                ") { (action:UITableViewRowAction! , indexPath:IndexPath! ) -> Void in
            print(self.posts_id)
            self.addingPostToFavourite(posts_id: self.arrayService[indexPath.row].posts_id, row: indexPath.row)
        }
        
        favouriteAction.backgroundColor = UIColor.clear
        return [favouriteAction]
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.offerAServiceCell.rawValue , for: indexPath) as? HomeScreenServiceTableViewCell  else {return UITableViewCell() }
        let model = arrayService[indexPath.row]

        cell.lblPrice.text = ("$ \(model.price?.toDouble() ?? 0.0)")
        cell.selectionStyle = .none
        cell.lblPrice.text = ("$ \(model.price.unwrap())")
        cell.lblServiceDuration.text = ("\(model.duration.unwrap()) hrs")
        cell.lblServiceTitle.text = model.service_name.unwrap()
        cell.lblUserName.text = model.firstname.unwrap() + " " + model.lastname.unwrap()
        cell.imgProfileImage?.sd_setImage(with: URL(string: model.image100.unwrap()))
        cell.lblRating.text = String(format:"%@  ",model.avg_rating.unwrap())
        
        if model.is_favourite == "0" {
            cell.lblAddTo.text = "Add to"
            cell.lblFavorites.text = "favourites"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart_white")
        }else{
            cell.lblAddTo.text = "Remove"
            cell.lblFavorites.text = "favourite"
            cell.imgViewHeartIcon.image = UIImage(named: "ic_heart")
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("The selected row has index: \(indexPath.row)")
        
        
        UserDefaults.standard.set(true, forKey: "headerViewConstraintToBeAdjusted")

        guard let addAdVc = addAdVc else { return }
        addAdVc.cardIndex = indexPath.row
        addAdVc.currentPage = "Services"
        addAdVc.currentDataFetched = arrayService
        addAdVc.amounts = arrayService[indexPath.row].price ?? "no price"
        addAdVc.postId = arrayService[indexPath.row].posts_id ?? "no post id"
        addAdVc.usersId = arrayService[indexPath.row].users_id ?? "no user id"
        addAdVc.typeToPass = 3
        ez.runThisInMainThread {
            self.presentVC(addAdVc)
        }
    }
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchViewPostService(searchText: (self?.temp)!)
        }
    }
    
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchViewPostService(searchText: (self?.temp)!)
        }
    }
}

//MARK:- API CALLS
extension HomePageServiceViewController {
    func addingPostToFavourite(posts_id:String?, row: Int)
    {
        let activityData = ActivityData(size: CGSize(width:50 , height: 50), message: "", messageFont:nil , type: .ballClipRotate, color: UIColor(r: 13, g: 213, b: 178), padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: UIColor.clear)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.addToFavourite(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            switch response{
           
            case .success(let data) :
                
                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message:(message?.message ?? ""), preferredStyle: .alert)
                    print(message?.message ?? "")
                    alert.view.tintColor = appColor
                    alert.view.backgroundColor = UIColor.white
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
                        if self.arrayService[row].is_favourite == "0" {
                            self.arrayService[row].is_favourite = "1"
                        }
                        else {
                            self.arrayService[row].is_favourite = "0"
                        }
                        self.tblView.reloadData()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
       
    }
    func fetchViewPostService(searchText: String){

        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewPosts(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page: page.toString, type: "3", search_key: searchText)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                print(data ?? "")
                guard let map = data as? [ServicePostViewModel] else { return }
                if self.temp == "" {
                    self.lblNoResultFound.isHidden = true
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayService.append(contentsOf: map)
                    case "Header":
                        self.arrayService = []
                        self.arrayService = map
                    default :
                        self.arrayService = map
                    }

                }else {
                    switch self.tempRefresh {
                    case "Footer":
                        self.arrayService.append(contentsOf: map)
                    case "Header":
                        self.arrayService = []
                        self.arrayService = map
                    default :
                        self.arrayService = map
                    }
                }
                
                if self.arrayService.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }
                else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                
                self.tempRefresh = ""
                self.tblView.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
                
            }
        }
    }
}
