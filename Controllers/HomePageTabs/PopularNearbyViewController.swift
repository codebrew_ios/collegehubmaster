//
//  PopularNearbyViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EZSwiftExtensions

class PopularNearbyViewController: UIViewController {
    
    //MARK::- VC VARIABLES
    var arrayCarpool : [RidePostViewModel]?
    var arrayServices : [ServicePostViewModel]?
    var arrayItems : [OtherPostViewModel]?
    var arrayBooks : [BookPostViewModel]?
    var arrayTickets : [TicketPostViewModel]?
    var heightForRow:Int?
    var myTextToSearch = ""
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    
    weak var delegate: DelegateMoveSegmentTo?
    //    weak var delegateSearchText: DelegateSearchText?
    
    let addAdVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    //MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    
    //MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        
        super.viewDidLoad()
        pullToRefreshUpward()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        UserDefaults.standard.set(true, forKey: "SearchKeyNull")

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(PopularNearbyViewController.initialise),
                                               name: NSNotification.Name(rawValue: "PopularData"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(PopularNearbyViewController.editingEndedSearch),
                                               name: NSNotification.Name(rawValue: "EditingEnded"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(PopularNearbyViewController.showCardDetail),
                                               name: NSNotification.Name(rawValue: "PopularCollectionCell"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "PopularData"),
                                                  object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchViewPopular()
    }
    
    //MARK::- FUNCTIONS
    func showCardDetail(notification: Notification) {
        var indexOfType = Int(), indexLocation = Int()
        if let myDict = notification.object as? [String: Any] {
            if let index = myDict["index"] as? Int {
                indexOfType = index
            }
        }
        print("the index of tab is \(indexOfType)")
        print("")
        /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
    }
    
    //editing ended showing the action sheet controller
    func editingEndedSearch(not: Notification) {
        var myTextToSearch = ""
        if let text = not.object as? String {
            myTextToSearch = text
            UserDefaults.standard.set(false, forKey: "SearchKeyNull")
        }else{
            UserDefaults.standard.set(true, forKey: "SearchKeyNull")
        }
        
        
        UtilityFunctions.popularAlertForSearch(controller: self, showtab: {(indexToSet) -> Void in
            switch indexToSet {
            case 0:
                print("Rides is selected")
                self.delegate?.moveSegement(index: indexToSet)
                if myTextToSearch.length != 0 {
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "SearchTextInPopularNearby"),
                        object: myTextToSearch,
                        userInfo:nil)
                }
                
            case 1:
                print("Service")
                self.delegate?.moveSegement(index: indexToSet)
                if myTextToSearch.length != 0 {
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "SearchTextInPopularNearby"),
                        object: myTextToSearch,
                        userInfo:nil)
                }
                
            case 2:
                print("tickets")
                self.delegate?.moveSegement(index: indexToSet)
                if myTextToSearch.length != 0 {
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "SearchTextInPopularNearby"),
                        object: myTextToSearch,
                        userInfo:nil)
                }
                
            case 3:
                print("Books")
                self.delegate?.moveSegement(index: indexToSet)
                if myTextToSearch.length != 0 {
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "SearchTextInPopularNearby"),
                        object: myTextToSearch,
                        userInfo:nil)
                }
                
            case 4:
                print("Items")
                self.delegate?.moveSegement(index: indexToSet)
                if myTextToSearch.length != 0 {
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: "SearchTextInPopularNearby"),
                        object: myTextToSearch,
                        userInfo:nil)
                }
                
            default: break
            }
        })
    }
    
    func initialise(){
        tblView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0)
        tblView?.rowHeight = UITableViewAutomaticDimension;
        tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        fetchViewPopular()
    }
    
    
    //MARK::- BTN ACTIONS
    // view all button in header section action
    @objc func action(_ sender: UIButton) {
        print("Button view all pressed in popular nearby.........\(sender.tag) and title is \(sender.title)")
        delegate?.moveSegement(index: sender.tag)
    }
    func heightToSet(section: Int) -> Int{
        switch section{
        case 0 :
            if arrayCarpool?.count == 0 {
                return 10
            }
            else {
                return 150
            }
        //            return "Carpool"
        case 1 :
            if arrayServices?.count == 0 {
                return 10
            }
            else {
                return 150
            }
        //            return "Service"
        case 2 :
            if arrayTickets?.count == 0 {
                return 10
            }
            else {
                return 150
            }
        //            return "Tickets"
        case 3 :
            if arrayBooks?.count == 0 {
                return 10
            }
            else {
                return 150
            }
        //            return "Book"
        case 4 :
            if arrayItems?.count == 0 {
                return 10
            }
            else {
                return 150
            }
        //return "Items"
        default :
            return 150
        }
    }
    
}

//MARK::- TABLE VIEW DELEGATES
extension PopularNearbyViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0 :
            return "Carpool"
        case 1 :
            return "Services"
        case 2 :
            return "Tickets"
        case 3 :
            return "Books"
        case 4 :
            return "Items"
        default :
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(heightToSet(section: indexPath.section))
        //return 150
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        header.textLabel?.font = UIFont(name: "Gotham-Bold", size: 16.0)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
        let headerButton:UIButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width-150, y: 0, width: 200, height: 30))
        headerButton.setTitle("View All", for: .normal)
        // setting the title for adding the action reponse on clicked section button
        switch section{
        case 0 :
            headerButton.tag = section
        case 1 :
            headerButton.tag = section
        case 2 :
            headerButton.tag = section
        case 3 :
            headerButton.tag = section
        case 4 :
            headerButton.tag = section
        default :
            headerButton.tag = section
        }
        //
        // creating action for button view all above---------------------
        headerButton.addTarget(self, action: #selector(PopularNearbyViewController.action(_:)), for: UIControlEvents.touchUpInside)
        //---------------------
        headerButton.titleLabel?.textColor = UIColor.purple //UIColor(red: 13/255.0, green: 213/255.0, blue: 178/255.0, alpha: 1.0)
        headerButton.setTitleColor(appColor, for: UIControlState.normal)
        headerButton.titleLabel?.font = UIFont(name: "Gotham-Bold", size: 12.0)
        header.contentView.backgroundColor = UIColor.white
        header.addSubview(headerButton)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0 :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.carpoolPopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyCarpoolTableViewCell  else {return UITableViewCell() }
            cell.carpool = arrayCarpool ?? []
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        case 1 :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.servicePopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyServiceTableViewCell  else {return UITableViewCell() }
            cell.services = arrayServices ?? []
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        case 2 :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.ticketsPopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyTicketsTableViewCell  else {return UITableViewCell() }
            cell.tickets = arrayTickets ?? []
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        case 3 :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.booksPopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyBooksTableViewCell  else {return UITableViewCell() }
            cell.books = arrayBooks ?? []
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        case 4 :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.itemsPopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyItemTableViewCell  else {return UITableViewCell() }
            cell.items = arrayItems ?? []
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        default :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.carpoolPopularNearbyCell.rawValue , for: indexPath) as? PopularNearbyCarpoolTableViewCell  else {return UITableViewCell() }
            return cell
        }
        
    }
}
//MARK::- CollectionViewSelection
extension PopularNearbyViewController: DelgatePopularVC {
    func passType(sectionType: Int, selectedIndexRow: Int) {
        print("\(sectionType) and \(selectedIndexRow)")
        print("adios")
        guard let addAdVc = addAdVc else { return }
        switch sectionType {
        case 0:
            
            //carpool
            guard let arrayCarpool = arrayCarpool else { return }
            addAdVc.cardIndex = selectedIndexRow
            addAdVc.currentPage = "Rides"
            addAdVc.currentDataFetched = arrayCarpool
            addAdVc.amounts = arrayCarpool[selectedIndexRow].price ?? "no price"
            addAdVc.postId = arrayCarpool[selectedIndexRow].posts_id ?? "no post id"
            addAdVc.usersId = arrayCarpool[selectedIndexRow].users_id ?? "no user id"
            //addAdVc.btnCross.setTitle("Book Now", for: .normal)
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.typeToPass = 1
            ez.runThisInMainThread {
                self.presentVC(addAdVc)
            }
        case 1:
            //services
            guard let arrayServices = arrayServices else { return }
            addAdVc.cardIndex = selectedIndexRow
            addAdVc.currentPage = "Services"
            addAdVc.currentDataFetched = arrayServices
            addAdVc.amounts = arrayServices[selectedIndexRow].price ?? "no price"
            addAdVc.postId = arrayServices[selectedIndexRow].posts_id ?? "no post id"
            addAdVc.usersId = arrayServices[selectedIndexRow].users_id ?? "no user id"
            addAdVc.typeToPass = 3
            ez.runThisInMainThread {
                self.presentVC(addAdVc)
            }
            
        case 2:
            //tickets
            guard let arrayTickets = arrayTickets else { return }
            addAdVc.cardIndex = selectedIndexRow
            addAdVc.currentPage = "Tickets"
            addAdVc.currentDataFetched = arrayTickets
            addAdVc.amounts = arrayTickets[selectedIndexRow].price ?? "no price"
            addAdVc.postId = arrayTickets[selectedIndexRow].posts_id ?? "no post id"
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.maxBid = arrayTickets[selectedIndexRow].maxBid ?? "no max bid"
            addAdVc.minBid = arrayTickets[selectedIndexRow].minBid ?? "no min bid"
            addAdVc.usersId = arrayTickets[selectedIndexRow].users_id ?? "no user id"
            addAdVc.duration = arrayTickets[selectedIndexRow].duration ?? "no remaining duration"
            addAdVc.createdOn = arrayTickets[selectedIndexRow].createdAt.unwrap()
            addAdVc.isFixed = arrayTickets[selectedIndexRow].is_fixed?.toInt() ?? 100
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.typeToPass = 6
            
            //self.presentVC(addAdVc)
            ez.runThisInMainThread {
                self.presentVC(addAdVc)
            }
            
        case 3:
            //books
            guard let arrayBooks = arrayBooks else { return }
            addAdVc.cardIndex = selectedIndexRow
            addAdVc.currentPage = "Items"
            addAdVc.currentDataFetched = arrayBooks
            addAdVc.amounts = arrayBooks[selectedIndexRow].price ?? "no price"
            addAdVc.postId = arrayBooks[selectedIndexRow].posts_id ?? "no post id"
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.maxBid = arrayBooks[selectedIndexRow].maxBid ?? "no max bid"
            addAdVc.minBid = arrayBooks[selectedIndexRow].minBid ?? "no min bid"
            addAdVc.usersId = arrayBooks[selectedIndexRow].users_id ?? "no user id"
            addAdVc.duration = arrayBooks[selectedIndexRow].duration ?? "no remainingduration"
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.typeToPass = 5
            addAdVc.createdOn = arrayBooks[selectedIndexRow].createdAt.unwrap()
            addAdVc.isFixed = (arrayBooks[selectedIndexRow].is_fixed)?.toInt() ?? 100
            ez.runThisInMainThread {
                self.presentVC(addAdVc)
            }
            
        case 4:
            //items
            guard let arrayItems = arrayItems else { return }
            addAdVc.cardIndex = selectedIndexRow
            addAdVc.currentPage = "Items"
            addAdVc.currentDataFetched = arrayItems
            addAdVc.amounts = arrayItems[selectedIndexRow].price ?? "no price"
            addAdVc.postId = arrayItems[selectedIndexRow].posts_id ?? "no post id"
            /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
            addAdVc.maxBid = arrayItems[selectedIndexRow].maxBid ?? "no max bid"
            addAdVc.minBid = arrayItems[selectedIndexRow].minBid ?? "no min bid"
            addAdVc.usersId = arrayItems[selectedIndexRow].users_id ?? "no user id"
            addAdVc.duration = arrayItems[selectedIndexRow].duration ?? "no remainingduration"
            addAdVc.isFixed = (arrayItems[selectedIndexRow].is_fixed)?.toInt() ?? 100
             addAdVc.createdOn = arrayItems[selectedIndexRow].createdAt.unwrap()
            addAdVc.typeToPass = 7
            //print(arrayItems[0].image?[0] ?? "no string")
            //self.presentVC(addAdVc)
            ez.runThisInMainThread {
                self.presentVC(addAdVc)
            }
            
        default: break
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.fetchViewPopular()
            self?.tempRefresh = "Header"
        }
        
    }
}
extension PopularNearbyViewController {
    func fetchViewPopular(){
        
//        if(tempRefresh != "Header"){
//            UtilityFunctions.startColoredLoader()
//        }
        let currentLat = UserDataSingleton.sharedInstance.latitude
        let currentLong = UserDataSingleton.sharedInstance.longitude
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewPopular(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone,current_lat:(currentLat ),current_lng:currentLong)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                print(data ?? "")
                let modal = data as? PopularNearbyModal
                self.arrayCarpool =  (modal?.carpool) ?? [] as [RidePostViewModel]
                self.arrayServices = (modal?.services) ?? [] as [ServicePostViewModel]
                self.arrayTickets = (modal?.tickets) ?? [] as [TicketPostViewModel]
                self.arrayBooks = (modal?.books) ?? [] as [BookPostViewModel]
                self.arrayItems = (modal?.items) ?? [] as [OtherPostViewModel]
                self.tblView.reloadData()
                self.tblView.endHeaderRefreshing()
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endHeaderRefreshing()
            default :
                break
                
            }
        }
    }
}



