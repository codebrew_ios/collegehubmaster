//
//  SlideShowViewController.swift
//  CollegeHub
//
//  Created by Sierra 2 on 12/06/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit


class SlideShowViewController: UIViewController {
    
    var arrayImages = [String]()
    var cardDetailVC : CardDetailViewController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       collectionView.delegate = self
        collectionView.dataSource = self
    }
        
    @IBAction func btnBack(_ sender: Any) {
         self.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
}
extension SlideShowViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(arrayImages )
        return (arrayImages.count)
    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth =    UIScreen.main.bounds.width
        let height = collectionView.frame.height
        return CGSize(width : cellWidth,height : height)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlideShowCell", for: indexPath as IndexPath) as! SlideShowCollectionViewCell
        
        print(arrayImages)
        let imageURL = URL(string: arrayImages[indexPath.item])
        cell.imgView?.sd_setImage(with: imageURL)
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)!")
    }
    
}

