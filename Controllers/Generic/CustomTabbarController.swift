//
//  CustomTabbarController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 03/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class CustomTabbarController: UITabBarController {

//MARK:- VARIABLES
    var menuButton:UIButton! = nil
    var loaderLoadFirstTime = 0

//MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfUserRated()
        setupMiddleButton()
        
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.white.withAlphaComponent(0.6)
        } else {
           
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMenuButton), name: NSNotification.Name(rawValue: "showMenuButton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideMenuButton), name: NSNotification.Name(rawValue: "hideMenuButton"), object: nil)
        //for postad in profile tab
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CustomTabbarController.changeTabToPopAd),
                                               name: NSNotification.Name(rawValue: "SlideToPopAdController"),
                                               object: nil)
//        menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 20))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if (UserDataSingleton.sharedInstance.userPush == 1){
            
            UserDataSingleton.sharedInstance.userPush = 0
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.handlePushNavigation(userInfo: UserDataSingleton.sharedInstance.notificationData!)
            
        }
        else{
            
        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
//MARK:- FUNCTIONS
    func changeTabToPopAd() {
        self.selectedIndex = 2
    }
    func showMenuButton(){
        menuButton.isHidden = false
    }
    
    func hideMenuButton(){
        menuButton.isHidden = true
    }
    
    func setupMiddleButton() {
        menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = view.bounds.height - menuButtonFrame.height - 5
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        
        menuButton.backgroundColor = UIColor.white
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        view.addSubview(menuButton)
        
        
        menuButton.setImage(UIImage(named: "ic_add"), for: .normal)
        menuButton.addTarget(self, action: #selector(menuButtonAction(sender:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
// MARK: - BTN ACTIONS
    // plus button action in the tabBar
    @objc private func menuButtonAction(sender: UIButton) {
        selectedIndex = 2
    }
    
  
}
extension CustomTabbarController{
    func checkIfUserRated()
    {
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.checkIfRated(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                let x = data as? PendingRating
                guard  let rateVC =  UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "RatingNFeedbackViewController") as? RatingNFeedbackViewController else {return}
                rateVC.delecateDimOut = self
                rateVC.user_id = (x?.users_id2).unwrap()
                rateVC.post_id = (x?.posts_id).unwrap()
                rateVC.profilePic = (x?.image100).unwrap()
                rateVC.userName = String(format:"%@ %@",(x?.firstname).unwrap(),(x?.lastname).unwrap())
                //self.dim(direction: .in, alpha: 0.5, speed: 0.5)
                self.presentVC(rateVC)
                
            case .success_wrongPassword(let data):
                print(data ?? "")
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
        
    }
}
extension CustomTabbarController : DimoutVC {
    func dimOut()
    {
        // self.dim(direction: .out)
        print("dimout")
    }
}


