//
//  CardDetailViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 11/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class CardDetailViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var imgTrustedVendor: UIImageView!
    @IBOutlet weak var lblTrustedVendor: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCross: UIButton!
    
    //MARK:- Variables
    var arrayNotificationDetail:[ProfileTabSubMenus]?
    var cardIndex: Int!
    var currentPage:String?
    var currentData:AnyObject?
    var collectionViewVisible = false
    var emptyCells = [IndexPath]()
    var currentDataFetched = [AnyObject]()
    var productImages = [AnyObject]()
    var postIdNoti:String?
    var notiCheck : Int?
    var amounts:String?
    var totalAmount : Int?
    var postId:String?
    var usersId: String?
    var typeToPass:Int?
    var callingView: String?
    var isFixed: Int?
    
    //for showing the days left.
    var createdOn: String?
    var duration: String?
    var minBid: String?
    var maxBid: String?
    var loaderLoadFirstTime = 0
    var chooseBuyOrBid = 0
    
    typealias CellToIdentifier = (cell: UITableViewCell, identifier: String)
    
    let arrayRideCells : [CellToIdentifier] = [(CardRideTableViewCell(),"CardRideTableViewCell"),
                                               (CardOwnerTableViewCell(),"CardOwnerTableViewCell"),
                                               (CardSegmentTableViewCell(),"CardSegmentTableViewCell"),
                                               (CardStopsTableViewCell(),"CardStopsTableViewCell"),
                                               (CardPriceTableViewCell(),"CardPriceTableViewCell"),
                                               (CardAboutTableViewCell(),"CardAboutTableViewCell")
    ]
    
    let arrayItemCells : [CellToIdentifier] = [(CardCollectionTableViewCell(),"CardCollectionTableViewCell"),
                                               (CardTitleTableViewCell(),"CardTitleTableViewCell"),
                                               (CardOwnerTableViewCell(),"CardOwnerTableViewCell"),
                                               (CardSegmentTableViewCell(),"CardSegmentTableViewCell"),
                                               (CardPriceTableViewCell(),"CardPriceTableViewCell"),
                                               (CardHighestBidTableViewCell(), "CardHighestBidTableViewCell"),
                                               (CardAboutTableViewCell(),"CardAboutTableViewCell")
    ]
    
    let arrayBooksCells : [CellToIdentifier] = [(CardCollectionTableViewCell(),"CardCollectionTableViewCell"),
                                                (CardTitleTableViewCell(),"CardTitleTableViewCell"),
                                                (CardOwnerTableViewCell(),"CardOwnerTableViewCell"),
                                                (CardSegmentTableViewCell(),"CardSegmentTableViewCell"),
                                                (CardPriceTableViewCell(),"CardPriceTableViewCell"),
                                                (CardHighestBidTableViewCell(), "CardHighestBidTableViewCell"),
                                                (CardAboutTableViewCell(),"CardAboutTableViewCell")
    ]
    
    let arrayServiceCells : [CellToIdentifier] = [(CardTitleTableViewCell(),"CardTitleTableViewCell"),
                                                  (CardOwnerTableViewCell(),"CardOwnerTableViewCell"),
                                                  (CardSegmentTableViewCell(),"CardSegmentTableViewCell"),
                                                  (CardPriceTableViewCell(),"CardPriceTableViewCell"),
                                                  (CardAboutTableViewCell(),"CardAboutTableViewCell")
    ]
    
    let arrayTicketsCells: [CellToIdentifier] = [(CardTitleTableViewCell(), "CardTitleTableViewCell"),
                                                 (CardOwnerTableViewCell(), "CardOwnerTableViewCell"),
                                                 (CardSegmentTableViewCell(), "CardSegmentTableViewCell"),
                                                 (CardStopsTableViewCell(),"CardStopsTableViewCell"),
                                                 (CardPriceTableViewCell(), "CardPriceTableViewCell"),
                                                 (CardHighestBidTableViewCell(), "CardHighestBidTableViewCell")
    ]
    
}


//MARK:-
//MARK:- VC LifeCycle

extension CardDetailViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navBar?.isHidden = false
        self.navBar?.tintColor = UIColor.white
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back")
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        tableView?.rowHeight = UITableViewAutomaticDimension;
        tableView?.estimatedRowHeight = 1000;//(Maximum Height that should be assigned to your cell)
        
        if(notiCheck != 1){
            SettingUI()
        }
        else{
            
            fetchNotificationsDetail()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true);
        self.navBar?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        print(cardIndex ?? "")
        print(currentPage ?? "")
        
        if(notiCheck != 1){
            
            setData()
        }
        
        //setTheBuyButton()
    }
}

//MARK:-
//MARK:- SettingUI
extension CardDetailViewController{
    
    func SettingUI(){
        
        btnBuy.isHidden = false
        setTheBuyButton()
    }
    
}


//MARK:-
//MARK:- Actions
extension CardDetailViewController{
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismiss(animated: true) {
            
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnBuyClick(_ sender: Any) {
        /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
        guard let type = typeToPass else { return }
        switch type {
        case 1, 3:
            fetchProfileData()
            chooseBuyOrBid = 1
        case 5, 6, 7:
            guard let isFixed = isFixed else { return }
            switch isFixed {
            case 1:
                print("Booking will take place")
                fetchProfileData()
                chooseBuyOrBid = 1
                
            case 2:
                fetchProfileData()
                chooseBuyOrBid = 2
                
            default : break
            }
        default: break
        }
    }
    
    
}

//MARK:-CheckCreditCardDetails

extension CardDetailViewController{
    
    func checkCreditCardDetails() {
        //self.fetchProfileData()
        guard let popUpVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as? PopUpViewController else { return }
        let cardCheck = UserDataSingleton.sharedInstance.loggedInUser?.card_added
        if cardCheck == "0" {
            popUpVC.cardDetailVariable = self
            self.dim(direction: .in, alpha: 0.5, speed: 0.5)
            self.presentVC(popUpVC)
            
        }
        else {
            switch chooseBuyOrBid {
            case 1:
                buyPost()
            case 2 :
                callBidVC()
            default:
                break
            }
            buyPost()
            
        }
    }
    func dimOutVC()
    {
        self.dim(direction: .out)
    }
    func dismissVc()
    {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func setTheBuyButton() {
        /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
        
        guard let type = typeToPass else { return }
        switch type {
        case 1, 3:
            btnBuy.setTitle("Book Now", for: .normal)
        case 5, 6, 7:
            if isFixed == 1 {
                btnBuy.setTitle("Book Now", for: .normal)
            }else {
                btnBuy.setTitle("Bid Now", for: .normal)
            }
            
        default:
            btnBuy.isHidden = true
            
        }
    }
}

//MARK: - Setup TableView
extension CardDetailViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let page = currentPage{
            switch page{
            case  pageName.item.rawValue:
                return arrayItemCells.count
            case  pageName.ride.rawValue:
                return arrayRideCells.count
            case  pageName.service.rawValue:
                return arrayServiceCells.count
            case pageName.tickets.rawValue:
                return arrayTicketsCells.count
            case pageName.books.rawValue:
                return arrayBooksCells.count
            default: return 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let page = currentPage {
            switch page{
            case  pageName.item.rawValue:
                let cell = setUpCell(tableView: tableView, indexPath: indexPath, identifier: arrayItemCells[indexPath.row].identifier, cellType: arrayItemCells[indexPath.row].cell)
                return cell
            case  pageName.ride.rawValue:
                let cell = setUpCell(tableView: tableView, indexPath: indexPath, identifier: arrayRideCells[indexPath.row].identifier, cellType: arrayRideCells[indexPath.row].cell)
                return cell
            case  pageName.service.rawValue:
                let cell = setUpCell(tableView: tableView, indexPath: indexPath, identifier: arrayServiceCells[indexPath.row].identifier, cellType: arrayServiceCells[indexPath.row].cell)
                return cell
            case  pageName.tickets.rawValue:
                let cell = setUpCell(tableView: tableView, indexPath: indexPath, identifier: arrayTicketsCells[indexPath.row].identifier, cellType: arrayTicketsCells[indexPath.row].cell)
                return cell
            case pageName.books.rawValue:
                let cell = setUpCell(tableView: tableView, indexPath: indexPath, identifier: arrayBooksCells[indexPath.row].identifier, cellType: arrayBooksCells[indexPath.row].cell)
                return cell
            default: break
            }
        }
        else {
            print("no page value")
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if emptyCells.count == 1{
            if emptyCells[0] == indexPath{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }else if emptyCells.count == 2{
            if emptyCells[1] == indexPath{
                return 0
            }else{
                return UITableViewAutomaticDimension
            }
        }else{
            if collectionViewVisible == true{
                collectionViewVisible = false
                return 250
            }else{
                return UITableViewAutomaticDimension
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        print("row test")
        
    }
}

extension CardDetailViewController{
    
    func setUpCell(tableView: UITableView, indexPath: IndexPath, identifier: String, cellType: UITableViewCell) -> UITableViewCell{
        
        //MARK:- Price Cell
        if ((cellType as? CardPriceTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardPriceTableViewCell  else {return UITableViewCell()}
            cell.currentData = currentData
            
            if ((currentData as? RidePostViewModel) != nil){
                let data = currentData as? RidePostViewModel
                lblTrustedVendor.isHidden = true
                imgTrustedVendor.isHidden = true
                totalAmount = (data?.price).unwrap().toInt()! * (data?.num_of_seats).unwrap().toInt()!
                //if let price = asketPrice{
                cell.lblPriceValue.text = "$ " + (totalAmount?.toString)!
            }else if((currentData as? ServicePostViewModel) != nil){
                let data = currentData as? ServicePostViewModel
                if(data?.created_by == "2"){
                    lblTrustedVendor.isHidden = false
                    imgTrustedVendor.isHidden = false
                }
                else {
                    lblTrustedVendor.isHidden = true
                    imgTrustedVendor.isHidden = true
                }
                if let price = data?.price {
                    cell.lblPriceValue.text = "$ \(price.toDouble() ?? 0.0)"
                    totalAmount = price.toInt()
                }
                else {
                    cell.lblPriceValue.text = "not available"
                }
            }else if((currentData as? BookPostViewModel) != nil){
                let data = currentData as? BookPostViewModel
                if let price = data?.price {
                    cell.lblPriceValue.text = "$ \(price.toDouble() ?? 0.0)"
                    totalAmount = price.toInt()
                }
                else {
                    cell.lblPriceValue.text = "not available"
                }
                lblTrustedVendor.isHidden = true
                imgTrustedVendor.isHidden = true
            }else if((currentData as? TicketPostViewModel) != nil){
                let data = currentData as? TicketPostViewModel
                totalAmount = (data?.price).unwrap().toInt()! * (data?.num_of_tickets).unwrap().toInt()!
                cell.lblPriceValue.text = "$ " + (totalAmount?.toString)!
                lblTrustedVendor.isHidden = true
                imgTrustedVendor.isHidden = true
            }else if((currentData as? OtherPostViewModel) != nil){
                let data = currentData as? OtherPostViewModel
                if let price = data?.price {
                    cell.lblPriceValue.text = "$ \(price.toDouble() ?? 0.0)"
                    totalAmount = price.toInt()
                }
                else {
                    cell.lblPriceValue.text = "not available"
                }
                lblTrustedVendor.isHidden = true
                imgTrustedVendor.isHidden = true
                //cell.lblPriceValue.text = "$ " + data.price!
            }
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                let data = currentData as? ProfileTabSubMenus
                guard let typeToPass = typeToPass else { return cell }
                // if let price = data?.price {
                switch typeToPass {
                case 1:
                    totalAmount = (data?.price).unwrap().toInt()! * (data?.num_of_seats).unwrap().toInt()!
                    cell.lblPriceValue.text = "$ " + (totalAmount?.toString)!
                    lblTrustedVendor.isHidden = true
                    imgTrustedVendor.isHidden = true
                case 5:
                    if(data?.created_by == "2"){
                        lblTrustedVendor.isHidden = false
                        imgTrustedVendor.isHidden = false
                    }
                    else {
                        lblTrustedVendor.isHidden = true
                        imgTrustedVendor.isHidden = true
                    }
                    
                case 6 :
                    totalAmount = (data?.price).unwrap().toInt()! * (data?.num_of_tickets).unwrap().toInt()!
                    cell.lblPriceValue.text = "$ " + (totalAmount?.toString)!
                    lblTrustedVendor.isHidden = true
                    imgTrustedVendor.isHidden = true
                default :
                    cell.lblPriceValue.text = "$ \(data?.price?.toDouble() ?? 0.0)"
                    totalAmount = data?.price?.toInt()
                    lblTrustedVendor.isHidden = true
                    imgTrustedVendor.isHidden = true
                }
                
            }
            
            return cell
            
            //MARK:- Title Cell
            
        }else if ((cellType as? CardTitleTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardTitleTableViewCell  else {return UITableViewCell()}
            
            
            if((currentData as? ServicePostViewModel) != nil){
                let data = currentData as? ServicePostViewModel
                cell.lblTitle.text = data?.service_name
                
                if let des = data?.Description, des != ""{
                    cell.lblTitleDescription.isHidden = false
                    cell.lblTitleDescription.text = data?.Description
                }else{
                    cell.lblTitleDescription.isHidden = true
                }
            }else if((currentData as? BookPostViewModel) != nil){
                let data = currentData as? BookPostViewModel
                cell.lblTitle.text = data?.name
                cell.lblTitleDescription.isHidden = true
                
                
            }else if((currentData as? TicketPostViewModel) != nil){
                let data = currentData as? TicketPostViewModel
                cell.lblTitle.text = data?.name
                cell.lblTitleDescription.isHidden = true
                
                
            }else if((currentData as? OtherPostViewModel) != nil){
                let data = currentData as? OtherPostViewModel
                cell.lblTitle.text = data?.name
                cell.lblTitleDescription.isHidden = true
                
            }else if ((currentData as? ProfileTabSubMenus) != nil) {
                let data = currentData as? ProfileTabSubMenus
                
                cell.lblTitle.text = data?.name
                cell.lblTitleDescription.isHidden = true
                guard let typeToPass = typeToPass else {return cell}
                if (typeToPass == 3){
                    if let des = data?.Description, des != ""{
                        cell.lblTitleDescription.isHidden = false
                        cell.lblTitle.text = data?.service_name
                        cell.lblTitleDescription.text = data?.Description
                    }else{
                        cell.lblTitleDescription.isHidden = true
                    }
                }
                
            }
            
            return cell
            
            //MARK:- Owner Cell
            
        }else if ((cellType as? CardOwnerTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardOwnerTableViewCell  else {return UITableViewCell()}
            
            
            if ((currentData as? RidePostViewModel) != nil){
                let data = currentData as? RidePostViewModel
                cell.lblRating.text = data?.avg_rating
                cell.lblOwner.text = "Ride Offered By:"
                cell.lblOwnerValue.text = (data?.firstname).unwrap() + " " + ((data?.lastname).unwrap())
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
                
            }else if((currentData as? ServicePostViewModel) != nil){
                
                let data = currentData as? ServicePostViewModel
                cell.lblRating.text = data?.avg_rating
                cell.lblOwner.text = "Service Offered By:"
                cell.lblOwnerValue.text = (data?.firstname).unwrap() + " " + ((data?.lastname).unwrap())
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
                
            }else if((currentData as? BookPostViewModel) != nil){
                
                let data = currentData as? BookPostViewModel
                cell.lblRating.text = data?.avg_rating
                cell.lblOwner.text = "Onwer"
                cell.lblOwnerValue.text = (data?.firstname).unwrap() + " " + ((data?.lastname).unwrap())
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
                
            }else if((currentData as? TicketPostViewModel) != nil){
                let data = currentData as? TicketPostViewModel
                cell.lblRating.text = data?.avg_rating
                cell.lblOwner.text = "Tickets offered by"
                cell.lblOwnerValue.text = (data?.firstname).unwrap() + " " + ((data?.lastname).unwrap())
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
            }else if((currentData as? OtherPostViewModel) != nil){
                let data = currentData as? OtherPostViewModel
                cell.lblOwner.text = "Onwer"
                cell.lblRating.text = data?.avg_rating
                cell.lblOwnerValue.text = (data?.firstname).unwrap() + " " + ((data?.lastname).unwrap())
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
            }
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                guard let typeToPass = typeToPass else { return cell }
                let data = currentData as? ProfileTabSubMenus
                cell.lblRating.text = data?.avg_rating
                switch typeToPass {
                case 1:
                    cell.lblOwner.text = "Rides offered by:"
                case 3 :
                    cell.lblOwner.text = "Services offered by"
                case 5,7:
                    cell.lblOwner.text = "Owner"
                case 6:
                    cell.lblOwner.text = "Tickets offered by"
                default :
                    break
                }
                if let firstName = data?.firstname, let lastName = data?.lastname {
                    cell.lblOwnerValue.text = firstName + " " + lastName
                }
                if let img = data?.image100 {
                    cell.imgView.sd_setImage(with: URL.init(string: img),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
                }
            }
            return cell
            
            //MARK:- Ride Cell
        }else if((cellType as? CardRideTableViewCell) != nil) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardRideTableViewCell  else {return UITableViewCell()}
            
            if ((currentData as? RidePostViewModel) != nil){
                let data = currentData as? RidePostViewModel
                cell.lblDeparture.text = data?.departure
                cell.lblDestination.text = data?.destination
            }
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                let data = currentData as? ProfileTabSubMenus
                if let departureCity = data?.departure, let destinationCity = data?.destination {
                    cell.lblDeparture.text = departureCity
                    cell.lblDestination.text = destinationCity
                }
            }
            
            return cell
            
            //MARK:- Segment Cell
        }else if((cellType as? CardSegmentTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardSegmentTableViewCell  else {return UITableViewCell()}
            
            
            if ((currentData as? RidePostViewModel) != nil){
                let data = currentData as? RidePostViewModel
                cell.lblSegment2.isHidden = false
                cell.lblSegment2Value.isHidden = false
                cell.lblSegment1.text = "Leaving On"
                let  dateIs = data?.leaving_on
                let dateString = dateIs?.toDateTime()
                cell.lblSegment1Value.text = dateString
                cell.lblSegment2.text = "Departure Time"
                cell.lblSegment2Value.text = data?.departure_time
                cell.lblSeats.isHidden = false
                cell.lblSeatsValue.isHidden = false
                cell.lblSeatsValue.text = data?.num_of_seats
            }else if((currentData as? ServicePostViewModel) != nil){
                let data = currentData as? ServicePostViewModel
                cell.lblSeats.isHidden = true
                cell.lblSeatsValue.isHidden = true
                cell.lblSegment2.isHidden = false
                cell.lblSegment2Value.isHidden = false
                cell.lblSegment1.text = "Session Length"
                cell.lblSegment1Value.text = (data?.duration).unwrap() + " hrs"
                cell.lblSegment2.text = "Availability"
                cell.lblSegment2Value.text = (data?.offer_1_time).unwrap()
                
            }else if((currentData as? BookPostViewModel) != nil){
                let data = currentData as? BookPostViewModel
                if(data?.is_fixed == "2"){
                    cell.lblSeats.isHidden = false
                    cell.lblSeatsValue.isHidden = false
                    cell.lblSeats.text = "Duration"
                    cell.lblSeatsValue.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                }
                else{
                    cell.lblSeats.isHidden = true
                    cell.lblSeatsValue.isHidden = true
                }
                cell.lblSegment2.isHidden = false
                cell.lblSegment2Value.isHidden = false
                cell.lblSegment1.text = "Edition"
                cell.lblSegment1Value.text = data?.edition
                cell.lblSegment2.text = "Condition"
                cell.lblSegment2Value.text = data?.condition_status
            }
            else if((currentData as? OtherPostViewModel) != nil){
                let data = currentData as? OtherPostViewModel
                if(data?.is_fixed == "2"){
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.isHidden = false
                    cell.lblSegment2.text = "Duration"
                    cell.lblSegment2Value.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                }
                else{
                    cell.lblSegment2.isHidden = true
                    cell.lblSegment2Value.isHidden = true
                }
                cell.lblSegment1.text = "Condition"
                cell.lblSegment1Value.text = data?.condition_status
                cell.lblSeats.isHidden = true
                cell.lblSeatsValue.isHidden = true
                
                    }
                
            else if((currentData as? TicketPostViewModel) != nil){
                let data = currentData as? TicketPostViewModel
                if(data?.is_fixed == "2"){
                    cell.lblSeats.isHidden = false
                    cell.lblSeatsValue.isHidden = false
                    cell.lblSeats.text = "Duration"
                    cell.lblSeatsValue.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                }
                else{
                    cell.lblSeats.isHidden = true
                    cell.lblSeatsValue.isHidden = true
                }
                cell.lblSegment2.isHidden = false
                cell.lblSegment2Value.isHidden = false
                cell.lblSegment1.isHidden = false
                cell.lblSegment1Value.isHidden = false
                cell.lblSegment1.text = "Event Date"
                let  dateIs = data?.date
                let dateString = dateIs?.toDateTime()
                cell.lblSegment1Value.text = dateString
                cell.lblSegment2.text = "Number Of Tickets"
                cell.lblSegment2Value.text = data?.num_of_tickets
            }
                // profile tab sub menus-----------------------------------------------------
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
                if typeToPass == 1 {
                    let data = currentData as? ProfileTabSubMenus
                    cell.lblSegment1.text = "Leaving On"
                    let  dateIs = data?.leaving_on
                    let dateString = dateIs?.toDateTime()
                    cell.lblSegment1Value.text = dateString
                    cell.lblSegment2.text = "Departure Time"
                    cell.lblSegment2Value.text = data?.departure_time
                    cell.lblSeats.isHidden = false
                    cell.lblSeatsValue.isHidden = false
                    cell.lblSeatsValue.text = data?.num_of_seats
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.isHidden = false
                }
                else if typeToPass == 3 {
                    let data = currentData as? ProfileTabSubMenus
                    cell.lblSeats.isHidden = true
                    cell.lblSeatsValue.isHidden = true
                    cell.lblSegment1.text = "Session Length"
                    cell.lblSegment1Value.text = (data?.duration).unwrap() + " hrs"
                    cell.lblSegment2.text = "Availability"
                    cell.lblSegment2Value.isHidden = false
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.text = (data?.offer_1_time).unwrap()
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.isHidden = false
                }
                else if typeToPass == 5 {
                    let data = currentData as? ProfileTabSubMenus
                    if(data?.is_fixed == "2"){
                        cell.lblSeats.isHidden = false
                        cell.lblSeatsValue.isHidden = false
                        cell.lblSeats.text = "Duration"
                        cell.lblSeatsValue.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                    }
                    else{
                        cell.lblSeats.isHidden = true
                        cell.lblSeatsValue.isHidden = true
                    }
                    cell.lblSegment1.text = "Edition"
                    cell.lblSegment1Value.text = data?.edition
                    cell.lblSegment2.text = "Condition"
                    cell.lblSegment2Value.text = data?.condition_status
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.isHidden = false
                    
                }
                else if typeToPass == 6 {
                    let data = currentData as? ProfileTabSubMenus
                    if(data?.is_fixed == "2"){
                        cell.lblSeats.isHidden = false
                        cell.lblSeatsValue.isHidden = false
                        cell.lblSeats.text = "Duration"
                        cell.lblSeatsValue.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                    }
                    else{
                        cell.lblSeats.isHidden = true
                        cell.lblSeatsValue.isHidden = true
                    }
                    cell.lblSegment1.isHidden = false
                    cell.lblSegment1Value.isHidden = false
                    cell.lblSegment1.text = "Event Date"
                    let  dateIs = data?.date
                    let dateString = dateIs?.toDateTime()
                    cell.lblSegment1Value.text = dateString
                    cell.lblSegment2.text = "Number Of Tickets"
                    cell.lblSegment2Value.text = data?.num_of_tickets
                    cell.lblSegment2.isHidden = false
                    cell.lblSegment2Value.isHidden = false
                    
                }
                else if typeToPass == 7 {
                    let data = currentData as? ProfileTabSubMenus
                    if(data?.is_fixed == "2"){
                        cell.lblSegment2.isHidden = false
                        cell.lblSegment2Value.isHidden = false
                        cell.lblSegment2.text = "Duration"
                        cell.lblSegment2Value.text = UtilityFunctions.calculateRemainingDays(data?.createdAt, data?.duration)
                    }
                    else{
                        cell.lblSegment2.isHidden = true
                        cell.lblSegment2Value.isHidden = true
                    }
                    cell.lblSegment1.text = "Condition"
                    cell.lblSegment1Value.text = data?.condition_status
                    cell.lblSeats.isHidden = true
                    cell.lblSeatsValue.isHidden = true
                    //                    emptyCells.append(indexPath)
                    
                }
                
            }
            
            return cell
            
            //MARK:- Stops Cell
            
        }else if((cellType as? CardStopsTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardStopsTableViewCell  else {return UITableViewCell()}
            
            cell.lblStopsValue1.isHidden = true
            cell.lblStopsValue2.isHidden = true
            cell.lblStopsValue3.isHidden = true
            cell.lblStops.isHidden = true
            if ((currentData as? RidePostViewModel) != nil){
                let data = currentData as? RidePostViewModel
                let stopsCount = data?.stops?.count ?? 0
                if stopsCount>0 && data?.stops?[0] != "No" {
                    cell.viewShowWhenNoStops.isHidden = true
                    //cell.lblStops.isHidden = true
                    let stop1 = data?.stops?[0] ?? "no stop set"
                    cell.lblStops.isHidden = false
                    cell.lblStopsValue1.isHidden = false
                    cell.lblStops.text = "Stops during the trip"
                    cell.lblStopsValue1.text = "1. " + stop1
                    
                    if stopsCount > 1 &&  data?.stops?[1] != "No" {
                        let stop2 = data?.stops?[1] ?? "no stop set"
                        cell.lblStopsValue2.isHidden = false
                        cell.lblStopsValue2.text = "2. " + stop2
                        
                        if stopsCount > 2 && data?.stops?[2] != "No" {
                            let stop3 = data?.stops?[2] ?? "no stop set"
                            cell.lblStopsValue3.isHidden = false
                            cell.lblStopsValue3.text = "3. " + stop3
                        }
                    }
                }
            }
            else if ((currentData as? TicketPostViewModel) != nil){
                let data = currentData as? TicketPostViewModel
                cell.viewShowWhenNoStops.isHidden = true
                cell.lblStops.isHidden = false
                cell.lblStopsValue1.isHidden = false
                cell.lblStops.text = "Row / Section"
                cell.lblStopsValue1.text = data?.row_section
            }
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                let data = currentData as? ProfileTabSubMenus
                guard let typeToPass = typeToPass else { return cell }
                switch typeToPass {
                case 1:
                    let stopsCount = data?.stops?.count ?? 0
                    if stopsCount>0 && data?.stops?[0] != "No" {
                        cell.viewShowWhenNoStops.isHidden = true
                        let stop1 = data?.stops?[0] ?? "no stop set"
                        cell.lblStops.isHidden = false
                        cell.lblStopsValue1.isHidden = false
                        cell.lblStops.text = "Stops during the trip"
                        cell.lblStopsValue1.text = "1. " + stop1
                        
                        if stopsCount > 1 &&  data?.stops?[1] != "No" {
                            let stop2 = data?.stops?[1] ?? "no stop set"
                            cell.lblStopsValue2.isHidden = false
                            cell.lblStopsValue2.text = "2. " + stop2
                            
                            if stopsCount > 2 && data?.stops?[2] != "No" {
                                let stop3 = data?.stops?[2] ?? "no stop set"
                                cell.lblStopsValue3.isHidden = false
                                cell.lblStopsValue3.text = "3. " + stop3
                            }
                        }
                    }
                    else {
                        cell.viewShowWhenNoStops.isHidden = false
                    }
                    
                case 3,5:
                    cell.viewShowWhenNoStops.isHidden = true
                case 6:
                    cell.viewShowWhenNoStops.isHidden = true
                    cell.lblStops.isHidden = false
                    cell.lblStopsValue1.isHidden = false
                    cell.lblStops.text = "Row / Section"
                    cell.lblStopsValue1.text = data?.row_section
                default: break
                    
                }
                
            }
            else {
                print(emptyCells)
            }
            
            return cell
            
            
        }
            //MARK:- Highest Bid cell
        else if ((cellType as? CardHighestBidTableViewCell) != nil) {
            if isFixed == 2 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CardHighestBidTableViewCell else { return UITableViewCell() }
                
                if ((currentData as? RidePostViewModel) != nil){
                    let data = currentData as! RidePostViewModel
                    if data.maxBid == "0.00"{data.maxBid = data.price}//edited for bidding check
                    if let price = data.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "not available"
                    }
                    
                }else if((currentData as? ServicePostViewModel) != nil){
                    let data = currentData as! ServicePostViewModel
                    if data.maxBid == "0.00"{data.maxBid = data.price}
                    if let price = data.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "not available"
                    }
                    
                }else if((currentData as? BookPostViewModel) != nil){
                    let data = currentData as! BookPostViewModel
                    if data.maxBid == "0.00"{data.maxBid = data.price}
                    if let price = data.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "not available"
                    }
                    
                }else if((currentData as? TicketPostViewModel) != nil){
                    let data = currentData as! TicketPostViewModel
                    if data.maxBid == "0.00"{data.maxBid = data.price}
                    if let price = data.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "not available"
                    }
                    
                }else if((currentData as? OtherPostViewModel) != nil){
                    let data = currentData as! OtherPostViewModel
                    if data.maxBid == "0.00"{data.maxBid = data.price}
                    if let price = data.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "not available"
                    }
                    
                }
                else if ((currentData as? ProfileTabSubMenus) != nil) {
                    let data = currentData as? ProfileTabSubMenus
                    if data?.maxBid == "0.00"{data?.maxBid = data?.price}
                    if let price = data?.maxBid {
                        cell.lblHighestBidValue.text = "$ \(price.toDouble() ?? 0.0)"
                    }
                    else {
                        cell.lblHighestBidValue.text = "nil"
                    }
                }
                return cell
            }
        }
            //MARK:- Collection Cell
        else if((cellType as? CardCollectionTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardCollectionTableViewCell  else {return UITableViewCell()}
            cell.delegateSlideVC = self
            if((currentData as? BookPostViewModel) != nil){
                let data = currentData as? BookPostViewModel
                if let count = data?.image?.count, count > 0{
                    guard let imagesArray = data?.image else { return cell }
                    cell.arrayImages = (imagesArray as? [String]) ?? []
                    cell.collectionView.reloadData()
                    //cell.arrayImages = data.image as! [String] as! [imageModal]
                    collectionViewVisible = true
                }
            }
                
            else if((currentData as? OtherPostViewModel) != nil){
                let data = currentData as? OtherPostViewModel
                if let count = data?.image?.count, count > 0{
                    guard let imagesArray = data?.image else { return cell }
                    cell.arrayImages = imagesArray as? [String] ?? []
                    cell.collectionView.reloadData()
                    //cell.arrayImages = data.image as! [String] as! [imageModal]
                    collectionViewVisible = true
                }
            }
            else if ((currentData as? ProfileTabSubMenus) != nil) {
                let data = currentData as? ProfileTabSubMenus
                if let count = data?.image?.count, count > 0{
                    guard let imagesArray = data?.image else { return cell }
                    cell.arrayImages = imagesArray as? [String] ?? []
                    cell.collectionView.reloadData()
                    //cell.arrayImages = data.image as! [String] as! [imageModal]
                    collectionViewVisible = true
                }
            }
            cell.cardDetailVC = self
            return cell
            
            
            //MARK:- Location Cell
        }else if((cellType as? CardLocationTableViewCell) != nil) {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? CardLocationTableViewCell  else {return UITableViewCell()}
            
            if((currentData as? BookPostViewModel) != nil){
                _ = currentData as? BookPostViewModel
                cell.lblLocation.text = "Meetup At"
            }else if((currentData as? TicketPostViewModel) != nil){
                _ = currentData as! TicketPostViewModel
            }else if((currentData as? OtherPostViewModel) != nil){
                _ = currentData as! OtherPostViewModel
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func setData() {
        if callingView == "Favourites" {
            print(currentData ?? "no data")//currentData = dataPassedFromFav
            btnBuy.isHidden = false
        }
        else if callingView == "CurrentListing" {
            btnBuy.isHidden = true
        }
        else {
            currentData = currentDataFetched[cardIndex]
            btnBuy.isHidden = false
        }
        setTheBuyButton()
        tableView.reloadData()
        /* 1 - rider, 3 - services, 5 - books, 6 - tickets, 7 - items*/
        guard let typeToPass = typeToPass else { return }
        switch typeToPass {
        case 1:
            currentPage = pageName.ride.rawValue
        case 3:
            currentPage = pageName.service.rawValue
        case 5:
            currentPage = pageName.books.rawValue
        case 6:
            currentPage = pageName.tickets.rawValue
        case 7:
            currentPage = pageName.item.rawValue
        default: break
            
        }
        
    print("The data set :- \n" , currentData ?? "no data set.")
    }
    
    func setupData(){
        if let page = currentPage{
            switch page{
            case  pageName.item.rawValue:
                
                if ((UserDefaults.standard.value(forKey: userPrefrences.currentTabForItem.rawValue) as! String) == itemSubCategory.books.rawValue){
                    
                    currentData = UserDataSingleton.sharedInstance.booksData?[cardIndex]
                    
                }else if((UserDefaults.standard.value(forKey: userPrefrences.currentTabForItem.rawValue) as! String) == itemSubCategory.tickets.rawValue){
                    
                    currentData = UserDataSingleton.sharedInstance.ticketsData?[cardIndex]
                    
                }else{
                    
                    currentData = UserDataSingleton.sharedInstance.othersData?[cardIndex]
                }
                
            case pageName.ride.rawValue:
                
                let result = UserDefaults.standard.value(forKey: userPrefrences.currentTabForRide.rawValue) as! String
                if (result == rideSubCategory.lookForRide.rawValue){
                    currentData = UserDataSingleton.sharedInstance.rideDataLookingForRide?[cardIndex]
                    
                }else if(result == rideSubCategory.lookForPassenger.rawValue){
                    currentData = UserDataSingleton.sharedInstance.rideDataLookingForPassenger?[cardIndex]
                    
                }
                
            case pageName.service.rawValue:
                
                if ((UserDefaults.standard.value(forKey: userPrefrences.currentTabForService.rawValue) as! String) == serviceSubCategory.lookForService.rawValue){
                    currentData = UserDataSingleton.sharedInstance.serviceDataLookforService?[cardIndex]
                    
                }else if((UserDefaults.standard.value(forKey: userPrefrences.currentTabForService.rawValue) as! String) == serviceSubCategory.offerAService.rawValue){
                    currentData = UserDataSingleton.sharedInstance.serviceDataOfferAService?[cardIndex]
                }
            default: return
            }
        }
    }
}




//MARK::-
extension CardDetailViewController {
    func addingPostToFavourite(posts_id:String?)
    {
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.addToFavourite(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message:(message?.message ?? ""), preferredStyle: .alert)
                    print(message?.message ?? "")
                    alert.view.tintColor = appColor
                    alert.view.backgroundColor = UIColor.white
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
                        _ = self.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
        
    }
    
}






//MARK::- Payment CardDetails
extension CardDetailViewController {
    
    func buyPost() {
        print(typeToPass?.toString ?? "")
     
        UtilityFunctions.show(alert: "", message: "Are you sure you want to buy this post?", buttonOk: {
            UtilityFunctions.startColoredLoader()
            
            APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.buyPosts(access_token:  UserDataSingleton.sharedInstance.loggedInUser?.access_token, amount: self.amounts, posts_id: self.postId, users_id: self.usersId, type: self.typeToPass?.toString)),loaderNeeded:self.loaderLoadFirstTime) { (response) in
                
                self.loaderLoadFirstTime = 1
                
                UtilityFunctions.stopLoader()
                
                print(response)
                switch response{
                case .success(let data) :
                    print(data ?? "no data")
                    print("success")
                    
                    guard let price = self.totalAmount else { return }
                    UtilityFunctions.showAlertAndDismiss(message: "We have successfully recieved the payment of $ \(price). Now you can chat with the concerned person for the follow up.", controller: self, dismissController: {(flag) -> Void in
                        _ = self.dismiss(animated: true, completion: nil)
                    })
                    //                UtilityFunctions.showAlert(message: "We have successfully recieved the payment of $ \(price.toInt() ?? 0). Now you can chat with the concerned person for the folloup.", controller: self)
                    
                case .failureMessage(let message):
                    print("failure")
                    
                    UtilityFunctions.showAlert(message: message, controller: self)
                default: break
                }
                
            }

        }, viewController: self)
    }
    func fetchProfileData()
    {
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfile(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            switch response {
            case .success(let data):
                
                weak var weakSelf  = self
                print(data ?? "")
                let x = data as? User
                print(x?.access_token ?? "")
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                self.checkCreditCardDetails()
            case .failureMessage(let message):
                print(message ?? "")
                UtilityFunctions.showAlert(message: message, controller: self)
                
            default : break
            }
        }
        
    }
    func callBidVC()
    {
        guard let addBidVc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangeBidViewController") as? ChangeBidViewController else { return }
        //data for bidding
        var typeForBid = Int()
        switch typeToPass ?? 100 {
        case 5:
            typeForBid = 9
        case 6:
            typeForBid = 10
        case 7:
            typeForBid = 11
        default:
            break
        }
        let remainingDays = UtilityFunctions.calculateRemainingDays(createdOn, duration)
        print(remainingDays)
        
        addBidVc.remainingDays = remainingDays
        addBidVc.type = typeForBid
        addBidVc.postsId = postId
        addBidVc.usersId = usersId
        addBidVc.minBid = minBid
        addBidVc.maxBid = maxBid
        addBidVc.price = totalAmount?.toString
        addBidVc.cardDetailVar = self
        //print(usersId)
        addBidVc.presentingVeiw = "CardDetail"
        self.presentVC(addBidVc)
    }
    
    
}



extension CardDetailViewController: DelegateShowSlideShow {
    func showSlideShow(arrayimage : [String])
    {
        guard let slideShowVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SlideShowViewController") as? SlideShowViewController else { return}
        slideShowVC.arrayImages = arrayimage
        self.presentVC(slideShowVC)
        
    }
}







//MARK:- Fetch Notification

extension CardDetailViewController {
    
    func fetchNotificationsDetail () {
        
        UtilityFunctions.startColoredLoader()
        //self.loaderLoadFirstTime = 0
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.notificationDetail(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone : APIConstants.timezone,posts_id: postIdNoti)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 0
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                self.arrayNotificationDetail = map
                self.viewAlertDetail()
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
                
            }
        }
        UtilityFunctions.stopLoader()
    }
    
    
    func viewAlertDetail(){
        
        //        fetchNotificationsDetail()
        guard let modal = arrayNotificationDetail?[0] else { return }
        
        self.currentData = modal as AnyObject?
        let modalType = (modal.type).unwrap()
        callingView = "CurrentListing"
        self.usersId = modal.users_id
        self.postId = modal.posts_id
        self.amounts = modal.price
        
        switch (modalType) {
        case "1" :
            self.currentPage = pageName.ride.rawValue
            self.cardIndex = 0
        case "3":
            self.currentPage = pageName.service.rawValue
            self.cardIndex = 1
        case "5" :
            self.currentPage = pageName.books.rawValue
            self.cardIndex = 3
        case "6":
            self.currentPage = pageName.tickets.rawValue
            self.cardIndex = 2
        case "7":
            self.currentPage = pageName.item.rawValue
            self.cardIndex = 4
        default :
            break
        }
        
        self.typeToPass = modalType.toInt()
        
        self.SettingUI()
        self.setData()
        self.tableView.reloadData()
        
    }
}

