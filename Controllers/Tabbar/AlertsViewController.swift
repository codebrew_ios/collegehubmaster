//
//  AlertsViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 25/01/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import EZSwiftExtensions

class AlertsViewController: UIViewController{

//MARK::- VC VARIABLES
    var arrayNotification = [NotificationModal]()
    var arrayNotificationDetail:[ProfileTabSubMenus]?
    var page = 1
    var posts_id = ""
    var varSec = 0
    var varRow = 0
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    var arrayModal = ["" ,"has booked your ride offer for ","","has booked your service offer ","","has bought your book ","has bought your ticket ","has bought your item ","has approved payment for ","has placed a bid for your book ","has placed a  bid for your ticket ","has placed a bid for your item  "]
    
    let addAdVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
//MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!

//MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        pullToRefresh()
        pullToRefreshUpward()
        fetchNotifications()
        
        tempRefresh = "first"
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 140;//(Maximum Height that should be assigned to your cell)
        
    }

}
//MARK::- TABLE VIEW DELEGATES
extension AlertsViewController:UITableViewDelegate,UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayNotification.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNotification[section].details.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(30)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        headerView?.lblFormattedDate.text = (arrayNotification[section].formatted_date)
        headerView?.lblTimeSince.text = (arrayNotification[section].time_since)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlertCell" , for: indexPath) as? AlertTableViewCell  else { return UITableViewCell() }
//        cell.layer.masksToBounds = true
//        let border = CALayer()
//        border.frame = CGRect(x: 4, y: 0 , width:  cell.frame.size.width - 4 , height: cell.frame.size.height - 4)
//        cell.layer.borderColor = UIColor( red: 153/255, green: 153/255, blue:0/255, alpha: 1.0 ).cgColor
//        cell.layer.borderWidth = 2.0
         let modal = (arrayNotification[indexPath.section].details[indexPath.row])
        cell.selectionStyle = .none
        cell.imgUserProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        cell.imgUserProfileImage?.sd_setImage(with: URL(string: (modal.image100).unwrap()), placeholderImage: UIImage(imageLiteralResourceName: "ic_profile"))
        cell.lblNotificationDate.text = (modal.time)
        let userName = String(format:"%@ %@ ",(modal.firstname).unwrap(),(modal.lastname).unwrap() )
        let notificationTitle = (modal.title).unwrap()
        let notificationType = (modal.type).unwrap().toInt() ?? 0
        switch notificationType {
        case 1 :
            cell.lblNotification.text = userName + arrayModal[notificationType] + "\"" + notificationTitle + "\""
        case 3:
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" +  notificationTitle + "\""
        case 5 :
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 6:
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 7:
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 8 :
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 9:
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 10 :
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        case 11:
            cell.lblNotification.text = userName + arrayModal[notificationType]  + "\"" + notificationTitle + "\""
        default :
            cell.lblNotification.text = userName
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        posts_id = (arrayNotification[indexPath.section].details[indexPath.row].posts_id).unwrap()
        varSec = indexPath.section
        varRow = indexPath.row
       // fetchNotificationsDetail()
        
        guard let VC = self.storyboard?.instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController else{return}
        VC.postIdNoti = posts_id
        VC.notiCheck = 1
        self.presentVC(VC)
         // self.navigationController?.pushViewController(VC, animated: true)
       // viewAlertDetail(section: indexPath.section, row: indexPath.row, view: "CurrentListing")
    }
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchNotifications ()
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchNotifications()
        }
        
    }
    
}
//MARK::- API CALLS
extension AlertsViewController {
    
    func fetchNotifications () {
        
//        if(tempRefresh != "Header" && tempRefresh != "Footer"){
//            UtilityFunctions.startColoredLoader()
//        }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.notification(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone : APIConstants.timezone,page: page.toString)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [NotificationModal] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayNotification.append(contentsOf: map)
                case "Header":
                    self.arrayNotification = map
                default :
                    self.arrayNotification = map
                }
                //self.arrayNotification.append(contentsOf: map)
                self.tblView?.delegate = self
                self.tblView?.dataSource = self
                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
  
    }
    func fetchNotificationsDetail () {
        
        UtilityFunctions.startColoredLoader()
         //self.loaderLoadFirstTime = 0
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.notificationDetail(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone : APIConstants.timezone,posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 0

            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                self.arrayNotificationDetail = map
                self.viewAlertDetail(section: self.varSec, row: self.varRow, view: "CurrentListing")
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                default : break
                
            }
        }
       UtilityFunctions.stopLoader() 
    }
}
extension AlertsViewController {
    func viewAlertDetail(section: Int, row: Int, view: String){
//        fetchNotificationsDetail()
        guard let modal = arrayNotificationDetail?[0] else { return }
        guard let addAdVc = addAdVc else { return }
        addAdVc.currentData = modal as AnyObject?
        let modalType = (modal.type).unwrap()
        addAdVc.callingView = "CurrentListing"
        addAdVc.usersId = modal.users_id
        addAdVc.postId = modal.posts_id
//        addAdVc.maxBid = modal.maxBid
//        addAdVc.minBid = modal.minBid
        addAdVc.amounts = modal.price
//        addAdVc.createdOn = modal.createdAt
//        addAdVc.duration = modal.duration
       // addAdVc.isFixed = modal.is_fixed?.toInt() ?? 100
        switch (modalType) {
        case "1" :
           addAdVc.currentPage = pageName.ride.rawValue
            addAdVc.cardIndex = 0
        case "3":
            addAdVc.currentPage = pageName.service.rawValue
            addAdVc.cardIndex = 1
        case "5" :
            addAdVc.currentPage = pageName.books.rawValue
            addAdVc.cardIndex = 3
        case "6":
            addAdVc.currentPage = pageName.tickets.rawValue
            addAdVc.cardIndex = 2
        case "7":
            addAdVc.currentPage = pageName.item.rawValue
            addAdVc.cardIndex = 4
        default :
           break
        }

        addAdVc.typeToPass = modalType.toInt()
        ez.runThisInMainThread {
            self.presentVC(addAdVc)
        }
        
    }
}
