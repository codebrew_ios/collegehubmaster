//
//  PostAdViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 25/01/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class PostAdViewController: UIViewController {

//MARK:- OUTLETS
    
    @IBOutlet weak var btnPopView: UIButton?
    @IBOutlet weak var btnOfferARide: UIButton!
    @IBOutlet weak var btnOfferAService: UIButton!
    @IBOutlet weak var btnSellATicket: UIButton!
    @IBOutlet weak var btnSellABook: UIButton!
    @IBOutlet weak var btnSellAnItem: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    //MARK::- Variables
    var flag = true
    
    
//MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navBar?.isHidden = true
        
        btnOfferARide.layer.cornerRadius = btnRadius
        btnOfferAService.layer.cornerRadius = btnRadius
        btnSellATicket.layer.cornerRadius = btnRadius
        btnSellABook.layer.cornerRadius = btnRadius
        btnSellAnItem.layer.cornerRadius = btnRadius
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navBar?.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "showMenuButton"),
            object: nil,
            userInfo:nil)
        
        
        if flag == true {
            btnPopView?.isHidden = true
        }
        else {
            btnPopView?.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//MARK:- BTN ACTION   
    
    /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
    @IBAction func btnOfferARideClick(_ sender: Any) {
        let addAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 1
        addAdVc.adType = "Ride"
        self.presentVC(addAdVc)
//        self.navigationController?.pushViewController(addAdVc, animated: true)
    }
    
    @IBAction func btnOfferAServiceClick(_ sender: Any) {
        let addAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 3
        addAdVc.adType = "Service"
        self.presentVC(addAdVc)
//        self.navigationController?.pushViewController(addAdVc, animated: true)
    }
    
    @IBAction func btnSellATickerClick(_ sender: Any) {
        let addAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 6
        addAdVc.adType = "Ticket"
        self.presentVC(addAdVc)
//        self.navigationController?.pushViewController(addAdVc, animated: true)
    }
    
    @IBAction func btnSellABookClick(_ sender: Any) {
        let addAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 5
        addAdVc.adType = "Book"
        self.presentVC(addAdVc)
//        self.navigationController?.pushViewController(addAdVc, animated: true)
    }
    
    @IBAction func btnSellAnItemClick(_ sender: Any) {
        let addAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
        addAdVc.typeOfConversation = 7
        addAdVc.adType = "Item"
        self.presentVC(addAdVc)
//        self.navigationController?.pushViewController(addAdVc, animated: true)
    }
    
    @IBAction func btnDismissControllerClick(_ sender: Any) {
        _ = self.dismiss(animated: true, completion: nil)
    }
    
    
    //    @IBAction func rideBtnAction(_ sender: UIButton) {
//        let AddAdVc = self.storyboard?.instantiateViewController(withIdentifier: "AddAdViewController") as! AddAdViewController
//        self.navigationController?.pushViewController(AddAdVc, animated: true)
//    }

}
