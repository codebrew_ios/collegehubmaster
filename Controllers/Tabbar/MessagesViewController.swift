//
//  MessagesViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 25/01/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.

import UIKit

class MessagesViewController: UIViewController {
    
//MARK:- VARIABLE
    var arrayMatches = [MessageModel]()
    var arrayMessages = [MessageModel]()
    var filteredMessages = [MessageModel]()//edited
    var temp = ""//edited
    let searchController = UISearchController(searchResultsController: nil)//edited
    var loaderLoadFirstTime = 0
    
//MARK:- VC OUTLETS
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtFieldSearch: UITextField!
    
//MARK:- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar?.isHidden = true
        txtFieldSearch.delegate = self

        
        // creating a observer for the notification
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(MessagesViewController.popUpVC), name: NSNotification.Name(rawValue: "ColllectionCellSelected"), object: nil)
        
    }
    
//MARK::- Notification
    //notification observer selector function
    func popUpVC(notification: Notification) {
        
        //setting the user_id in order to pass it to the chat view controller
        if let model = notification.userInfo?["model"] as? MessageModel {
                        
            let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
            chatVC?.user_id2 = model.users_id2
            chatVC?.model = model
            chatVC?.newMatch = true
            self.presentVC(chatVC!)
            //self.navigationController?.pushViewController(chatVC!, animated: true)
            //self.present(chatVC!, animated: true, completion: nil)
            
        }
    }

    override func viewWillAppear(_ animated: Bool) {
       // txtFieldSearch.addTarget(self, action:#selector(MessagesViewController.myTargetFunction(textField:)), for: UIControlEvents.touchDown)
        //self.navBar?.isHidden = true
        //self.tabBarController?.tabBar.isHidden = false
        self.txtFieldSearch.text = ""
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "showMenuButton"),
            object: nil,
            userInfo:nil)
        getConversationList(searchKey: "")
        
    }
//MARK:- FUNCTIONS    
    //edited
    func filterContentForSearchText(searchText: String, scope: String = "All") {
            
        filteredMessages = arrayMessages.filter { MessageModel in
            let categoryMatch = (scope == "All") || (MessageModel.firstname == scope)

         return  categoryMatch && ( ((MessageModel.firstname?.lowercased().contains(searchText.lowercased()))! && (MessageModel.firstname?.lowercased().characters.first == searchText.lowercased().characters.first)) || ((MessageModel.lastname?.lowercased().contains(searchText.lowercased()))! && (MessageModel.lastname?.lowercased().characters.first == searchText.lowercased().characters.first)))
       
      //  }
        }
        
        tblView.reloadData()
    }
   
}

//MARK:- TABLE VIEW DELEGATES
extension MessagesViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sectionCount = 0
        if arrayMatches.count > 0{
            sectionCount = 1
            if arrayMessages.count > 0 {
                sectionCount = 2
            }
        }else{
            if arrayMessages.count > 0 {
                sectionCount = 1
            }
        }
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        if arrayMatches.count > 0{
            if section == 0 {
                headerView?.lblFormattedDate.text = ""
                headerView?.lblTimeSince.text = "New Matches"
            }
            if arrayMessages.count > 0{
                if section == 1 {
                    headerView?.lblFormattedDate.text = ""
                    headerView?.lblTimeSince.text = "Conversations"
                }
            }
        }else{
            if arrayMessages.count > 0{
                if section == 0 {
                    headerView?.lblFormattedDate.text = ""
                    headerView?.lblTimeSince.text = "Conversations"
                }
            }
        }
        
        return headerView
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayMatches.count > 0{
            if (section == 0){
                return 1
            }
            if arrayMessages.count > 0 {
                if (section == 1){
//                    if temp != ""  {
//                        return filteredMessages.count
//                    }
                    return arrayMessages.count
                }
            }
        }else{
            if (section == 0){
                if arrayMessages.count > 0 {
//                    if temp != ""  {
//                        return filteredMessages.count
//                    }
                        return arrayMessages.count
                    }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var heightForRow = Int()
        
        if arrayMatches.count > 0{
            if (indexPath.section == 0){
                heightForRow = 120
            }
            if arrayMessages.count > 0 {
                if (indexPath.section == 1){
                    heightForRow = 100
                }
            }
        }else{
            if (indexPath.section == 0){
                if arrayMessages.count > 0 {
                    heightForRow = 100
                }
            }
        }
        
        return CGFloat(heightForRow)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if arrayMatches.count > 0{
            if (indexPath.section == 0){
                let cellMatches = tableView.dequeueReusableCell(withIdentifier: "MatchesTableViewCell", for: indexPath) as! MatchesTableViewCell
                 cellMatches.selectionStyle = .none
                cellMatches.arrayMatches = arrayMatches
                cellMatches.collectionView.reloadData()
                
                return cellMatches
            }
            if arrayMessages.count > 0 {
                if (indexPath.section == 1){
                }
            }
        }else{
            if (indexPath.section == 0){
                if arrayMessages.count > 0 {
                }
            }
        }
        
        let cellMessages = tableView.dequeueReusableCell(withIdentifier: "MessagesTableViewCell", for: indexPath) as! MessagesTableViewCell
        cellMessages.selectionStyle = .none

        var modal = arrayMessages[indexPath.row]
        //edited
   // if searchController.isActive && searchController.searchBar.text != "" {
//      if temp != ""  {
//       
           // modal = filteredMessages[indexPath.row]
//        } else {
            modal = arrayMessages[indexPath.row]
       // }
        //
        cellMessages.lblUsername.text = modal.firstname! + " " + modal.lastname!
        cellMessages.lblLastMsg.text = modal.message
        
        if modal.unread_count == "0" {
            cellMessages.lblUnreadCount.isHidden = true
        }
        else {
            cellMessages.lblUnreadCount.isHidden = false
            cellMessages.lblUnreadCount.text = modal.unread_count.unwrap()
        }
        
        cellMessages.imgView?.sd_setImage(with: NSURL.init(string: (modal.image100)!)! as URL!)
        
        return cellMessages
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrayMatches.count > 0{
            if (indexPath.section == 0){
                return
            }
            if arrayMessages.count > 0 {
                if (indexPath.section == 1){
                }
            }
        }else{
            if (indexPath.section == 0){
                if arrayMessages.count > 0 {
                }
            }
        }
        
            guard let ChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { return }
            let data = arrayMessages[indexPath.row]
            
            ChatVC.lastMessageId = data.message_id
            ChatVC.user_id2 = data.users_id2
            guard let firstName = arrayMessages[indexPath.row].firstname, let lastName = arrayMessages[indexPath.row].lastname else { return }
            ChatVC.userName = "\(firstName) \(lastName)"
            ChatVC.newMatch = false
            self.presentVC(ChatVC)
            //self.navigationController?.pushViewController(ChatVC!, animated: true)
    }
}
//edited
//extension MessagesViewController : UISearchResultsUpdating {
//   
//    func updateSearchResults(for searchController: UISearchController) {
//         print("bar")
//      filterContentForSearchText(searchText: searchController.searchBar.text!)
//       print("bar2")
//    }
//}

//MARK:- TEXT FIELD DELEGATES
extension MessagesViewController : UITextFieldDelegate {
//    func myTargetFunction(textField: UITextField) {
//        print("bar1")
//        filterContentForSearchText(searchText: txtFieldSearch.text!)
//    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        filterContentForSearchText(searchText: txtFieldSearch.text!)
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(string != ""){
        temp = textField.text! + string
        }
        else {
          temp = String(textField.text!.characters.dropLast())
        }
        
        if(temp == ""){
            tblView.reloadData()
        }
        getConversationList(searchKey: temp)
        
    return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let temp = (textField.text).unwrap()
       getConversationList(searchKey: temp)
    }

}

//MARK:- API CALLS
extension MessagesViewController{
    
    func getConversationList(searchKey: String){
        
//        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.getConversationList(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, search_key: searchKey)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                
                guard let arrayOfDicts = data as? [String:AnyObject] else { return }
                
                self.arrayMatches = (arrayOfDicts[APIConstants.matches] as? [MessageModel]) ?? []
                self.arrayMessages = (arrayOfDicts[APIConstants.messages] as? [MessageModel]) ?? []
                
                self.tblView.delegate = self
                self.tblView.dataSource = self
                self.tblView.reloadData()
                
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
                
                
            }
        }
    }
    
    
}



