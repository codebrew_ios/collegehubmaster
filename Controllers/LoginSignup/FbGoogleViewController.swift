//
//  FbGoogleViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 25/01/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftyJSON
import CoreLocation

class FbGoogleViewController: UIViewController ,GIDSignInUIDelegate{
    
    //MARK::- VARIABLES
    var dict : [String : AnyObject]!
    let fbManger = FBSDKLoginManager()
    var loaderLoadFirstTime = 0
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnGoogleLogin: UIButton!
    @IBOutlet weak var btnTermsConditions: UIButton!
    @IBOutlet weak var btnSignInEmail: UIButton!
    @IBOutlet weak var btnFbLogin: UIButton!
    
    //MARK::- ACTIONS
    @IBAction func btnGoogleLoginAction(_ sender: UIButton) {
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied {
            GIDSignIn.sharedInstance().signIn()
            
        }else {
            
            UtilityFunctions.alertForLocationAccess(vc:self)
        }
    }
    
    @IBAction func btnSignInEmailAction(_ sender: UIButton) {
        
        
    }
    
    @IBAction func btnFbLoginAction(_ sender: UIButton) {
        
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied {
            
            FacebookManager.sharedInstance.fbLogin(fbManger, viewcontroller: self, Success: { (response) in
                
                self.callFacebookAccountLogin(accountType: "0", response: response as! Facebook)
            }) { (error) in
                
                //            _ = SweetAlert().showAlert(helperNames.oops.rawValue, subTitle: error, style: AlertStyle.none)
            }
            
        }else {
            
            UtilityFunctions.alertForLocationAccess(vc:self)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initialise()
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FbGoogleViewController.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true);
        loaderLoadFirstTime = 0
    }
    
    func initialise(){
        //        self.navBar?.isHidden = true
        btnFbLogin.layer.borderWidth = 1.0
        btnFbLogin.layer.borderColor = UIColor.white.cgColor
        btnFbLogin.layer.cornerRadius = btnRadius
        btnGoogleLogin.layer.borderWidth = 1.0
        btnGoogleLogin.layer.borderColor = UIColor.white.cgColor
        btnGoogleLogin.layer.cornerRadius = btnRadius
        btnTermsConditions.titleLabel?.numberOfLines = 2
        btnTermsConditions.titleLabel?.adjustsFontSizeToFitWidth = true
        btnTermsConditions.titleLabel?.textAlignment = NSTextAlignment.center
    }
    
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            if notification.userInfo != nil {
                if let googleUser = notification.userInfo?["data"] as? GIDGoogleUser {
                    print(googleUser.userID)
                    
                    self.callGoogleAccountLogin(accountType: "1", response: googleUser)
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                  object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FbGoogleViewController{
    
    func callFacebookAccountLogin(accountType:String?,response: Facebook){
        
        let fbModel = response
        print(fbModel)
        UtilityFunctions.startColoredLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.socialAccountLogin(account_type: accountType, fb_id: fbModel.fbId,google_id: "", firstname: fbModel.firstName, lastname: fbModel.lastName, image: fbModel.imageUrl, email: fbModel.email, gender:"", dob: "", device_token: UserDefaults.standard.string(forKey: "device_token"))),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 0
            UtilityFunctions.stopLoader()
            switch response{
                
            case .success_wrongPassword(let data):
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                UtilityFunctions.show(alert: "", message: "You have not verified your email.\n Do you want to resend verification mail??", buttonOk: { self.resendVerificationMail()
                }
                    , viewController: self)
                
            case .success(let data) :
                
                print(data ?? "")
                
                let x = data as? User
                print(x?.access_token ?? "")
                
                //if(success == 1){
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                if UserDataSingleton.sharedInstance.loggedInUser?.is_verified != nil && UserDataSingleton.sharedInstance.loggedInUser?.is_verified == "1"{
                    let tabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabbarController") as? CustomTabbarController
                    self.presentVC(tabbarVC!)
                    
                }
                else if /*UserDataSingleton.sharedInstance.loggedInUser?.is_verified != nil && */UserDataSingleton.sharedInstance.loggedInUser?.is_verified == "0"{
                    //                    UtilityFunctions.show(alert: "", message: "You have not completed sign up process completely.\n Want to complete it now?", buttonOk: {
                    //                        guard let verifyEduVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController else { return }
                    //                        self.navigationController?.pushViewController(verifyEduVC, animated: true)
                    //                    }, viewController: self)
                    //}
                    guard let verifyEduVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController else { return }
                    self.navigationController?.pushViewController(verifyEduVC, animated: true)
                }
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
                
                
            }
        }
    }
    
    func callGoogleAccountLogin(accountType:String?,response: GIDGoogleUser){
        
        var image: String?
        if response.profile.hasImage{
            let imageUrl = response.profile.imageURL(withDimension:120)
            //print(" image url: ", imageUrl?.absoluteString ?? "")
            image = imageUrl?.absoluteString
        }else{
            image = ""
        }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.socialAccountLogin(account_type: accountType, fb_id:"" ,google_id: response.userID, firstname: response.profile.givenName, lastname: response.profile.familyName, image: image, email: response.profile.email, gender:"", dob: "", device_token: UserDefaults.standard.string(forKey: "device_token"))),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 0
            
            switch response{
                
            case .success_wrongPassword(let data):
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                UtilityFunctions.show(alert: "", message: "You have not verified your email.\n Do you want to resend verification mail??", buttonOk: { self.resendVerificationMail()
                }
                    , viewController: self)
                
                
            case .success(let data) :
                
                print(data ?? "")
                let x = data as? User
                print(x?.access_token ?? "")
                
                UserDataSingleton.sharedInstance.loggedInUser = data as? User
                //print(UserDataSingleton.sharedInstance.loggedInUser ?? "")
                
                if UserDataSingleton.sharedInstance.loggedInUser?.is_verified != nil && UserDataSingleton.sharedInstance.loggedInUser?.is_verified == "1"{
                    let tabbarVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabbarController") as? CustomTabbarController
                    self.presentVC(tabbarVC!)
                }else if UserDataSingleton.sharedInstance.loggedInUser?.is_verified == "0" {
                    //                    UtilityFunctions.show(alert: "", message: "You have not completed sign up process completely.\n Want to complete it now?", buttonOk: {
                    //                        guard let verifyEduVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController else { return }
                    //                        self.navigationController?.pushViewController(verifyEduVC, animated: true)
                    //     }, viewController: self)
                    
                    guard let verifyEduVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController else { return }
                    self.navigationController?.pushViewController(verifyEduVC, animated: true)
                }
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
                
                
            }
        }
    }
    func resendVerificationMail()
    {
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.resendCode(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                let message = data as! JSON
                print(message.stringValue)
                UtilityFunctions.showAlert(message: "Please verify yourself by clicking on the Verification Link that we have sent on your college Email ID and then try to login", controller: self)
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
            }
        }
        
    }
    
}
