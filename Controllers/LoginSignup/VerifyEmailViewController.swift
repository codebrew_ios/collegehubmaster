//
//  VerifyEmailViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 09/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import SwiftyJSON
import TTGSnackbar

class VerifyEmailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnVerifyOutlet: UIButton!
    @IBOutlet weak var txtFieldEmail1: UITextField!
    @IBOutlet weak var txtFieldEmail2: UITextField!
    @IBOutlet weak var btnResendCodeOutlet: UIBarButtonItem!
    var snackbar:TTGSnackbar?
    var loaderLoadFirstTime = 0
    
    @IBAction func btnVerify(_ sender: Any) {
        
        if case .success(_) = User.validateEduEmailFields(txtFieldEmail1.text, emailExtension: txtFieldEmail2.text, controller: self){
            dismissKeyboard()
            
            let eduEmail = txtFieldEmail1.text! + "@" + txtFieldEmail2.text!
            
            APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.sendEmailToEduId(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, edu_email: eduEmail)),loaderNeeded:loaderLoadFirstTime) { (response) in
                self.loaderLoadFirstTime = 1

                switch response{
                    
                case .success(let data) :
                    
                    print(data ?? "")
                    let message = data as! JSON
                    print(message.stringValue)
                    self.snackbar?.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
                    
                    // Change margin
                    self.snackbar?.leftMargin = 8
                    self.snackbar?.rightMargin = 8
                    
                    // Change message text font and color
                    self.snackbar?.messageTextColor = appColor
                    self.snackbar?.messageTextFont = UIFont.boldSystemFont(ofSize: 18)
                    
                    // Change snackbar background color
                    self.snackbar?.backgroundColor = UIColor.white
                    
                    // Change animation duration
                    self.snackbar?.animationDuration = 10.0
                    self.btnResendCodeOutlet.tintColor = UIColor.white
                    self.btnResendCodeOutlet.isEnabled = true
                    self.btnResendCodeOutlet.title = "Resend"
                    self.navigationController?.navigationBar.backIndicatorImage?.accessibilityElementsHidden = false
                    self.snackbar = TTGSnackbar.init(message: "Please verify yourself by clicking on the Verification Link that we have sent on your college Email ID and then try to login", duration: TTGSnackbarDuration.forever, actionText: "OK")
                    { (snackbar) -> Void in
                        let _ = self.storyboard?.instantiateViewController(withIdentifier: "FbGoogleViewController") as? FbGoogleViewController
                        //let fbgoogleVC = self.storyboard?.instantiateViewController(withIdentifier: "FbGoogleViewController") as? FbGoogleViewController
                        let controllers = self.navigationController?.viewControllers
                        for vc in controllers! {
                            if vc is FbGoogleViewController {
                                _ = self.navigationController?.popToViewController(vc as! FbGoogleViewController, animated: true)
                            }
                        }
                        snackbar.dismiss()
                        
                    }
                    self.snackbar?.show()
                    self.btnVerifyOutlet.isHidden = true
                    
                    

                case .failureMessage(let message):
                    print("failure")
                    
                    self.snackbar?.dismiss()
                    UtilityFunctions.showAlert(message: message, controller: self)
                    
                default : break
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnResendCodeOutlet.tintColor = .clear
        btnResendCodeOutlet.isEnabled = false
        btnResendCodeOutlet.title = ""
        
       
        
        self.navBar?.isHidden = false
        
        //Remove bottom border of navigation bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        //Back button setting
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_back_white")
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnResendCode(_ sender: Any) {
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.resendCode(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            switch response{
                
            case .success(let _ ) :
                
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true);
                self.snackbar?.dismiss()
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
            }
        }

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFieldEmail1{
            txtFieldEmail2.becomeFirstResponder()
            return true
        }else if textField == txtFieldEmail2{
            txtFieldEmail2.resignFirstResponder()
            return true
        }
        return true
    }
}
