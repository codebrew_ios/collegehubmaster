//
//  SettingsViewController.swift
//  CollegeHub
//
//  Created by Sierra 2 on 09/06/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var lblSliderValue: UILabel!
    @IBOutlet weak var horizontalSlider: UISlider!
    //var loaderLoadFirstTime = 0
    
    @IBAction func btnBack(_ sender: Any) {
        changeRadiusUpdate()
    }
    
    

    @IBAction func sliderValue(_ sender: UISlider) {
        var currentValue = Int(sender.value)
        lblSliderValue.text = "\(currentValue) miles"
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let currentRadius = UserDataSingleton.sharedInstance.loggedInUser?.radius else {return}
        horizontalSlider.setValue(currentRadius.toFloat()!, animated: true)
        lblSliderValue.text = "\(String(describing: currentRadius)) miles"

    }
}
extension SettingsViewController {
    func changeRadiusUpdate(){
        let currentRadius = (horizontalSlider.value)
        let user = UserDataSingleton.sharedInstance.loggedInUser
UtilityFunctions.startColoredLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.editProfile(access_token: user?.access_token, firstname: user?.firstname, lastname: user?.lastname, gender: user?.gender, dob: user?.dob, radius: String(describing: currentRadius))), image: UIImage() , completion: { (response) in
    
                UtilityFunctions.stopLoader()
                
                switch response{
                
                case .success(let data) :
                
                    print(data ?? "")
                    let x = data as? User
                    print(x?.access_token ?? "")
                    
                    UserDataSingleton.sharedInstance.loggedInUser = data as? User
                    UtilityFunctions.showAlertAndDismiss(message: "Settings updated succcessfully.", controller: self, dismissController: {(flag) -> Void in
                            if flag == true {
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            })
                    print(UserDataSingleton.sharedInstance.loggedInUser ?? "")
                
                case .failureMessage(let message):
                    print("failure")
                    UtilityFunctions.showAlert(message: message, controller: self)
                    default : break
                }
            })
    }

}
