//
//  PastOrdersViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 15/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import NVActivityIndicatorView

class PastOrdersViewController: UIViewController {
    
    //MARK::- VC VARIABLES
    var arrayPastOrder = [ProfileTabSubMenus]()
    var page = 1
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    
//MARK:- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
    
//MARK:- VC LIFE CYCLE
     override func viewDidLoad() {
        super.viewDidLoad()
        fetchPastOrder ()
        pullToRefresh()
         pullToRefreshUpward()
        self.lblNoResultFound.isHidden = true
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        tblView.delegate = self
        tblView.dataSource = self
      
    }

//MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- TABLE VIEW DELEGATES
extension PastOrdersViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayPastOrder.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPastOrder[section].details.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(30)
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        headerView?.lblFormattedDate.text = (arrayPastOrder[section].formatted_date)
        headerView?.lblTimeSince.text = (arrayPastOrder[section].time_since)
        return headerView
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modal = (arrayPastOrder[indexPath.section].details[indexPath.row])
        if let type = modal.type?.toInt(){
            switch type {
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.pastOrderTripCell.rawValue , for: indexPath) as? PastOrdersTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProgileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                cell.lblTitlePayed.text = "Paid"//modal.price
                cell.lblDate.text = modal.formatted_date
                cell.lblDestinationFrom.text = modal.departure
                cell.lblDestinationTo.text = modal.destination
                cell.lblTitleTripDetails.text = "Trip Details"
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                 let price = (modal.price).unwrap().toInt()! * (modal.num_of_seats).unwrap().toInt()!
                cell.lblPayed.text = "$ " +  price.toString
                return cell
            case 3,5,6,7:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.pastOrderItemCell.rawValue , for: indexPath) as? PastOrderItemOrServiceCellTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                cell.lblDate.text = modal.formatted_date
                cell.lblItemOrServiceProvided.text = modal.name
                cell.lblAmountPayedOrEarned.text = "$ " + modal.price.unwrap()
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                cell.lblTitlePayedOrEarned.text = "Paid"
                cell.lblTitleDate.text = "Date"
                switch type {
                case 3:
                    cell.lblTitleItemOrservice.text = "Service Title"
                    cell.lblItemOrServiceProvided.text = modal.service_name
                case 5:
                    cell.lblTitleItemOrservice.text = "Book Title"
                case 6:
                    cell.lblTitleItemOrservice.text = "Ticket Details"
                    let price = (modal.price).unwrap().toInt()! * (modal.num_of_tickets).unwrap().toInt()!
                    cell.lblAmountPayedOrEarned.text = "$ " + price.toString
                case 7:
                    cell.lblTitleItemOrservice.text = "Item Details"

                default:
                    break
                }
                
                return cell
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.approvePaymentBookCell.rawValue , for: indexPath) as? ApprovePaymentsBooksTableViewCell  else {return UITableViewCell() }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchPastOrder()
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchPastOrder()
        }
        
    }

}

//MARK:- API CALLS
extension PastOrdersViewController {
    func fetchPastOrder() {
        
//        if(tempRefresh != "Header" && tempRefresh != "Footer"){
//            UtilityFunctions.startColoredLoader()
//        }
        let type = "5"
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfileData(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page : page.toString,data_for: type)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayPastOrder.append(contentsOf: map)
                case "Header":
                    self.arrayPastOrder = map
                default :
                    self.arrayPastOrder = map
                }
                if self.arrayPastOrder.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                self.tblView?.delegate = self
                self.tblView?.dataSource = self
                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
        
}
}

    
