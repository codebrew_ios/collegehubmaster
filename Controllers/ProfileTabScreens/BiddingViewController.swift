//
//  BiddingViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 27/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class BiddingViewController: UIViewController {

//MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
//MARK::- Variables
    
    var arrayBidding = [ProfileTabSubMenus]()
    var page = 1
    var passBidValues : ProfileTabSubMenus?
    var loaderLoadFirstTime = 0
    var remainingDays = ""
    var tempRefresh = ""
    
     let addBidVc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangeBidViewController") as? ChangeBidViewController

//MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoResultFound.isHidden = true
        pullToRefresh()
        pullToRefreshUpward()
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 400;//(Maximum Height that should be assigned to your cell)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchBiddingData()
    }
//MARK::- Functions
    func increaseBidViewController() {
        guard let addBidVc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangeBidViewController") as? ChangeBidViewController else { return }
        addBidVc.presentingVeiw = "BiddingView"
        self.navigationController?.pushViewController(addBidVc, animated: true)
    }
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchBiddingData()
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchBiddingData()        }
        
    }

//MARK::- BTN ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}


//MARK::- TABLE VIEW DELEGATES
extension BiddingViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayBidding.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(arrayBidding[indexPath.section].details[indexPath.row].type == "5" || arrayBidding[indexPath.section].details[indexPath.row].type == "7"){
            return UITableViewAutomaticDimension
        }
        else {
            return 212
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBidding[section].details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.biddingCell.rawValue , for: indexPath) as? BiddingTableViewCell  else {return UITableViewCell() }
        let modal = (arrayBidding[indexPath.section].details[indexPath.row])
        if (modal.type.unwrap() == "5" || modal.type.unwrap() == "7"){
            cell.constraintCollectionViewHeight.constant = 64
            guard let imagesArray = modal.image, let imageURL = modal.image100 else { return cell }
            cell.type = "1"
            cell.productImages = imagesArray
            
            
        }
        else {
            cell.constraintCollectionViewHeight.constant = 0
            cell.type = "2"
            cell.productImages = []
            
            
        }
        cell.selectionStyle = .none
        cell.section = indexPath.section
        cell.row = indexPath.row
        cell.lblUserName.text = modal.firstname.unwrap() + " " + modal.lastname.unwrap()
        cell.lblProductName.text = modal.name.unwrap()
        cell.lblTitleTopBid.text = "My Bid"
        cell.lblTopBidAmount.text = "$ " + modal.myBid.unwrap()
        cell.lblRating.text = String(format:" %@  ",modal.avg_rating.unwrap())
        //  cell.type = modal.type.unwrap()
        remainingDays = UtilityFunctions.calculateRemainingDays(modal.createdAt, modal.duration)
        cell.btnIncreadeBidOutlet.setTitle("Increase Bid(\(remainingDays))", for: .normal)
        guard  let imageURL = modal.image100 else { return cell }
        //        cell.prodImages = imagesArray
        cell.imgUserProfileImgage.sd_setImage(with: URL(string: imageURL))
        cell.myBiddingCell = self
        return cell

    }
}
//MARK::- Bidding Data
extension BiddingViewController {
    func fetchBiddingData() {
        
        if(tempRefresh != "Header" && tempRefresh != "Footer"){
            UtilityFunctions.startColoredLoader()
        }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfileData(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page : page.toString,data_for: "6")), loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                print("data testing")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayBidding.append(contentsOf: map)
                case "Header":
                    self.arrayBidding = map
                default :
                    self.arrayBidding = map
                }
                self.tempRefresh = ""
                if self.arrayBidding.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                self.tblView?.delegate = self
                self.tblView?.dataSource = self
                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
        
    }
    func viewChangeBidVC(section: Int, row: Int, view: String){
        let modal = (arrayBidding[section].details[row])
        guard let addBidVc = addBidVc else { return }
        remainingDays = UtilityFunctions.calculateRemainingDays(modal.createdAt, modal.duration)
        print(remainingDays)
        addBidVc.remainingDays = remainingDays
        addBidVc.type = modal.type?.toInt()
        addBidVc.postsId = modal.posts_id
        addBidVc.usersId = modal.users_id
        addBidVc.minBid = modal.minBid
        addBidVc.maxBid = modal.maxBid
        addBidVc.price = modal.price
        addBidVc.presentingVeiw = "BiddingCell"
        self.presentVC(addBidVc)
        }
    
       }




