//
//  CurrentListingViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 15/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import NVActivityIndicatorView
import EZSwiftExtensions

protocol DelegateCurrentListing {
    func removePost(_ postsId: String)
    func approveAction()
}

class CurrentListingViewController: UIViewController {
    
    //MARK::- VC VARIABLES
    var arrayCurrentListing = [ProfileTabSubMenus]()
    var page = 1
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    let addAdVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    //MARK:- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
    //MARK:- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        pullToRefresh()
        pullToRefreshUpward()
        lblNoResultFound.isHidden = true
        self.tblView?.delegate = self
        self.tblView?.dataSource = self
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        
        fetchCurrentListing()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CurrentListingViewController.deletePosts),
                                               name: Notification.Name(rawValue: "removePostFromCurrent"),
                                               object: nil)
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(CurrentListingViewController.reloadTable),
        //                                               name: Notification.Name(rawValue: "viewDetailsFromCurrent"),
        //                                               object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    //MARK::- Functions
    func deletePosts(not: Notification) {
        tblView.reloadData()
        
    }
    
    //MARK:- BTN ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- TABLE VIEW DELEGATES
extension CurrentListingViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayCurrentListing.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCurrentListing[section].details.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(30)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        headerView?.lblFormattedDate.text = (arrayCurrentListing[section].formatted_date)
        headerView?.lblTimeSince.text = (arrayCurrentListing[section].time_since)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modal = (arrayCurrentListing[indexPath.section].details[indexPath.row])
        if let type = modal.type?.toInt(){
            switch type {
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.currentListingTripCell.rawValue , for: indexPath) as? CurrentListingTableViewCell  else {return UITableViewCell() }
                cell.myTripTableViewController = self
                cell.lblDeparture.text = (modal.departure).unwrap()
                cell.lblDestination.text = (modal.destination).unwrap()
                cell.lblTitle.text = "Offering A Ride"
                cell.isEditable = (modal.is_editable).unwrap()
                cell.posts_id = (modal.posts_id).unwrap()
                cell.alertDelegate = self
                cell.section = indexPath.section
                cell.row = indexPath.row
                return cell
            case 3,5,6,7:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.currentListingBookCell.rawValue , for: indexPath) as? CurrentListingBooksTableViewCell  else {return UITableViewCell() }
                cell.myBookTableViewController = self
                cell.alertDelegate = self
                 cell.lblBooktTitle.text = modal.name
                cell.posts_id = (modal.posts_id).unwrap()
                switch type {
                case 3:
                    cell.lblTitle.text = "Offering A Service"
                    cell.lblDetailTitle.text = "Service Details"
                    cell.lblBooktTitle.text = modal.service_name
                case 5:
                    cell.lblTitle.text = "Selling A Book"
                    cell.lblDetailTitle.text = "Book Title"
                case 6:
                    cell.lblTitle.text = "Selling A Ticket"
                    cell.lblDetailTitle.text = "Ticket Details"
                case 7:
                    cell.lblTitle.text = "Selling An Item"
                    cell.lblDetailTitle.text = "Item Details"
                default:
                    break
                }
                cell.isEditable = (modal.is_editable).unwrap()
                cell.section = indexPath.section
                cell.row = indexPath.row
                cell.selectionStyle = .none
                return cell
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.currentListingBookCell.rawValue , for: indexPath) as? CurrentListingBooksTableViewCell  else {return UITableViewCell() }
                cell.myBookTableViewController = self
                return cell
            }
            
        }
        return UITableViewCell()
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("\(indexPath.row) is selected")
//        print("test")
//    }
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchCurrentListing()
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchCurrentListing()
        }
        
    }
    
}

extension CurrentListingViewController: DelegateUpdateCurrentListing {
    
    func showSingleAlert(message : String?) {
        UtilityFunctions.showSingle(alert: "", message: message!, buttonOk: {}, viewController: self)
    }
    func showAlert(message: String?,postsId: String?, section: Int?, row: Int?){
        UtilityFunctions.show(alert: "", message: message.unwrap(), buttonOk: {self.deletePost(postsId: postsId, section: section!, row: row!)}, viewController: self)
    }
    
}


//MARK:- API CALLS
extension CurrentListingViewController {

    
    func fetchCurrentListing () {
        
//        if(tempRefresh != "Header" && tempRefresh != "Footer"){
//            UtilityFunctions.startColoredLoader()
//        }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfileData(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page : page.toString,data_for: "3")),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayCurrentListing.append(contentsOf: map)
                case "Header":
                    self.arrayCurrentListing = map
                default :
                    self.arrayCurrentListing.append(contentsOf: map)
                }

                if self.arrayCurrentListing.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }

                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
        
    }
    
    //MARK::- DELETING POST
    func deletePost(postsId: String? , section: Int , row: Int ){
         UtilityFunctions.startColoredLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.deletePost(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: postsId)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            UtilityFunctions.stopLoader()
            switch response{
                
            case .success(let data) :
           
                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                UtilityFunctions.showSingle(alert: "", message: (message?.message ?? ""), buttonOk: {
                    let sections = section ?? 0
                    let rows = row ?? 0
                    self.updateTableView(section: sections, row: rows)}, viewController: self)
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message ?? "", controller: self)
            default : break
            }
        }
    }
    
}

extension CurrentListingViewController{
    func viewCurrentListing(section: Int, row: Int, view: String){
        let modal = (arrayCurrentListing[section].details[row])
        guard let addAdVc = addAdVc else { return }
        addAdVc.currentData = modal
        addAdVc.callingView = "CurrentListing"
        addAdVc.postId = modal.posts_id
        addAdVc.typeToPass = modal.type?.toInt()
        addAdVc.isFixed = modal.is_fixed?.toInt() ?? 100
        switch (modal.type).unwrap() {
        case "1" :
            addAdVc.currentPage = pageName.ride.rawValue
        case "3":
            addAdVc.currentPage = pageName.service.rawValue
        case "5" :
            addAdVc.currentPage = pageName.books.rawValue
        case "6":
            addAdVc.currentPage = pageName.tickets.rawValue
        case "7":
            addAdVc.currentPage = pageName.item.rawValue
        default :
            break
        }

        /* 0 - rider, 2 - services, 4 - books, 5 - tickets, 6 - items*/
        ez.runThisInMainThread {
            self.presentVC(addAdVc)
        }
        
    }
    func updateTableView(section: Int?,row:Int?)
    {
        let sections = section ?? 0
        let rows = row ?? 0
        self.arrayCurrentListing[sections].details.remove(at: rows)
        if self.arrayCurrentListing[sections].details.count == 0 {
            self.arrayCurrentListing.remove(at: sections)
            self.tblView.beginUpdates()
            self.tblView.deleteSections([sections], with: .left)
            self.tblView.deleteRows(at: [IndexPath(row: rows, section: sections)], with: .right)
            self.tblView.endUpdates()
        }else {
            self.tblView.beginUpdates()
            self.tblView.deleteRows(at: [IndexPath(row: rows, section: sections)], with: .right)
            self.tblView.endUpdates()
        }
        
    }
    
}
