//
//  MyProfileViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 15/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import RSKImageCropper
import NVActivityIndicatorView

class MyProfileViewController: UIViewController {

//MARK:- VARIABLES
    var imageLoaded: Bool = false
    var imagePicked: UIImage?
    var loaderLoadFirstTime = 0
    
 //MARK:- BTN OUTLETS
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblLastNameFixed: UILabel!
    @IBOutlet weak var lblFirstNameFixed: UILabel!
    @IBOutlet weak var lblLastNameValue: UILabel!
    @IBOutlet weak var lblFirstNameValue: UILabel!
    
//MARK:- BTN ACTIONS
    @IBAction func btnImageAction(_ sender: Any) {
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeAction(_ sender: Any) {
        openImagePicker(sender)
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        editProfileVC.imagePicked = imagePicked
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    @IBAction func btnChangePassAction(_ sender: Any) {
        guard let addAdVc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController else { return }
        self.pushVC(addAdVc)
        //self.presentVC(addAdVc)
        
    }
    
//MARK:- VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar?.isHidden = true
        if UserDataSingleton.sharedInstance.loggedInUser?.fb_id != "" || UserDataSingleton.sharedInstance.loggedInUser?.google_id != "" {
            btnChangePassword.isHidden = true
        }
        else {
            btnChangePassword.isHidden = false
        }

        fetchProfileData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navBar?.isHidden = true
//        //Remove bottom border of navigation bar
        setIntialValues()

    }
    
    func setIntialValues(){
        
        lblFirstNameValue.text = UserDataSingleton.sharedInstance.loggedInUser?.firstname
        lblLastNameValue.text = UserDataSingleton.sharedInstance.loggedInUser?.lastname
        lblEmail.text = UserDataSingleton.sharedInstance.loggedInUser?.email
        lblDob.text = UserDataSingleton.sharedInstance.loggedInUser?.dob
        lblRating.text = String(format:" %@ ",(UserDataSingleton.sharedInstance.loggedInUser?.avg_rating).unwrap())
        
        let gender = UserDataSingleton.sharedInstance.loggedInUser?.gender
        if gender == "1"{
            lblGender.text = "Male"
        }else if gender == "2"{
            lblGender.text = "Female"
        }else{
            lblGender.text = ""
        }
        let image = UserDataSingleton.sharedInstance.loggedInUser?.image100 ?? "no url"
        print("the image URL is \n\(image)")
//        cell.imgView.sd_setImage(with: NSURL.init(string: img) as? URL,placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
        imgView.sd_setImage(with: URL(string: image),placeholderImage: UIImage.init(named: "ic_profile_placeholder"))
        imgView.layer.cornerRadius = imgView.frame.width/2
        imgView.clipsToBounds = true
        btnImage.isHidden = true

        btnChange.layer.cornerRadius = 27
        btnChangePassword.layer.cornerRadius = 27
    }
}

extension MyProfileViewController: RSKImageCropViewControllerDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    func openImagePicker(_ sender : Any){
        let alert = UIAlertController(title: "", message: "Please upload image from:", preferredStyle: .actionSheet)
        alert.view.tintColor = appColor
        alert.view.backgroundColor = UIColor.white
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Albums", style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let image : UIImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        var imageCropVC : RSKImageCropViewController!
        imageCropVC = RSKImageCropViewController(image: image, cropMode: RSKImageCropMode.circle)
        imageCropVC.delegate = self
        self.navigationController?.pushViewController(imageCropVC, animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        imagePicked = resizeImage(image: croppedImage, newWidth: 100)
        self.imgView.image = croppedImage
        self.imgView.clipsToBounds = true
        imageLoaded = true // checking whether there was an image change or not
        changeImage()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
//MARK::- Edit profile with change in the image
extension MyProfileViewController {
    func changeImage() {
        if imageLoaded == false{
            print("No image change")
        }else{
            
            UtilityFunctions.startColoredLoader()
            
            let user = UserDataSingleton.sharedInstance.loggedInUser
            
            APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.editProfile(access_token: user?.access_token, firstname: user?.firstname, lastname: user?.lastname, gender: user?.gender, dob: user?.dob, radius: UserDataSingleton.sharedInstance.loggedInUser?.radius)) , image: imagePicked , completion: { (response) in
                
                UtilityFunctions.stopLoader()
                
                switch response{
                    
                case .success(let data) :
                    
                    print(data ?? "")
                    guard let map = data as? User else { return }
                    
                    //populating the new URL of the new image
                    UserDataSingleton.sharedInstance.loggedInUser = map
                    
                    print("IMAGE CHANGED SUCCESSFULLY!!!")
                    UtilityFunctions.showAlert(message: "Image changed successfully!", controller: self)
                    
                case .failureMessage(let message):
                    print("failure")
                    UtilityFunctions.showAlert(message: message, controller: self)
                default : break
                }
            })
        }
    }
    
}
//MARK:- API CALL
extension MyProfileViewController{
  func fetchProfileData()
    {
    APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfile(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token)),loaderNeeded:loaderLoadFirstTime) { (response) in
    self.loaderLoadFirstTime = 1
    
    switch response {
    case .success(let data):
    
    weak var weakSelf  = self
    print(data ?? "")
    let x = data as? User
    print(x?.access_token ?? "")
    UserDataSingleton.sharedInstance.loggedInUser = data as? User
    self.setIntialValues()
    case .failureMessage(let message):
    print(message ?? "")
    UtilityFunctions.showAlert(message: message, controller: self)
    
    default : break
    }
    }

    }
}

