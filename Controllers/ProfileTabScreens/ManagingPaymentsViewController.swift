//
//  ManagingPaymentsViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 22/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import Stripe
import NVActivityIndicatorView


class ManagingPaymentsViewController: UIViewController{
    
//MARK::- VC VARIABLES
    var code : String = ""
    var isCode = ""
    
    var amounts:String?
    var postId:String?
    var usersId: String?
    var typeToPass:Int?
    var accessToken: String?
    var user_id: String?
    var loaderLoadFirstTime = 0
    var addAdVC : AddAdViewController?
    
//MARK::- VC OUTLETS
    
    @IBOutlet var webView: UIWebView!{
        didSet{
            webView.delegate = self
        }
    }
//MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(amounts)
        webView.loadRequest(URLRequest(url: URL(string: "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_AFvnodjTd6kCG0S2Jx9E4RlvuGLKwrja&scope=read_write")! as URL))

    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
 
    @IBAction func btnManagePayments(_ sender: Any) {
//        webView.loadRequest(URLRequest(url: URL(string: "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_AFvnodjTd6kCG0S2Jx9E4RlvuGLKwrja&scope=read_write")! as URL))
        
    }
   
}
//MARK::- WEB VIEW DELEGATES
extension ManagingPaymentsViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UtilityFunctions.startColoredLoader()
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let str = String(describing: request.url)
        
        isCode = String(describing:str.contains("code=") ? "true" : "false")
        
        if let range = str.range(of: "code=") {
            let endPos = str.characters.distance(from: str.characters.startIndex, to: range.upperBound)
            print(endPos)
            let startIndex = str.index(str.startIndex, offsetBy: endPos)
            let endIndex = str.index(str.endIndex, offsetBy: -1)
            let range = startIndex..<endIndex
            
        code = str.substring(with: range)
        //code = str.substring(from: startIndex)
            print("code is \(code)....")
        }
        print(request.url ?? "")
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UtilityFunctions.stopLoader()
         if(isCode == "true"){
        fetchUserAccountData(code: code)
        }
    }
}
//MARK::- API CALLS
extension ManagingPaymentsViewController {
    
    func fetchUserAccountData(code: String){
        
        UtilityFunctions.startColoredLoader()
    
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.updateStripeAccId(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, code: code)), loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
            
            case .success(let data) :
            
                print(data ?? "")
                
                let alert = UIAlertController(title: "Created Successfully", message: "Your stripe Account have been created successfully.\nDo you want to publish the post?", preferredStyle: .alert)
                alert.view.tintColor = appColor
                alert.view.backgroundColor = UIColor.white
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                    self.addAdVC?.publish()
                    self.dismiss(animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: {
                })
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                
            default : break

                }
            }
    }
    
  }
