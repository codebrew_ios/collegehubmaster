//
//  ApprovePaymentsViewController.swift
//  CollegeHub
//
//  Created by Sumanpreet on 23/02/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import NVActivityIndicatorView

class ApprovePaymentsViewController: UIViewController {
    
//MARK::- VC VARIABLES
    var arrayApprovePayments = [ProfileTabSubMenus]()
    var page = 1
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    let reportVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ReportIssueViewController") as? ReportIssueViewController

//MARK:- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lblNoResultFound: UILabel!
    
//MARK:- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchApprovePayments ()
        pullToRefresh()
         pullToRefreshUpward()
        self.lblNoResultFound.isHidden = true
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 212;//(Maximum Height that should be assigned to your cell)
        reportVC?.alertDelegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchApprovePayments ()
    }
//MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- TABLE VIEW DELEGATES
extension ApprovePaymentsViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayApprovePayments.count
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        
//        return "Today"
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(30)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayApprovePayments[section].details.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        headerView?.lblFormattedDate.text = (arrayApprovePayments[section].formatted_date)
        headerView?.lblTimeSince.text = (arrayApprovePayments[section].time_since)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modal = (arrayApprovePayments[indexPath.section].details[indexPath.row])
        if let type = modal.type?.toInt(){
            switch type {
                case 1:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.approvePaymentTripCell.rawValue , for: indexPath) as? ApprovePaymentTableViewCell  else {return UITableViewCell() }
                   // cell.delegate = self as! DelegateForTableCell
                    cell.modal = modal
                    cell.alertDelegate = self
                    cell.myApprovePayment = self
                    cell.section = indexPath.section
                    cell.row = indexPath.row
                    cell.selectionStyle = .none
                    cell.imgProgileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                    let price = (modal.price).unwrap().toInt()! * (modal.num_of_seats).unwrap().toInt()!
                    cell.lblAmmountToBePaid.text = "$ " + price.toString
                    //cell.lblAmmountToBePaid.text = "$ \(modal.price.unwrap())"
                    cell.lblDestinationFrom.text = modal.departure
                    cell.lblDestinationTo.text = modal.destination
                    cell.lblTitleTripDetails.text = "Trip Details"
                    cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                    return cell
                case 3,5,6,7:
                   guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.approvePaymentBookCell.rawValue , for: indexPath) as? ApprovePaymentsBooksTableViewCell  else {return UITableViewCell() }
                   cell.modal = modal
                   cell.section = indexPath.section
                   cell.row = indexPath.row
                   cell.alertDelegate = self
                   cell.imgProgileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                   cell.lblAmmountToBePaid.text = "$ \(modal.price.unwrap())"
                   cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                   cell.lblProductTitle.text = modal.name
                   switch type {
                   case 3:
                    cell.lblTitle.text = "Service Title"
                    cell.lblProductTitle.text = modal.service_name
                   case 5:
                    cell.lblTitle.text = "Book Title"
                   case 6:
                    cell.lblTitle.text = "Ticket Title"
                    let price = (modal.price).unwrap().toInt()! * (modal.num_of_tickets).unwrap().toInt()!
                    cell.lblAmmountToBePaid.text = "$ " + price.toString
                   case 7:
                    cell.lblTitle.text = "Item Title"
                   default:
                    break
                   }

                   return cell
                default:
                   guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.approvePaymentBookCell.rawValue , for: indexPath) as? ApprovePaymentsBooksTableViewCell  else {return UITableViewCell() }
                    return cell
            }
    }
        return UITableViewCell()
}
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchApprovePayments()
        }
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchApprovePayments()        }
        
    }
    
}

//MARK:- API CALLS
extension ApprovePaymentsViewController {
    
    func fetchApprovePayments() {
        
        if(tempRefresh != "Header" && tempRefresh != "Footer"){
            UtilityFunctions.startColoredLoader()
        }
        let type = "4"
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfileData(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page : page.toString,data_for: type)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayApprovePayments.append(contentsOf: map)
                case "Header":
                    self.arrayApprovePayments = map
                default :
                    self.arrayApprovePayments = map
                }
                self.tempRefresh = ""
                if self.arrayApprovePayments.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                self.tblView?.delegate = self
                self.tblView?.dataSource = self
                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
                
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
        
    }
    func approvePayment(postsId:String, usersId:String, adType: String,firstName:String,lastName:String,image:String,section:Int?,row:Int?) {
    UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.approvePayment(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: postsId, users_id: usersId, type: adType)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response {
                
            case .success(let data):
                print(data ?? "")
                print("approved successfully")
                let sections = section ?? 0
                let rows = row ?? 0
                self.updateTableView(section: sections, row: rows)
                guard  let rateVC =  self.storyboard?.instantiateViewController(withIdentifier: "RatingNFeedbackViewController") as? RatingNFeedbackViewController else {return}
               // rateVC.delegate = self
                rateVC.user_id = usersId
                rateVC.post_id = postsId
                rateVC.profilePic = image
                rateVC.userName = String(format:"%@ %@",firstName,lastName)
                self.dim(direction: .in, alpha: 0.5, speed: 0.5)
                self.presentVC(rateVC)                
            case .failure(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message.rawValue, controller: self)
                
            default: break
            }
        }
    }

}

extension ApprovePaymentsViewController : DelegateApprovePayment {
    
    func showAlert(message: String?,postsId: String?, section: Int?, row: Int?,users_id:String?,type:String?,firstname : String?,lastname: String?,image100: String?){
        UtilityFunctions.show(alert: "", message: message.unwrap(), buttonOk: {
            let sections = section ?? 0
            let rows = row ?? 0
            self.approvePayment(postsId: postsId.unwrap(), usersId: users_id.unwrap(), adType: type.unwrap(),firstName: firstname.unwrap(),lastName:lastname.unwrap(),image:image100.unwrap(),section: sections,row: rows)
        }, viewController: self)
    }
    
    func updateTableView(section: Int?,row:Int?)
    {
        let sections = section ?? 0
        let rows = row ?? 0
        self.arrayApprovePayments[sections].details.remove(at: rows)
        if self.arrayApprovePayments[sections].details.count == 0 {
            self.arrayApprovePayments.remove(at: sections)
            self.tblView.beginUpdates()
            self.tblView.deleteSections([sections], with: .left)
            self.tblView.deleteRows(at: [IndexPath(row: rows, section: sections)], with: .right)
            self.tblView.endUpdates()
        }else {
            self.tblView.beginUpdates()
            self.tblView.deleteRows(at: [IndexPath(row: rows, section: sections)], with: .right)
            self.tblView.endUpdates()
        }

    }
    func presentReportVc(modal:ProfileTabSubMenus?,section:Int?,row:Int?)
    {
        guard let reportVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ReportIssueViewController") as? ReportIssueViewController else {return}
        reportVC.dataPassed = modal
        reportVC.section = section
        reportVC.row = row
        self.presentVC(reportVC)
    }
}

    


