//
//  ChangePasswordViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 22/04/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import NVActivityIndicatorView
import SwiftyJSON

class ChangePasswordViewController: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var txtFieldOldPassword: UITextField!
    @IBOutlet weak var txtFieldConfirmPassword: UITextField!
    @IBOutlet weak var txtFieldNewPassword: UITextField!
    
    //MARK::- Variables
    let access_token = UserDataSingleton.sharedInstance.loggedInUser?.access_token ?? "no token"
    var loaderLoadFirstTime = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //removeBottomBorderOfNavigationItem()
        // Do any additional setup after loading the view.
        self.navBar?.isHidden = true
        
    }
    func removeBottomBorderOfNavigationItem() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Fucntions
    func checkPasswordsEntered() {
        if let newPassword = txtFieldNewPassword.text, let oldPassword = txtFieldOldPassword.text, let confirmPassword = txtFieldConfirmPassword.text {
            if (newPassword.length) < 6 {
                print("New Password length less than 8 characters")
                UtilityFunctions.showAlert(message: "New Password length less than 6 characters", controller: self)
            }
            if (newPassword.length) > 15 {
                print("New Password length less than 8 characters")
                UtilityFunctions.showAlert(message: "New Password length greater than 15 characters", controller: self)
            }
            if newPassword != confirmPassword {
                print("Passwords don't match - put a alert")
                UtilityFunctions.showAlert(message: "Password and Confirm Passwords don't match", controller: self)
            }
            else if newPassword == oldPassword {
                print("Old password and new password cannot be same.")
                UtilityFunctions.showAlert(message: "Old password and new password cannot be same.", controller: self)
            }
            else if newPassword == confirmPassword {
                changePassword(oldPassword, newPassword)
            }
        }
        else {
            print("All fields are empty")
            UtilityFunctions.showAlert(message: "All fields are empty", controller: self)
        }
    }
    //MARK:- Actions
    
    @IBAction func btnBackClick(_ sender: Any) {
        //self.dismiss(animated: false, completion: nil)
        //self.popVC()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnChangePasswordClick(_ sender: Any) {
        checkPasswordsEntered()
    }
}

//MARK:- HANDLE TEXTFIELD DELEGATES
extension ChangePasswordViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFieldOldPassword{
            txtFieldOldPassword.becomeFirstResponder()
            return true
        }else if textField == txtFieldConfirmPassword{
            txtFieldConfirmPassword.becomeFirstResponder()
            return true
        }else if textField == txtFieldNewPassword{
            txtFieldNewPassword.becomeFirstResponder()
            return true
        }else{
            return true
        }
    }

}
//MARK::- API
extension ChangePasswordViewController {
    
    func changePassword(_ oldPassword: String, _ newPassword: String) {
        
        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.changePassword(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, old_password: oldPassword, new_password: newPassword)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
             UtilityFunctions.stopLoader()
            
            switch response {
            case .success(let data):
                
                print("password changed successfully")
                print(data ?? "no data")
                
                UtilityFunctions.showAlertAndDismiss(message: "Password changed succesfully,\n Now Login again with new password.", controller: self, dismissController: {(flag) -> Void in
                    if flag == true {
//                        _ = self.navigationController?.popViewController(animated: true)
//                        OnBoardingViewController
                        //self.callLogoutAPI(self.access_token)
//                        guard let onBoardingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingViewController") as? OnBoardingViewController else { return }
//                        self.presentVC(onBoardingVC)
                        guard let navController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() else { return }
                        self.presentVC(navController)
                        
                    }
                })
            case .success_wrongPassword(let data):
                print("wrong password")
                print(data ?? "no data")
                
                UtilityFunctions.showAlert(message: "Wrong Old password", controller: self)
                
            case .failure(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message.rawValue, controller: self)
            default: break
            }
        
        }
    }
    func callLogoutAPI(_ accessToken: String){
        
        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.logout(access_token: accessToken)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                let message = data as! JSON
                print(message.stringValue)
                
                UserDataSingleton.sharedInstance.loggedInUser?.access_token = nil
                
                guard let onBoardingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingViewController") as? OnBoardingViewController else { return }
                self.presentVC(onBoardingVC)
                
            case .failureMessage(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
            default : break
            }
        }
    }

}
