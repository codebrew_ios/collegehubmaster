//
//  ChangeBidViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 28/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit

class ChangeBidViewController: UIViewController {

//MARK::- Outlets
    @IBOutlet weak var txtFieldBidAmount: UITextField!
    @IBOutlet weak var lblMinimumBidValueText: UILabel!
    @IBOutlet weak var lblCurrentHighestBidValue: UILabel!
    @IBOutlet weak var lblCurrentHighestBidLabel: UILabel!
    @IBOutlet weak var lblRemainingDurationLabel: UILabel!
    @IBOutlet weak var lblRemainingDaysValue: UILabel!
    
//MARK::- Variables
    var postsId:String?
    var type:Int?
    var usersId: String?
    var presentingVeiw:String?
    var bidCheck:String?
    var data: ProfileTabSubMenus?
    var minBid: String?
    var maxBid: String?
    var price: String?
    var remainingDays: String?
    var createdOn : String?
    var biddingVales : BiddingViewController?
    var cardDetailVar : CardDetailViewController?
    var loaderLoadFirstTime = 0
//MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initialise()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
            self.initialise()
    }
//MARK::- Functions
    func initialise() {
        
        
        if (maxBid == "0.00" && minBid == "0.00") {
            lblCurrentHighestBidValue.text =  "$ \(price.unwrap())"
            lblMinimumBidValueText.text = "Minimum Bid $ \(price.unwrap())"
            
        }
        else {
            lblCurrentHighestBidValue.text = "$ \(maxBid.unwrap())"
            lblMinimumBidValueText.text = "Minimum Bid $ \(minBid.unwrap())"
        }
        
        lblRemainingDaysValue.text = remainingDays
    }
    
    func choosePopOrDismiss() {
        guard let view = presentingVeiw else { return }
        if view == "CardDetail" {
        self.cardDetailVar?.dismissVc()
        }
        else if view == "BiddingCell" {
            self.dismissVC(completion: nil)
        }
    }
    
    func bidStatusCheck() {
        guard let statusOfBid = bidCheck else { return }
        print(statusOfBid)
    }

//MARK::- BTN ACTIONS
    
    @IBAction func btnBack(_ sender: Any) {
    choosePopOrDismiss()
    }
    
    @IBAction func btnSubmitClick(_ sender: Any) {
        var maxPrice = ""
        if(maxBid == "0.00")
        {
           maxPrice = price.unwrap()
        }
        else {
            maxPrice = maxBid.unwrap()
        }
        let priceOfBid = txtFieldBidAmount.text
        let postsId = self.postsId
        guard let typeToSend = self.type else {return}
        switch typeToSend {
        case 5:
            type = 9
        case 6:
            type = 10
        case 7:
            type = 11
        default: break
        }
        let usersId = self.usersId
        if (lblRemainingDaysValue.text == "closed" ) {
            UtilityFunctions.showAlert(message : "Bidding closed for this item.", controller: self)
        }
        else if (priceOfBid?.toInt() ?? 0 <= ((maxPrice).toInt())!){
            UtilityFunctions.showAlert(message : "Bid Should be greater than current highest bid", controller: self)
        }
        else {
            bidForItem(postsId!, usersId!, (type?.toString)!, priceOfBid!)
        }
        
    }}


//MARK::- UITextFieldDelegate
extension ChangeBidViewController: UITextFieldDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        txtFieldBidAmount.keyboardType = .numberPad
        txtFieldBidAmount.becomeFirstResponder()
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        txtFieldBidAmount.resignFirstResponder()
        return true
        
    }
    

}
extension ChangeBidViewController {
    
    func bidForItem(_ postsId:String, _ usersID: String, _ type: String, _ priceOfBid: String) {
        
        UtilityFunctions.startColoredLoader()
        
        print("User ID me" + (UserDataSingleton.sharedInstance.loggedInUser?.user_id)!)
        print("User ID passed" + usersID)
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.buyPosts(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, amount: priceOfBid, posts_id: postsId, users_id: usersID, type: type)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response {
            case .success(let data):
                print(data ?? " no data")
                print("Placed the bid successfully")
                UtilityFunctions.showSingle(alert: "", message: "Your Bid have been placed and you will be notified if your bid remains rhe highest one.You can increase your bid from bidding list present in profile tab", buttonOk: {
                    self.lblCurrentHighestBidValue.text = "$ " + priceOfBid
                    self.txtFieldBidAmount.text = ""
                    self.choosePopOrDismiss()
                }
                    , viewController: self)
                
                
            case .failure(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message.rawValue, controller: self)
            default: break
    
            }
        
        }
    }
}
