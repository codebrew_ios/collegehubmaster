//
//  ReportIssueViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 10/05/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit


class ReportIssueViewController: UIViewController {

//MARK::- OUTLETS
    
    @IBOutlet weak var lblTypeDetail: UILabel!
    @IBOutlet weak var lblDestinationCityName: UILabel!
    @IBOutlet weak var lblDepartureCityName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblProfilePic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtViewFeedback: UITextView!
    @IBOutlet weak var lblDescriptionFixed: UILabel!
    @IBOutlet weak var lblTo: UILabel!
//MARK::- VARIABLES
    var dataPassed: ProfileTabSubMenus?
    var detailIndex : Int?
    var users_id = ""
    var posts_id = ""
    var section: Int?
    var row: Int?
    var loaderLoadFirstTime = 0
    weak var alertDelegate:DelegateApprovePayment?
    override func viewDidLoad() {
        super.viewDidLoad()
        setIntialValues()

    }
    override func viewDidAppear(_ animated: Bool) {
       // setIntialValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//MARK::- Functions
    func setIntialValues() {
        txtViewFeedback.text = ""
        guard let data = dataPassed /*, let detailIndex = detailIndex*/ else { return }
        let modal = data
        users_id = (modal.users_id).unwrap()
        posts_id = (modal.posts_id).unwrap()
        lblUserName?.text = String(format:"%@ %@ ",(modal.firstname).unwrap(),(modal.lastname).unwrap())
        lblProfilePic.sd_setImage(with: URL(string: (modal.image100).unwrap()))
        guard let type = modal.type else {return}
        switch type {
            case "1":
                let price = (modal.price).unwrap().toInt()! * (modal.num_of_seats).unwrap().toInt()!
                lblPrice?.text = "$ " + price.toString
                lblDepartureCityName?.text = modal.departure.unwrap()
                lblDestinationCityName?.text = modal.destination.unwrap()
                lblTypeDetail?.text = "Trip Detail"
            case "3":
                lblPrice?.text = "$ " + modal.price.unwrap()
                lblDepartureCityName?.text = modal.service_name.unwrap()
                lblDestinationCityName?.text = ""
                lblTypeDetail?.text = "Service Name"
                lblTo?.text = ""
            case "5":
                lblPrice?.text = "$ " + modal.price.unwrap()
                lblDepartureCityName?.text = modal.name.unwrap()
                lblDestinationCityName?.text = ""
                lblTypeDetail?.text = "Book Title"
                lblTo?.text = ""
            case "6":
                let price = (modal.price).unwrap().toInt()! * (modal.num_of_tickets).unwrap().toInt()!
                lblPrice?.text = "$ " + price.toString
                lblDepartureCityName?.text = modal.name.unwrap()
                lblDestinationCityName?.text = ""
                lblTypeDetail?.text = "Ticket Detail"
                lblTo?.text = ""
            case "7":
                lblPrice?.text = "$ " + modal.price.unwrap()
                lblDepartureCityName?.text = modal.name.unwrap()
                lblDestinationCityName?.text = ""
                lblTypeDetail?.text = "Item Detail"
                lblTo?.text = ""
            default: break
            
        }
    }
    
//MARK::- Actions
    
    @IBAction func btnBackClick(_ sender: Any) {
        //pop the view controller
        self.dismiss(animated: true, completion: nil)
        //_ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnReportClick(_ sender: Any) {
        if(txtViewFeedback.text == "")
        {
           UtilityFunctions.showAlert(message: "Please write something", controller: self)
        }
        else {
            reportIssue()
        }
//        guard let dataPassed = dataPassed, let index = detailIndex else { return }
//        let postsId = dataPassed.details[index].posts_id.unwrap()
//        let usersId = dataPassed.details[index].users_id.unwrap()
//        guard let feedback = txtViewFeedback.text else { return }
//        
//        reportIssue(postsId: postsId, usersId: usersId, feedback: feedback)
       
    }


}
//MARK::- TableView
//extension ReportIssueViewController: UITableViewDelegate, UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let datPassed = dataPassed , let detailIndex = detailIndex else { return UITableViewCell() }
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReportIssueTableViewCell", for: indexPath) as? ReportIssueTableViewCell else { return UITableViewCell() }
//        //username, price, image, if trip then trip name and if other then title
//        
//        cell.lblUserName.text = datPassed.details[detailIndex].firstname.unwrap() + datPassed.details[detailIndex].lastname.unwrap()
//        cell.lblDepratureCityName.text = datPassed.details[detailIndex].departure.unwrap()
//        cell.lblDestinationCityName.text = datPassed.details[detailIndex].destination.unwrap()
//        cell.lbPrice.text = "\(datPassed.details[detailIndex].price?.toDouble() ?? 0.0)"
//        
//        return cell
//    }
//}


//MARK::- API reportIssue
extension ReportIssueViewController {
    //func reportIssue(postsId: String, usersId: String, feedback: String) {
     func reportIssue() {
        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.reportIssue(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id, users_id: users_id, feedback: txtViewFeedback.text)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 0
            
            UtilityFunctions.stopLoader()
            
            switch response {
                
            case .success(let data):
                print(data ?? "")
                print("approved successfully")
                UtilityFunctions.showSingle(alert: "", message: "Your issue have been reported.We will look into the matter and will get back to you on this as soon as possible", buttonOk:
                    {
                        self.alertDelegate?.updateTableView(section: self.section, row: self.row)
                        self.dismiss(animated: true, completion: nil)
                
                }, viewController: self)
                
                self.txtViewFeedback.text = ""
               // self.dismiss(animated: true, completion: nil)
            case .failure(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message.rawValue, controller: self)
                
            default: break
            }
        }
        
    }
}
