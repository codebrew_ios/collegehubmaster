//
//  RatingNFeedbackViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 10/05/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import Cosmos

protocol DimoutVC {
    func dimOut()
}


class RatingNFeedbackViewController: UIViewController {

    @IBOutlet weak var imgVIewPorfilePic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRateMessageFixed: UILabel!
    @IBOutlet weak var lblFeedbackMessageFixed: UILabel!
    @IBOutlet weak var txtViewFeedback: UITextView!
    
    var delecateDimOut : DimoutVC?
    var user_id = ""
    var post_id = ""
    var userName = ""
    var profilePic = ""
    var loaderLoadFirstTime = 0
    
    @IBOutlet weak var viewStarsForRating: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblUserName.text = userName
        imgVIewPorfilePic.sd_setImage(with: URL(string: (profilePic)))
        txtViewFeedback.placeholderText = "Write Here...."
        viewStarsForRating.settings.fillMode = .half
        viewStarsForRating.settings.starSize = Double(UIScreen.main.bounds.width/6)
        
    }
    
//MARK::- Action

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   
    @IBAction func btnSubmitAction(_ sender: Any) {
        rateUser()
//        (self.presentingViewController as? UINavigationController)?.topViewController?.dim(direction: .out)
//        self.dismissVC(completion: nil)
    }
}
//MARK::- API reportIssue
extension RatingNFeedbackViewController {
    //func reportIssue(postsId: String, usersId: String, feedback: String) {
    func rateUser() {
        UtilityFunctions.startColoredLoader()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.rateUser(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: post_id, users_id: user_id, stars: (viewStarsForRating.rating).toString, feedback: txtViewFeedback.text)),loaderNeeded:loaderLoadFirstTime)
            { (response) in
            self.loaderLoadFirstTime = 0
            
            UtilityFunctions.stopLoader()
            
            switch response {
                
            case .success(let data):
                print(data ?? "")
                print("approved successfully")
                UtilityFunctions.showSingle(alert: "", message: "Feedback Submitted Successfully", buttonOk:{
                    self.txtViewFeedback.text = ""
                        (self.presentingViewController as? UINavigationController)?.topViewController?.dim(direction: .out)
                    self.delecateDimOut?.dimOut()
                        self.dismissVC(completion: nil)}, viewController: self)
            case .failure(let message):
                print("failure")
                UtilityFunctions.showAlert(message: message.rawValue, controller: self)
                
            default: break
            }
        }
        
    }
}
