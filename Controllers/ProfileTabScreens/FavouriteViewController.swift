//
//  FavouriteViewController.swift
//  CollegeHub
//
//  Created by Sierra 4 on 27/03/17.
//  Copyright © 2017 Sumanpreet. All rights reserved.
//

import UIKit
import PullToRefreshKit
import NVActivityIndicatorView
import EZSwiftExtensions

class FavouriteViewController: UIViewController {
    
//MARK::- VC VARIABLES
    var arrayFavourites = [ProfileTabSubMenus]()
    var page = 1
    var tempRefresh = ""
    var loaderLoadFirstTime = 0
    //view controller for the detail
    let addAdVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardDetailViewController") as? CardDetailViewController
    
    
//MARK::- VC OUTLETS
    @IBOutlet var tblView: UITableView!
    
    @IBOutlet weak var lblNoResultFound: UILabel!
//MARK::- VC LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        pullToRefresh()
         pullToRefreshUpward()
        self.lblNoResultFound.isHidden = true
        if(arrayFavourites == []){
            loaderLoadFirstTime = 1
        }
        self.tblView?.rowHeight = UITableViewAutomaticDimension;
        self.tblView?.estimatedRowHeight = 400;//(Maximum Height that should be assigned to your cell)
        fetchFavourites ()
    }
    
//MARK::- BTN ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK::- TABLE VIEW DELEGATES
extension FavouriteViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayFavourites.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(30)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFavourites[section].details.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let nib = UINib(nibName: "HeaderView", bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderView
        headerView?.lblFormattedDate.text = (arrayFavourites[section].formatted_date)
        headerView?.lblTimeSince.text = (arrayFavourites[section].time_since)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, commiteditingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let favouriteAction = UITableViewRowAction(style: .normal, title: "\nAdd to \nFavourites") { (action:UITableViewRowAction! , indexPath:IndexPath! ) -> Void in
            print(self.arrayFavourites[indexPath.section].details[indexPath.row].posts_id ?? "")
            print(self.arrayFavourites[indexPath.section].details[indexPath.row].is_favourite ?? "")
            self.addingPostToFavourite(posts_id: self.arrayFavourites[indexPath.section].details[indexPath.row].posts_id, section: indexPath.section, row: indexPath.row)
        }
        
        favouriteAction.backgroundColor = appColor
        favouriteAction.title = "❤️\n Remove\n from\n favourites"
        return [favouriteAction]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modal = (arrayFavourites[indexPath.section].details[indexPath.row])
        if(modal.maxBid == "0.00"){modal.maxBid = modal.price}
        if let type = modal.type?.toInt(){
            switch type {
            case 1 :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteCarpoolCell.rawValue , for: indexPath) as? FavouriteCarpoolTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.lblDeparture.text = (modal.departure)
                cell.lblDestination.text = (modal.destination)
                let price = (modal.price).unwrap().toInt()! * (modal.num_of_seats).unwrap().toInt()!
                cell.lblPrice.text = "$ " + price.toString
                //cell.lblPrice.text = "$ "  + (modal.price ?? "0")
                cell.lblRating.text = String(format:"%@  ",modal.avg_rating.unwrap())
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                let  dateIs = (modal.leaving_on)
                let dateString = dateIs?.toDateTime()
                cell.lblDateTime.text = dateString.unwrap()
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                return cell
            case 3 :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteServiceCell.rawValue , for: indexPath) as? FavouriteServiceTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                cell.lblPrice.text = "$" + (modal.price ?? "0")
                cell.lblRating.text = String(format:"%@  ",modal.avg_rating.unwrap())
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                cell.lblServiceName.text = (modal.service_name)
                cell.lblSessionLength.text = (modal.duration)
                return cell
            case 5 :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteBooksCell.rawValue , for: indexPath) as? FavouriteBooksTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                if(modal.is_fixed == "2"){
                    cell.lblPriceOrBid.text = "Bid"
                    cell.lblPrice.text = "$" + (modal.maxBid ?? "0")
                }
                else {
                    cell.lblPriceOrBid.text = "Price"
                    cell.lblPrice.text = "$" + (modal.price ?? "0")
                }

                cell.lblRating.text = String(format:"%@  ",modal.avg_rating.unwrap())
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                cell.lblBookTitle.text = (modal.name)
                cell.productImages = (modal.image) ?? []
                return cell
            case 6 :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteTicketsCell.rawValue , for: indexPath) as? FavouriteTicketsTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                if(modal.is_fixed == "2"){
                    cell.lblPriceOrBid.text = "Bid"
                    cell.lblPrice.text = "$" + (modal.maxBid ?? "0")
                }
                else {
                    cell.lblPriceOrBid.text = "Price"
                    let price = (modal.price).unwrap().toInt()! * (modal.num_of_tickets).unwrap().toInt()!
                    cell.lblPrice.text = "$ " + price.toString
                    //cell.lblPrice.text = "$" + (modal.price ?? "0")
                }

                cell.lblRating.text = String(format:"%@  ",modal.avg_rating.unwrap())
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                cell.lblTitle.text = (modal.name)
                let  dateIs = (modal.date).unwrap()
                let dateString = dateIs.toDateTime()
                cell.lblDateTime.text = dateString.unwrap() + "  " + (modal.time).unwrap()
                return cell
            case 7 :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteBooksCell.rawValue , for: indexPath) as? FavouriteBooksTableViewCell  else {return UITableViewCell() }
                cell.selectionStyle = .none
                cell.imgProfileImage.sd_setImage(with: URL(string: (modal.image100).unwrap()))
                if(modal.is_fixed == "2"){
                    cell.lblPriceOrBid.text = "Bid"
                    cell.lblPrice.text = "$" + (modal.maxBid ?? "0")
                }
                else {
                    cell.lblPriceOrBid.text = "Price"
                    cell.lblPrice.text = "$" + (modal.price ?? "0")
                }

                cell.lblRating.text = String(format:"%@  ",modal.avg_rating.unwrap())
                cell.lblUserName.text = (modal.firstname).unwrap() + " " + (modal.lastname).unwrap()
                cell.lblBookTitle.text = (modal.name)
                cell.productImages = (modal.image) ?? []
                return cell
            
            default :
                guard let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierTableCell.favouriteCarpoolCell.rawValue , for: indexPath) as? FavouriteCarpoolTableViewCell  else {return UITableViewCell() }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let modal = (arrayFavourites[indexPath.section].details[indexPath.row])
        guard let addAdVc = addAdVc else { return }
        addAdVc.currentData = modal
        addAdVc.callingView = "Favourites"
        addAdVc.postId = modal.posts_id
        addAdVc.duration = modal.duration
        addAdVc.createdOn = modal.createdAt
        addAdVc.usersId = modal.users_id
        addAdVc.maxBid = modal.maxBid
        addAdVc.minBid = modal.minBid
        addAdVc.amounts = modal.price
        addAdVc.typeToPass = modal.type?.toInt()
        addAdVc.isFixed = modal.is_fixed?.toInt() ?? 100
        let cellSelected = tableView.cellForRow(at: indexPath)
        if cellSelected is FavouriteCarpoolTableViewCell {
            print("Carpool")
            addAdVc.currentPage = "Rides"
        }
        else if cellSelected is FavouriteServiceTableViewCell {
            print("Service")
            addAdVc.currentPage = "Services"
        }
        else if cellSelected is FavouriteBooksTableViewCell {
            print("Books/ items")
            addAdVc.currentPage = "Items"
        }
        else if cellSelected is FavouriteTicketsTableViewCell {
            print("tickets")
            addAdVc.currentPage = "Tickets"
        }
        
        ez.runThisInMainThread {
            self.presentVC(addAdVc)
        }
    }
    
    
    func pullToRefresh(){
        _ = tblView.setUpFooterRefresh { [weak self] in
            if var value = self?.page{
                value = value + 1
                self?.page = value
                self?.tempRefresh = "Footer"
            }
            self?.fetchFavourites()
        }
        
    }
    func pullToRefreshUpward(){
        _ = tblView.setUpHeaderRefresh { [weak self] in
            self?.page = 1
            self?.tempRefresh = "Header"
            self?.fetchFavourites()
        }
        
    }
    
}

extension FavouriteViewController {
    func fetchFavourites () {
        
//        if(tempRefresh != "Header" && tempRefresh != "Footer"){
//          UtilityFunctions.startColoredLoader()
//        }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.viewProfileData(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, timezone: APIConstants.timezone, page : page.toString , data_for: "1")),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1
            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                
                print(data ?? "")
                guard let map = data as? [ProfileTabSubMenus] else { return }
                switch self.tempRefresh {
                case "Footer":
                    self.arrayFavourites.append(contentsOf: map)
                case "Header":
                    self.arrayFavourites = map
                default :
                    self.arrayFavourites.append(contentsOf: map)
                }
                //self.arrayFavourites.append(contentsOf: map)
                print(self.arrayFavourites)
                if self.arrayFavourites.count == 0 {
                    self.lblNoResultFound.isHidden = false
                    self.tblView.isHidden = true
                }else {
                    self.lblNoResultFound.isHidden = true
                    self.tblView.isHidden = false
                }
                self.tblView?.delegate = self
                self.tblView?.dataSource = self
                self.tblView?.reloadData()
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            case .failureMessage(let message):
                
                print("failure")
                UtilityFunctions.showAlert(message: message, controller: self)
                self.tblView.endFooterRefreshing()
                self.tblView.endHeaderRefreshing()
            default : break
                
            }
        }
        
    }
    func addingPostToFavourite(posts_id:String?, section: Int, row: Int)
    {
        UtilityFunctions.startColoredLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: api.userApi(ApiEnum: api.userApiEnum.addToFavourite(access_token: UserDataSingleton.sharedInstance.loggedInUser?.access_token, posts_id: posts_id)),loaderNeeded:loaderLoadFirstTime) { (response) in
            self.loaderLoadFirstTime = 1

            
            UtilityFunctions.stopLoader()
            
            switch response{
                
            case .success(let data) :
                self.arrayFavourites[section].details.remove(at: row)
                if self.arrayFavourites[section].details.count == 0 {
                    self.arrayFavourites.remove(at: section)
                    self.tblView.beginUpdates()
                    self.tblView.deleteSections([section], with: .left)
                    self.tblView.deleteRows(at: [IndexPath(row: row, section: section)], with: .right)
                    self.tblView.endUpdates()
                }else {
                    self.tblView.beginUpdates()
                    self.tblView.deleteRows(at: [IndexPath(row: row, section: section)], with: .right)
                    self.tblView.endUpdates()
                }

                print(data ?? "")
                let message = data as? ManagePaymentModal
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message:(message?.message ?? ""), preferredStyle: .alert)
                    print(message?.message ?? "")
                    alert.view.tintColor = appColor
                    alert.view.backgroundColor = UIColor.white
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                
            case .failureMessage( _):
                print("failure")
                UtilityFunctions.showAlert(message: "Failed!! Retry", controller: self)
            default : break
            }
        }
    }

}

